//
//  SettingsTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Frienji

class SettingsTests : XCTestCase {
    
    var settings:FakeSettings!
    
    override func setUp() {
        super.setUp()
        
        self.settings = FakeSettings()
    }
    
    override func tearDown() {
        super.tearDown()
        
        self.settings = nil
    }

    // MARK: Tests

    func testSavingPushNotificationsToken() {
        
        //given
        let token = "asdjhalf"
        XCTAssertNil(self.settings.pushNotificationsToken, "Token should not be present")
        
        //when
        self.settings.pushNotificationsToken = token
        
        //then
        XCTAssertEqual(token, self.settings.pushNotificationsToken, "Token should match")
    }
    
    func testSavingRegisteredForPushNotifications() {
        
        //given
        var registered = false
        XCTAssertEqual(registered, self.settings.registeredForPushNotifications, "Should not be registered")

        //when
        registered = true
        self.settings.registeredForPushNotifications = registered
        
        //then
        XCTAssertEqual(registered, self.settings.registeredForPushNotifications, "Should be registered")
    }
    
}