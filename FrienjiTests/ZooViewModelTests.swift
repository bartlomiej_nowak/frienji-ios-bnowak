//
//  ZooViewModelTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 12/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest
@testable import Frienji

class ZooViewModelTests: XCTestCase {

    let frienji1 = Frienji.fake()
    let frienji2 = Frienji.fake()
    
    func testLoadCommand() {
        //given
        let viewModel = ZooViewModel(frienjis: [frienji1])
        
        //when
        let result = viewModel.executeCommand(ZooCommand.LoadFrienjis(frienjis: [frienji2]))
        
        //then
        XCTAssertEqual([frienji2], result.frienjis)
    }

    func testDeleteCommand() {
        //given
        let viewModel = ZooViewModel(frienjis: [frienji1, frienji2])
        
        //when
        let result = viewModel.executeCommand(ZooCommand.DeleteFrienjiAtIndexPath(index: 0))
        
        //then
        XCTAssertEqual([frienji2], result.frienjis)
        XCTAssertEqual([frienji1], result.deletedFrienjis)
    }
    
    func testCommitDeleteCommand() {
        //given
        let viewModel = ZooViewModel(frienjis: [frienji2], deleted: [(index: 0, frienji: frienji1)])
        
        //when
        let result = viewModel.executeCommand(ZooCommand.CommitDelete())
        
        //then
        XCTAssertEqual([frienji2], result.frienjis)
        XCTAssertEqual([], result.deletedFrienjis)
    }
    
    func testCancelDeleteCommand() {
        //given
        let viewModel = ZooViewModel(frienjis: [frienji2], deleted: [(index: 0, frienji: frienji1)])
        
        //when
        let result = viewModel.executeCommand(ZooCommand.CancelDelete())
        
        //then
        XCTAssertEqual([frienji1, frienji2], result.frienjis)
        XCTAssertEqual([], result.deletedFrienjis)
    }
}
