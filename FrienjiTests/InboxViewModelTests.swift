//
//  InboxViewModelTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 19/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest

@testable import Frienji

class InboxViewModelTests: XCTestCase {
    
    let conversation1 = Conversation.fake()
    let conversation2 = Conversation.fake()

    func testLoadCommand() {
        //given
        let viewModel = InboxViewModel(conversations: [conversation1])
        
        //when
        let result = viewModel.executeCommand(.Load(conversations: [conversation2]))
        
        //then
        XCTAssertEqual([conversation2], result.conversations)
    }
    
}
