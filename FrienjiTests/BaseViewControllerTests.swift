//
//  BaseViewControllerTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
@testable import Frienji

class BaseViewControllerTests : XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        super.tearDown()
        
    }

    // MARK: Tests

    func testIfKeyboardWillHideCallbackIsCalledAfterReceivingNotification() {
        
        //given
        class TestController : BaseViewController {
            
            var expectation:XCTestExpectation!
            
            override func keyboardWillHide() {
                
                self.expectation.fulfill()
            }
        }
        
        let expectation = expectationWithDescription("keyboardWillHide")
        let testController = TestController()
        testController.expectation = expectation
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = testController
        
        //when
        NSNotificationCenter.defaultCenter().postNotificationName(UIKeyboardWillHideNotification, object: nil)
        
        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }

}