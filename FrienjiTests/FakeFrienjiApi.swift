//
//  FakeFrienjiApi.swift
//  Traces
//
//  Created by Adam Szeremeta on 20.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Alamofire
import CoreData
@testable import Frienji

struct FakeResponse {

    var data: NSHTTPURLResponse?
    var jsonData: AnyObject?
}

class FakeFrienjiApi: FrienjiApi {

    var request: String?
    var response = FakeResponse(data: nil, jsonData: nil)

}
