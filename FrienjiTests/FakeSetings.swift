//
//  FakeSetings.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
@testable import Frienji

class FakeSettings: Settings {

    let fakeFrienji = Frienji.fake()

    init(storage: [String: AnyObject]? = nil) {
        super.init()

        let defaults = DefaultsFake()
        if let storage = storage {
            defaults.storage = storage
        }
        self.defaults = defaults
    }

    override var userFrienji: Frienji? {
        return fakeFrienji
    }

    override var rx_userFrienji: Observable<Frienji> {
        return .just(fakeFrienji)
    }

}

class DefaultsFake: NSUserDefaults {

    var storage = [String: AnyObject]()

    // MARK: Override

    override func objectForKey(defaultName: String) -> AnyObject? {
        return self.storage[defaultName]
    }

    override func setObject(value: AnyObject?, forKey defaultName: String) {
        self.storage[defaultName] = value
    }

}
