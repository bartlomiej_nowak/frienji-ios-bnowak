//
//  LocationManagerTests.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import XCTest
import CoreLocation
import RxSwift
import RxCocoa
@testable import Frienji

class LocationManagerTests: XCTestCase {

    var locationManager:FakeLocationManager!

    override func setUp() {
        super.setUp()

        self.locationManager = FakeLocationManager()
    }

    override func tearDown() {
        super.tearDown()

    }

    // MARK: Tests

    func testThatLocationManagerUpdatesLocationAferStart() {

        XCTAssertFalse(self.locationManager.isUpdatingLocation, "Should not be updating")

        //when
        self.locationManager.startLocationUpdates()

        //then
        XCTAssertTrue(self.locationManager.isUpdatingLocation, "Should be updating")
    }

    func testThatLocationManagerDoesNotUpdatesLocationAferStop() {

        self.locationManager.startLocationUpdates()
        XCTAssertTrue(self.locationManager.isUpdatingLocation, "Should be updating")

        //when
        self.locationManager.stopLocationUpdates()

        //then
        XCTAssertFalse(self.locationManager.isUpdatingLocation, "Should not be updating")
    }

    func testThatLocationIsUpdatedAfterStartingUpdates() {

        //given
        let readyExpectation = expectationWithDescription("ready")
        let disposeBag = DisposeBag()

        //when
        self.locationManager.currentLocation.asObservable().skip(1).bindNext { (location:CLLocation?) in

            //then
            readyExpectation.fulfill()

            }.addDisposableTo(disposeBag)

        self.locationManager.startLocationUpdates()

        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }

    func testThatHeadingIsUpdatedAfterStartingUpdates() {

        //given
        let readyExpectation = expectationWithDescription("ready")
        let disposeBag = DisposeBag()

        //when
        self.locationManager.currentHeading.asObservable().skip(1).bindNext { (heading:Double?) in

            //then
            readyExpectation.fulfill()

            }.addDisposableTo(disposeBag)

        self.locationManager.startLocationUpdates()

        //then
        waitForExpectationsWithTimeout(3, handler: { error in
            XCTAssertNil(error, "Error")
        })
    }
    
    
}
