//
//  ReachabilityServiceTests.swift
//  Frienji
//
//  Created by Piotr Łyczba on 14/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import XCTest
import RxSwift
import RxTests

@testable import Frienji

class ReachabilityServiceTests: XCTestCase {

    func testReachabilityStatusReachableViaWiFi() {
        // when
        let status = ReachabilityStatus.Reachable(viaWiFi: true)

        // then
        XCTAssertTrue(status.reachable)
    }

    func testReachabilityStatusReachableViaMobile() {
        // when
        let status = ReachabilityStatus.Reachable(viaWiFi: false)

        // then
        XCTAssertTrue(status.reachable)
    }

    func testReachabilityStatusUnreachable() {
        // when
        let status = ReachabilityStatus.Unreachable

        // then
        XCTAssertFalse(status.reachable)
    }

}
