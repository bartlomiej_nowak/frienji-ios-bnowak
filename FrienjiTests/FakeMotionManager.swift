//
//  FakeMotionManager.swift
//  Traces
//
//  Created by Adam Szeremeta on 19.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreMotion
@testable import Frienji

class FakeMotionManager : MotionManager {
    
    override init() {
        super.init()
        
        self.motionManager = FakeCMMotionManager()
        self.motionManagerSessionQueue = NSOperationQueue.mainQueue()
    }
}

private class FakeCMMotionManager : CMMotionManager {
    
    override var accelerometerAvailable: Bool {
        
        return true
    }
    
    override func startAccelerometerUpdatesToQueue(queue:NSOperationQueue, withHandler handler:CMAccelerometerHandler) {
        
        queue.addOperationWithBlock {
            
            let data = FakeCMAccelerometerData()
            
            handler(data, nil)
        }
    }
    
}

private class FakeCMAccelerometerData : CMAccelerometerData {
    
    override var acceleration: CMAcceleration {
        
        return CMAcceleration(x: -0.9, y: 1.1, z: 0.9)
    }
}