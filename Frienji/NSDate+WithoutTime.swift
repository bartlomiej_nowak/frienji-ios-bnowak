//
//  NSDate+WithoutTime.swift
//  Frienji
//
//  Created by Adam Szeremeta on 21.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

extension NSDate {
    
    func withoutTime() -> NSDate? {
        
        guard let timezone = NSTimeZone(abbreviation: "UTC") else {
            return nil
        }
        
        let calendar = NSCalendar.currentCalendar()
        calendar.timeZone = timezone
        
        let componenets = calendar.components([NSCalendarUnit.Year, NSCalendarUnit.Month, NSCalendarUnit.Day], fromDate: self)
        return calendar.dateFromComponents(componenets)
    }
}
