//
//  ZooViewController.swift
//  Frienji
//
//  Created by bolek on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import SDWebImage
import CenterAlignedCollectionViewFlowLayout

typealias ZooSectionModel = AnimatableSectionModel<String, Frienji>
typealias ZooCollectionDataSource = RxCollectionViewSectionedAnimatedDataSource<ZooSectionModel>

class ZooViewController: UIViewController, StoryboardLoad {

    static var storyboardId: String = "Main"
    
    // MARK: - UI Controls

    var backButton: UIBarButtonItem {
        return UIBarButtonItem(title: Localizations.zoo.backBtn, style: .Plain, target: self, action: #selector(goBack(_:)))
    }

    var inboxButton: UIBarButtonItem {
        return UIBarButtonItem(title: Localizations.zoo.inboxBtn, style: .Plain, target: self, action: #selector(goToInbox(_:)))
    }
    
    var editZooButton: UIBarButtonItem {
        return UIBarButtonItem(title: self.editing ? Localizations.zoo.editDoneBtn : Localizations.zoo.editBtn, style: .Plain, target: self, action: #selector(toggleEditing(_:)))
    }
    
    var seeBlockedFrienjiButton: UIBarButtonItem? {
        let button = UIBarButtonItem(title: Localizations.zoo.see_blocked_frienji, style: .Plain, target: nil, action: nil)
        self.configureSeeBlockedFrienjiButton(button)
        
        return button
    }
    
    //outlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var emptyLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    // MARK: - Events

    let itemDeleted = PublishSubject<NSIndexPath>()
    let loadMoreTriggered = PublishSubject<Void>()
    let editingCommited = PublishSubject<Void>()
    let editingCanceled = PublishSubject<Void>()
    var viewModel: Observable<ZooViewModel>!
    let disposeBag = DisposeBag()

    // MARK: - Properties

    private var frienjisCount: Int = 0
    
    var emptyLabelText: String {
        return Localizations.zoo.empty_label
    }
    
    override var editing: Bool {
        didSet {
            configureNavigationBar()
            configureToolbar()

            collectionView?.reloadData()
        }
    }

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureNavigationBar()
        configureToolbar()

        if let layout = self.getCenterAlignedLayout() {
            self.collectionView.collectionViewLayout = layout
        }
        
        self.emptyLabel.text = self.emptyLabelText
        
        createBindings()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        self.configureToolbar()
        self.navigationController?.setToolbarHidden(false, animated: true)
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Zoo) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Zoo) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Zoo)
            }
            coachmark.showWithAnimationInContainer()
        }
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        self.navigationController?.setToolbarHidden(true, animated: true)
    }

    // MARK: - Configure view

    internal func configureNavigationBar() {
        navigationItem.leftBarButtonItem = backButton
        navigationItem.rightBarButtonItem = inboxButton
        navigationItem.title = editing ? Localizations.zoo.viewEditingTitle : Localizations.zoo.viewTitle

        if editing {
            navigationItem.leftBarButtonItem?.action = #selector(toggleEditing(_:))
            navigationItem.rightBarButtonItem = nil
        }
    }

    internal func configureToolbar() {
        
        guard let toolbar = self.navigationController?.toolbar else {
            return
        }
        
        toolbar.barTintColor = UIColor.yellowColor()
        toolbar.setTransparent(!self.editing)

        if editing {
            self.toolbarItems = [
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil),
                editZooButton,
                UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil)
            ]
            
        } else {
            self.toolbarItems = self.frienjisCount > 0 ?
                [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil), editZooButton] : [UIBarButtonItem]()
            
            if let blockedButton = self.seeBlockedFrienjiButton {
                self.toolbarItems?.insert(blockedButton, atIndex: 0)
            }
        }
    }
    
    private func configureSeeBlockedFrienjiButton(barButton:UIBarButtonItem) {
        //underline doesn't work in UIBarButtonItem
        let textAttributes = [
            NSFontAttributeName: UIFont.latoRegulatWithSize(13.0),
            NSForegroundColorAttributeName: UIColor.yellowColor(),
            NSUnderlineStyleAttributeName: NSUnderlineStyle.StyleSingle.rawValue,
            NSUnderlineColorAttributeName: UIColor.yellowColor()
        ]
        
        let button = UIButton(type: UIButtonType.Custom)
        button.setAttributedTitle(NSAttributedString(string: Localizations.zoo.see_blocked_frienji, attributes: textAttributes), forState: UIControlState.Normal)
        button.sizeToFit()
        
        button.rx_tap.asObservable().bindNext { [unowned self] _ in
            self.performSegueWithIdentifier(R.segue.zooViewController.showBlockedFrienji, sender: self)
        }.addDisposableTo(self.disposeBag)
        
        barButton.customView = button
    }
    
    internal func headerTitle() -> String {
        return self.editing ? Localizations.zoo.header.editingTitle : Localizations.zoo.header.title
    }
    
    // MARK: Layout
    
    private func getCenterAlignedLayout() -> UICollectionViewLayout? {
        
        guard let oldLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return nil
        }
        
        let layout = CenterAlignedCollectionViewFlowLayout()
        layout.minimumInteritemSpacing = oldLayout.minimumInteritemSpacing
        layout.minimumLineSpacing = oldLayout.minimumLineSpacing
        layout.headerReferenceSize = oldLayout.headerReferenceSize
        layout.itemSize = oldLayout.itemSize
        layout.sectionInset = oldLayout.sectionInset
        
        return layout
    }

    // MARK: - Bindings

    private func createBindings() {
        viewModel = prepareViewModel()

        bindViewModel()
        bindEditing()
        bindSelection()
    }

    private func prepareViewModel() -> Observable<ZooViewModel> {
        return Observable.of(
            prepareLoad(),
            prepareDelete(),
            prepareCancelEditing()
            )
            .merge()
            .scan(ZooViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .shareReplay(1)
    }

    internal func prepareLoad() -> Observable<ZooCommand> {
        let loadMore = collectionView?.loadMoreTrigger.filter { [unowned self] in !self.editing } ?? Observable.empty()
        return FrienjiApi.sharedInstance.getFrienjis(loadMore).showAlertOnApiError(self)
            .map(ZooCommand.LoadFrienjis)
    }

    private func prepareCancelEditing() -> Observable<ZooCommand> {
        return editingCanceled
            .map(ZooCommand.CancelDelete)
    }

    private func prepareCommitEditing() -> Observable<ZooCommand> {
        return editingCommited
            .map(ZooCommand.CommitDelete)
    }

    private func prepareDelete() -> Observable<ZooCommand> {
        return itemDeleted
            .map { $0.row }
            .map(ZooCommand.DeleteFrienjiAtIndexPath)
    }

    private func prepareDataSource() -> ZooCollectionDataSource {
        let dataSource = ZooCollectionDataSource()
        dataSource.configureCell = { _, collectionView, indexPath, frineji in
            let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ZooCollectionViewCell.reuseIdentifier, forIndexPath: indexPath)
            guard let zooCell = cell as? ZooCollectionViewCell else {
                return cell
            }

            zooCell.confireCellForFrienji(frineji, isViewInEditionMode: self.editing)
            zooCell.delegate = self
            
            return zooCell
        }
        dataSource.supplementaryViewFactory = { _, collectionView, kind, indexPath in
            let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "ZooHeader", forIndexPath: indexPath)
            guard let zooHeader = header as? ZooCollectionViewHeader else {
                return header
            }

            zooHeader.headerTitle.text = self.headerTitle()

            return zooHeader
        }

        return dataSource
    }

    private func bindViewModel() {
        viewModel
            .map { viewModel in
                [ZooSectionModel(model: "zoo", items: viewModel.frienjis)]
            }
            .bindTo(collectionView!.rx_itemsWithDataSource(prepareDataSource()))
            .addDisposableTo(disposeBag)
        
        viewModel.bindNext { [weak self] model in
            self?.loadingIndicator.hidden = true
            self?.frienjisCount = model.frienjis.count
            self?.configureToolbar()
        }.addDisposableTo(self.disposeBag)
        
        viewModel.map { $0.frienjis.count != 0 }.bindTo(self.emptyLabel.rx_hidden).addDisposableTo(self.disposeBag)
        viewModel.map { $0.frienjis.count == 0 }.bindTo(self.collectionView.rx_hidden).addDisposableTo(self.disposeBag)
    }

    private func bindSelection() {
        collectionView!.rx_modelSelected(Frienji.self)
            .filter { _ in !self.editing }
            .subscribeNext(showWallViewForFrienji)
            .addDisposableTo(disposeBag)
    }

    internal func bindEditing() {
        editingCommited
            .withLatestFrom(viewModel) { _, viewModel in
                viewModel.deletedFrienjis
            }
            .flatMap { frienjis in
                FrienjiApi.sharedInstance.deleteFrienjis(frienjis)
            }
            .subscribe()
            .addDisposableTo(disposeBag)
    }

    // MARK: - Actions

    func goBack(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }

    func goToInbox(sender: AnyObject) {
        self.navigationController?.pushViewController(InboxViewController.loadFromStoryboard(), animated: true)
    }

    @IBAction func toggleEditing(sender: UIBarButtonItem) {
        if self.editing && sender != self.navigationItem.leftBarButtonItem {
            editingCommited.onNext()
        } else if self.editing {
            editingCanceled.onNext()
        }
        
        editing = !editing
    }

    func showWallViewForFrienji(frienji: Frienji) {
        performSegueWithIdentifier(R.segue.zooViewController.showFrienji.identifier, sender: Observable.just(frienji))
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let wallPostsViewController = segue.destinationViewController as? WallPostsViewController, frienji = sender as? Observable<Frienji> {
            frienji.bindTo(wallPostsViewController.input.loaded).addDisposableTo(disposeBag)
        }
    }

}

extension ZooViewController: ZooCollectionViewCellProtocol {
    
    func zooCollectionViewCellDidTouchDeleteButton(cell: ZooCollectionViewCell) {
        
        guard let indexPath = self.collectionView.indexPathForCell(cell) else {
            return
        }
        
        itemDeleted.onNext(indexPath)
    }
}
