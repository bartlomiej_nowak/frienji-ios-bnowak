//
//  Frienji.swift
//  Frienji
//
//  Created by bolek on 16.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxDataSources
import Argo
import Curry
import Fakery
import CoreLocation

struct Frienji {

    enum Relation: String {
        case None = "none"
        case Catched = "saved"
        case Rejected = "rejected"
        case Blocked = "blocked"
    }

    let userId: Int
    let username: String
    let phoneNumber: String?
    let createdAt: NSDate
    let avatar: Avatar
    let backgroundImageUrl: String?
    let lastLatitude: Double?
    let lastLongitude: Double?
    let whoAreYou: String?
    var relation: Relation

    func frienjiByRelationChange(relation: Frienji.Relation) -> Frienji {
        return Frienji(
            userId: userId,
            username: username,
            phoneNumber: phoneNumber,
            createdAt: createdAt,
            avatar: avatar,
            backgroundImageUrl: backgroundImageUrl,
            lastLatitude: lastLatitude,
            lastLongitude: lastLongitude,
            whoAreYou: whoAreYou,
            relation: relation
        )
    }

}

// MARK: - Fake

extension Frienji.Relation: Fakeable {

    static func fake() -> Frienji.Relation {
        return [.None, .Catched, .Rejected, .Blocked][faker.number.randomInt(min: 0, max: 3)]
    }

}

extension Frienji: Fakeable {

    static func fake() -> Frienji {
        return Frienji(
            userId: faker.number.increasingUniqueId(),
            username: faker.name.name(),
            phoneNumber: faker.phoneNumber.phoneNumber(),
            createdAt: NSDate.randomWithinDaysBeforeToday(356),
            avatar: Avatar.fake(),
            backgroundImageUrl: faker.internet.image(),
            lastLatitude: faker.address.latitude(),
            lastLongitude: faker.address.longitude(),
            whoAreYou: faker.lorem.sentences(amount: 5),
            relation: .None
        )
    }

}

// MARK: - Decode

extension Frienji: Decodable {

    static func decode(json: JSON) -> Decoded<Frienji> {
        let d = curry(self.init)
        return d
            <^> json <| "id"
            <*> json <| "username"
            <*> json <|? "phone_number"
            <*> (json <| "created_at" >>- toNSDate)
            <*> json <| "avatar"
            <*> json <|? "cover_photo_url"
            <*> json <|? ["location", "latitude"]
            <*> json <|? ["location", "longitude"]
            <*> json <|? "who_you_are"
            <*> json <| "relation_type" <|> pure(Relation.None)
    }

}

extension Frienji.Relation: Decodable {

    static func decode(json: JSON) -> Decoded<Frienji.Relation> {
        switch json {
        case let .String(rawValue):
            guard let result = Frienji.Relation(rawValue: rawValue) else {
                return .typeMismatch("\(Frienji.Relation.self)", actual: json)
            }
            return pure(result)
        default:
            return .typeMismatch("String", actual: json)
        }
    }

}

// MARK: - Uniqueness

extension Frienji: IdentifiableType, Hashable {

    var identity: Int {
        get {
            return userId
        }
    }

    var hashValue: Int {
        return userId
    }

}

// MARK: - Equality

extension Frienji: Equatable {

}

func == (lhs: Frienji, rhs: Frienji) -> Bool {
    return lhs.userId == rhs.userId
}

// MARK: - AR view compatibility

extension Frienji {

    var dbID: Int64 {
        return Int64(userId)
    }

    var location: CLLocation {
        guard let latitude = lastLatitude, longitude = lastLongitude else {
            return CLLocation()
        }

        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
}
