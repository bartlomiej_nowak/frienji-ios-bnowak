//
//  Activity.swift
//  Frienji
//
//  Created by Piotr Łyczba on 26/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Activity<Entity: Any> {

    let key: ActivityType
    let entity: Entity?
    let owner: Frienji
    let createdAt: NSDate

    private var any: Activity<Any> {
        return Activity<Any>(key: key, entity: entity, owner: owner, createdAt: createdAt)
    }

}

enum ActivityType: String {

    case IncomingMessage = "receive_message"
    case DeletedMessage = "destroy_message"
    case LikePost = "like_for_post_created"
    case LikeComment = "like_for_comment_created"
    case Comment = "comment_created"
    case CommentOnMyWall = "comment_on_your_wall_created"
    case Post = "post_created"
    case Catch = "saved_created"
    case Reject = "saved_removed"
    case Block = "rejected_created"
    case Unknown = "unknown"

}

// MARK: - Fake

extension Activity: Fakeable {

    static func fake() -> Activity<Any> {
        return Activity<Post>(
            key: .Post,
            entity: Post.fake(),
            owner: Frienji.fake(),
            createdAt: NSDate.randomWithinDaysBeforeToday(365)
        ).any
    }

}

// MARK: - Decode

extension Activity: Decodable {

    static func decode(json: JSON) -> Decoded<Activity<Any>> {
        let key: Decoded<ActivityType> = json <| "key"
        switch key {
        case let .Success(value):
            switch value {
            case .IncomingMessage, .DeletedMessage:
                return decodeMessage(json).map { $0.any }
            case .Comment, .CommentOnMyWall, .Post:
                return decodePost(json).map { $0.any }
            case .LikeComment, .LikePost:
                return decodeLike(json).map { $0.any }
            case .Catch, .Reject, .Block:
                return decodeFrienji(json).map { $0.any }
            default:
                return decodeAny(json)
            }
        case let .Failure(error):
            return .Failure(error)
        }
    }

    static func decodeMessage(json: JSON) -> Decoded<Activity<Message>> {
        return curry(Activity<Message>.init)
            <^> json <| "key"
            <*> json <|? "entity"
            <*> json <| "owner"
            <*> (json <| "created_at" >>- toNSDate)
    }

    static func decodePost(json: JSON) -> Decoded<Activity<Post>> {
        return curry(Activity<Post>.init)
            <^> json <| "key"
            <*> json <|? "entity"
            <*> json <| "owner"
            <*> (json <| "created_at" >>- toNSDate)
    }

    static func decodeFrienji(json: JSON) -> Decoded<Activity<Frienji>> {
        return curry(Activity<Frienji>.init)
            <^> json <| "key"
            <*> json <|? "entity"
            <*> json <| "owner"
            <*> (json <| "created_at" >>- toNSDate)
    }
    
    static func decodeLike(json: JSON) -> Decoded<Activity<Like>> {
        return curry(Activity<Like>.init)
            <^> json <| "key"
            <*> json <|? "entity"
            <*> json <| "owner"
            <*> (json <| ["entity", "created_at"] >>- toNSDate)
    }

    static func decodeAny(json: JSON) -> Decoded<Activity<Any>> {
        return curry(Activity<Any>.init)
            <^> json <| "key"
            <*> pure(nil)
            <*> json <| "owner"
            <*> (json <| "created_at" >>- toNSDate)
    }

}

extension ActivityType: Decodable {

    static func decode(json: JSON) -> Decoded<ActivityType> {
        switch json {
        case let .String(rawValue):
            guard let result = ActivityType(rawValue: rawValue) else {
                return pure(ActivityType.Unknown)
            }
            return pure(result)
        default:
            return pure(ActivityType.Unknown)
        }
    }

}
