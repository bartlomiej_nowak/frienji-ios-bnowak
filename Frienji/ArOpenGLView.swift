//
//  ArOpenGLView.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright Â© 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import OpenGLES
import AVFoundation
import CoreLocation
import CoreMotion
import RxSwift
import RxCocoa

protocol ArOpenGLViewProtocol: class {

    func arOpenGLView(arOpenGLView:ArOpenGLView, didCatchFrenji frenji:Frienji, frienjiBoundingBox boundingBox:CGRect) -> Void
}

class ArOpenGLView : UIView {

    private static let kMinimumObjectDistance: CGFloat = 10

    private static let kMinimumBubbleSizeMultiplier: CGFloat = 0.36
    static let kMaximumBubbleSizeMultiplier: CGFloat = 0.6

    // MARK: Properties
    weak var delegate:ArOpenGLViewProtocol?

    private (set) var renderingActive:Bool = false

    let maximumObjectDistance = Variable<CGFloat>(ArSliderView.kMinSliderDistanceValue)
    
    private var openGLLayer: CAEAGLLayer!
    private var openGLContext: CVEAGLContext!

    private var colorRenderBuffer: GLuint = GLuint()

    private var viewportSize: CGSize = CGSizeZero
    var cameraAngle:CGFloat = 0 {
        didSet {
            self.radarView.setCameraAngle(self.cameraAngle)
        }
    }

    //objects to be drawn
    private var cameraObject:CameraObject!
    private var cameraPositionSlot: GLuint!
    private var cameraTextureCoordinateSlot: GLuint!
    private var cameraTextureUniform: GLuint!
    private var cameraProgramHandle: GLuint!
    private var cameraProgramLinkSuccess = false

    private (set) var bubbleObjects = Set<BubbleObject>()
    private var bubblePositionSlot: GLuint!
    private var bubbleTextureCoordinateSlot: GLuint!
    private var bubbleTextureUniform: GLuint!
    private var bubbleLogoTextureUniform: GLuint!
    private var bubbleFlatImageUniform: GLuint!
    private var bubbleGlowAnimationUniform: GLuint!
    private var bubbleGlowAnimationTimeUniform: GLuint!
    private var bubbleLogoWithoutBubbleUniform: GLuint!
    private var bubbleProgramHandle: GLuint!
    private var bubbleProgramLinkSuccess = false

    private var touchedBubbleObject:BubbleObject?
    
    //radar view
    private (set) var radarView:ArRadarView!

    //other data
    private var userLocation:CLLocation?
    private var userHeading:Double?
    private var userInclination:CGFloat = 0

    private let disposeBag = DisposeBag()
    
    // MARK: Life cycle

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        setUpView()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        setUpView()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.calculateBubbleSizes(animated: false)
    }
    
    // MARK: Set up

    private func setUpView() {
        self.openGLLayer = self.layer as! CAEAGLLayer
        self.openGLLayer.opaque = true
        self.openGLLayer.frame = UIScreen.mainScreen().bounds

        self.contentScaleFactor = UIScreen.mainScreen().scale
        self.contentMode = UIViewContentMode.ScaleAspectFill

        configureContext()
        configureRenderBuffer()
        configureFrameBuffer()

        compileAndLinkShaders()

        setupDisplayLink()

        setUpCameraObject()

        addRadarView()
        
        //force load of explosion frames
        BubbleObject.kBubbleExplosionImages
        
        setUpObservables()
    }

    // MARK: Layer

    override class func layerClass() -> AnyClass {
        // In order for our view to display OpenGL content, we need to set it's
        // default layer to be a CAEAGLayer
        return CAEAGLLayer.self
    }

    private func configureViewPort() {
        let cameraTextureSize = self.cameraObject.getCameraTextureSize()
        
        guard cameraTextureSize.width > 0 && cameraTextureSize.height > 0 else {
            return
        }
        
        let scale = UIScreen.mainScreen().scale
        let ratioWidth = self.frame.size.width / cameraTextureSize.width
        let ratioHeight = self.frame.size.height / cameraTextureSize.height
        
        let width = cameraTextureSize.width * max(ratioWidth, ratioHeight) * scale
        let height = cameraTextureSize.height * max(ratioWidth, ratioHeight) * scale
        
        glViewport(0, 0, GLint(width), GLint(height))
        self.viewportSize = CGSize(width: width, height: height)
    }

    // MARK: State

    func activateRenderer() {
        self.renderingActive = true
    }

    func deactivateRenderer() {
        self.renderingActive = false
    }

    func isOpenGLProgramCorrect() -> Bool {
        return self.cameraProgramLinkSuccess && self.bubbleProgramLinkSuccess
    }
    
    // MARK: Observables
    
    private func setUpObservables() {
        self.maximumObjectDistance.asObservable().observeOn(ConcurrentDispatchQueueScheduler(globalConcurrentQueueQOS: DispatchQueueSchedulerQOS.Background)).bindNext { [unowned self] _ in
            self.calculateBubbleSizes(animated: false)
        }.addDisposableTo(self.disposeBag)
    }

    // MARK: Radar view

    private func addRadarView() {
        self.radarView = ArRadarView()

        let size = UIScreen.mainScreen().bounds.size.width / 5
        let viewSize = CGSizeMake(size, size)
        let padding:CGFloat = 8

        self.addSubviewBottomLeft(self.radarView, withSize: viewSize, padding: padding)
    }

    // MARK: Camera session

    func updateCameraTextureUsingSampleBuffer(sampleBuffer: CMSampleBuffer!) {
        self.cameraObject.updateCameraTextureUsingSampleBuffer(sampleBuffer)
    }

    // MARK: Context

    private func configureContext() {
        //create context
        self.openGLContext = EAGLContext(API: EAGLRenderingAPI.OpenGLES2)

        EAGLContext.setCurrentContext(self.openGLContext)

        glBlendEquation(UInt32(GL_FUNC_ADD))
        glBlendFunc(UInt32(GL_SRC_ALPHA), UInt32(GL_ONE_MINUS_SRC_ALPHA))
        glEnable(UInt32(GL_BLEND))
    }

    // MARK: Buffers

    private func configureRenderBuffer() {
        // A render buffer is an OpenGL objec that stores the rendered image to present to the screen.
        //   OpenGL will create a unique identifier for a render buffer and store it in a GLuint.
        //   So we call the glGenRenderbuffers function and pass it a reference to our colorRenderBuffer.
        glGenRenderbuffers(1, &self.colorRenderBuffer)

        // Then we tell OpenGL that whenever we refer to GL_RENDERBUFFER, it should treat that as our colorRenderBuffer.
        glBindRenderbuffer(UInt32(GL_RENDERBUFFER), self.colorRenderBuffer)

        // Finally, we tell our context that the render buffer for our layer is our colorRenderBuffer.
        self.openGLContext.renderbufferStorage(Int(GL_RENDERBUFFER), fromDrawable: self.openGLLayer)
    }

    private func configureFrameBuffer() {
        // A frame buffer is an OpenGL object for storage of a render buffer... amongst other things (tm).
        //   OpenGL will create a unique identifier for a frame vuffer and store it in a GLuint. So we
        //   make a GLuint and pass it to the glGenFramebuffers function to keep this identifier.
        var frameBuffer: GLuint = GLuint()
        glGenFramebuffers(1, &frameBuffer)

        // Then we tell OpenGL that whenever we refer to GL_FRAMEBUFFER, it should treat that as our frameBuffer.
        glBindFramebuffer(UInt32(GL_FRAMEBUFFER), frameBuffer)

        // Finally we tell the frame buffer that it's GL_COLOR_ATTACHMENT0 is our colorRenderBuffer. Oh.
        glFramebufferRenderbuffer(UInt32(GL_FRAMEBUFFER), UInt32(GL_COLOR_ATTACHMENT0), UInt32(GL_RENDERBUFFER), self.colorRenderBuffer)
    }

    // MARK: Shaders & Program

    private func compileAndLinkShaders() {
        // Compile our vertex and fragment shaders.
        compileAndLinkCameraShaders()
        compileAndLinkBubbleShaders()
    }

    private func compileAndLinkCameraShaders() {
        if let vertexShader = ShaderUtils.compileShaderWithName("CameraVertexShader", shaderType: UInt32(GL_VERTEX_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)),
            let fragmentShader = ShaderUtils.compileShaderWithName("CameraFragmentShader", shaderType: UInt32(GL_FRAGMENT_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)) {

            // Call glCreateProgram, glAttachShader, and glLinkProgram to link the vertex and fragment shaders into a complete program.
            self.cameraProgramHandle = glCreateProgram()
            glAttachShader(self.cameraProgramHandle, vertexShader)
            glAttachShader(self.cameraProgramHandle, fragmentShader)
            glLinkProgram(self.cameraProgramHandle)

            // Check for any errors.
            var linkSuccess: GLint = GLint()
            glGetProgramiv(self.cameraProgramHandle, UInt32(GL_LINK_STATUS), &linkSuccess)

            self.cameraProgramLinkSuccess = linkSuccess != GL_FALSE

            // Finally, call glGetAttribLocation to get a pointer to the input values for the vertex shader, so we
            //  can set them in code. Also call glEnableVertexAttribArray to enable use of these arrays (they are disabled by default).
            self.cameraPositionSlot = UInt32(glGetAttribLocation(self.cameraProgramHandle, "Position"))
            glEnableVertexAttribArray(self.cameraPositionSlot)

            self.cameraTextureCoordinateSlot = UInt32(glGetAttribLocation(self.cameraProgramHandle, "TexCoordIn"))
            glEnableVertexAttribArray(self.cameraTextureCoordinateSlot)

            self.cameraTextureUniform = UInt32(glGetUniformLocation(self.cameraProgramHandle, "Texture"))
        }
    }

    private func compileAndLinkBubbleShaders() {
        if let vertexShader = ShaderUtils.compileShaderWithName("BubbleVertexShader", shaderType: UInt32(GL_VERTEX_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)),
            let fragmentShader = ShaderUtils.compileShaderWithName("BubbleFragmentShader", shaderType: UInt32(GL_FRAGMENT_SHADER), bundle: NSBundle(forClass: ArOpenGLView.self)) {

            // Call glCreateProgram, glAttachShader, and glLinkProgram to link the vertex and fragment shaders into a complete program.
            self.bubbleProgramHandle = glCreateProgram()
            glAttachShader(self.bubbleProgramHandle, vertexShader)
            glAttachShader(self.bubbleProgramHandle, fragmentShader)
            glLinkProgram(self.bubbleProgramHandle)

            // Check for any errors.
            var linkSuccess: GLint = GLint()
            glGetProgramiv(self.bubbleProgramHandle, UInt32(GL_LINK_STATUS), &linkSuccess)

            self.bubbleProgramLinkSuccess = linkSuccess != GL_FALSE

            // Finally, call glGetAttribLocation to get a pointer to the input values for the vertex shader, so we
            //  can set them in code. Also call glEnableVertexAttribArray to enable use of these arrays (they are disabled by default).
            self.bubblePositionSlot = UInt32(glGetAttribLocation(self.bubbleProgramHandle, "Position"))
            glEnableVertexAttribArray(self.bubblePositionSlot)

            self.bubbleTextureCoordinateSlot = UInt32(glGetAttribLocation(self.bubbleProgramHandle, "TexCoordIn"))
            glEnableVertexAttribArray(self.bubbleTextureCoordinateSlot)

            self.bubbleTextureUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "Texture"))
            self.bubbleLogoTextureUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "LogoTexture"))
            
            self.bubbleFlatImageUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "flatImage"))
            
            self.bubbleGlowAnimationUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "glowAnimation"))
            self.bubbleGlowAnimationTimeUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "glowAnimationTime"))
            
            self.bubbleLogoWithoutBubbleUniform = UInt32(glGetUniformLocation(self.bubbleProgramHandle, "logoWithoutBubble"))
        }
    }

    // MARK: Display link

    private func setupDisplayLink() {
        let displayLink = CADisplayLink(target: self, selector: #selector(ArOpenGLView.render(_:)))
        displayLink.addToRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
    }

    // MARK: Render

    func render(displayLink: CADisplayLink) {
        if self.renderingActive && self.cameraProgramLinkSuccess && self.bubbleProgramLinkSuccess {

            configureViewPort()

            //draw camera frames
            glUseProgram(self.cameraProgramHandle)
            self.cameraObject.render(self.cameraPositionSlot, textureCoordinateSlot: self.cameraTextureCoordinateSlot, textureUniform: self.cameraTextureUniform)

            //draw bubbles
            glUseProgram(self.bubbleProgramHandle)

            BubbleObject.activateTextureUnit(self.bubbleLogoTextureUniform)

            //sort by size
            let bubbles = self.bubbleObjects.sort({ (b1:BubbleObject, b2:BubbleObject) -> Bool in
                return b1.bubbleSize.width <= b2.bubbleSize.width && b1.bubbleSize.height <= b2.bubbleSize.height
            })

            for bubble in bubbles {
                bubble.glowAnimationUniform = self.bubbleGlowAnimationUniform
                bubble.glowAnimationTimeUniform = self.bubbleGlowAnimationTimeUniform
                bubble.flatImageUniform = self.bubbleFlatImageUniform
                bubble.logoWithoutBubbleUniform = self.bubbleLogoWithoutBubbleUniform
                
                bubble.render(self.bubblePositionSlot, textureCoordinateSlot: self.bubbleTextureCoordinateSlot, textureUniform: self.bubbleTextureUniform)
                
                //set animation flag to false for next bubble, if bubble is animating then this bubble will change this flag for himself
                glUniform1i(Int32(self.bubbleFlatImageUniform), 0)
                glUniform1i(Int32(self.bubbleGlowAnimationUniform), 0)
                glUniform1i(Int32(self.bubbleLogoWithoutBubbleUniform), 0)
            }

            BubbleObject.destroyLogoTexture()

            //show buffer content
            self.openGLContext.presentRenderbuffer(Int(GL_RENDERBUFFER))
        }
    }

    // MARK: Camera object

    private func setUpCameraObject() {
        self.cameraObject = CameraObject(context: self.openGLContext)
    }

    // MARK: Bubbles objects

    private func updateBubbleObjectsForFrienjis(frienjis:[Frienji]) {
        let frienjisIds = frienjis.map { (frienji:Frienji) -> Int64 in
            return frienji.dbID
        }

        let currentBubbles = self.bubbleObjects

        //remove old ones
        for bubble in currentBubbles {
            let newFrienji = frienjis.filter { $0.dbID == bubble.frenji.dbID }.first
            if !frienjisIds.contains(bubble.frenji.dbID) || newFrienji?.relation != bubble.frenji.relation || newFrienji?.avatar.name != bubble.frenji.avatar.name {
                //not in new traces to show - remove
                self.bubbleObjects.remove(bubble)
            }
        }

        //add new ones
        for frienji in frienjis {
            //check if this trace have already bubble
            if !self.bubbleObjects.contains({ (b:BubbleObject) -> Bool in return b.frenji.dbID == frienji.dbID }) {
                self.bubbleObjects.insert(BubbleObject(frenji: frienji))
            }
        }
    }

    private func calculateBubbleSizes(animated animated:Bool) {
        
        guard let userLocation = self.userLocation else {
            return
        }

        let kMaxBubbleSize = self.frame.size.width * ArOpenGLView.kMaximumBubbleSizeMultiplier
        let kMinBubbleSize = self.frame.size.width * ArOpenGLView.kMinimumBubbleSizeMultiplier
        
        let bubbles = self.bubbleObjects
        for bubble in bubbles {
            var bubbleSize: CGFloat = kMaxBubbleSize
            
            //calculate size not for our frienji
            if Int(bubble.frenji.dbID) != Settings.sharedInstance.userId {
                let bubbleLocation = bubble.frenji.location
                let bubbleDistance = CGFloat(userLocation.distanceFromLocation(bubbleLocation))
                let normalizedBubbleDistance = min(self.maximumObjectDistance.value, max(ArOpenGLView.kMinimumObjectDistance, bubbleDistance))
                
                let calculatedSizeMultiplier = (ArOpenGLView.kMaximumBubbleSizeMultiplier - ArOpenGLView.kMinimumBubbleSizeMultiplier) * normalizedBubbleDistance / self.maximumObjectDistance.value
                bubbleSize = (ArOpenGLView.kMaximumBubbleSizeMultiplier - calculatedSizeMultiplier) * self.frame.size.width
                bubbleSize = min(kMaxBubbleSize, max(kMinBubbleSize, bubbleSize))
            }
            
            bubble.calculateBubbleSizeForDesiredSize(bubbleSize, viewportWidth: self.frame.size.width, viewportHeight: self.frame.size.height, animated: animated)
            bubble.setBubbleVerticalOffsetForSize(bubbleSize, maxSize: kMaxBubbleSize, minSize: kMinBubbleSize)
            bubble.calculateBubbleOffsetMultiplier(bubbleSize, maxSize: kMaxBubbleSize, minSize: kMinBubbleSize)
        }
    }

    private func calculateBubblePositions(animated animated:Bool) {

        guard let userLocation = self.userLocation, let userHeading = self.userHeading else {

            return
        }

        var minFieldOfViewAngle = CGFloat(userHeading) - self.cameraAngle/2
        var maxFieldOfViewAngle = CGFloat(userHeading) + self.cameraAngle/2

        if minFieldOfViewAngle < 0 {

            minFieldOfViewAngle = 360 - abs(minFieldOfViewAngle)
        }

        if maxFieldOfViewAngle > 360 {

            maxFieldOfViewAngle = maxFieldOfViewAngle - 360
        }

        for bubble in self.bubbleObjects {
            let bubbleLocation = bubble.frenji.location
            let bearing = LocationManager.sharedInstance.getBearingBetweenTwoPoints(userLocation, point2: bubbleLocation)
            bubble.calculateBubblePositionForBearing(bearing, minFieldOfViewAngle: minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle, animated: animated)
        }
    }

    private func calculateBubbleYPositions() {
        for bubble in self.bubbleObjects {
            bubble.calculateBubblePositionForYMotion(self.userInclination)
        }
    }

    // MARK: Data

    func updateFrenji(frenji:Frienji) {
        let index = self.bubbleObjects.indexOf { (b:BubbleObject) -> Bool in return frenji.dbID == b.frenji.dbID }
        if let frienjiIndex = index {
            self.bubbleObjects[frienjiIndex].frenji = frenji
        }
    }

    func updateWithTraces(frienjis:[Frienji]) {
        self.updateBubbleObjectsForFrienjis(frienjis)
        
        self.calculateBubbleSizes(animated: true)
        self.calculateBubblePositions(animated: true)
        self.calculateBubbleYPositions()

        self.radarView.updateWithFrenjis(frienjis.filter { Int($0.dbID) != Settings.sharedInstance.userId })
    }

    func updateUserLocation(location:CLLocation?) {
        self.userLocation = location
        
        self.calculateBubbleSizes(animated: true)
        self.calculateBubblePositions(animated: true)

        if let location = self.userLocation {
            self.radarView.setUserLocation(location)
        }
    }

    func updateHeading(heading:Double?) {
        self.userHeading = heading

        self.calculateBubblePositions(animated: false)

        if let heading = self.userHeading {
            self.radarView.setBearing(heading)
        }
    }

    func updateInclination(inclination:CGFloat) {
        self.userInclination = inclination

        self.calculateBubbleYPositions()
    }

    // MARK: Touch
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesBegan(touches, withEvent: event)
    
        guard let touchLocation = touches.first?.locationInView(self) else {
            return
        }
        
        let bubbles = self.bubbleObjects.sort({ (b1:BubbleObject, b2:BubbleObject) -> Bool in
            return b1.bubbleSize.width >= b2.bubbleSize.width && b1.bubbleSize.height >= b2.bubbleSize.height
        })
        
        for bubble in bubbles {
            let boundingBox = bubble.getBoundingBoxForScreenPixelCoordinates(self.frame.size, viewportSize: self.viewportSize)
            
            if boundingBox.contains(touchLocation) && bubble.shouldBubbleBeDrawn {
                //hold reference to touched bubble
                self.touchedBubbleObject = bubble
                
                //bubble touched, play animation
                bubble.playHoldAnimation()
                break
            }
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        
        guard let touchedBubble = self.touchedBubbleObject else {
            return
        }
        
        touchedBubble.stopHoldAnimation()
        
        touchedBubble.playExplosionAnimation({
            let boundingBox = touchedBubble.getBoundingBoxForScreenPixelCoordinates(self.frame.size, viewportSize: self.viewportSize)
            self.delegate?.arOpenGLView(self, didCatchFrenji: touchedBubble.frenji, frienjiBoundingBox: boundingBox)
        })
    }

}

extension ArOpenGLView : ArCameraSessionProtocol {

    func arCameraSession(session: ArCameraSession, didOutputSampleBuffer sampleBuffer: CMSampleBuffer) {
        self.updateCameraTextureUsingSampleBuffer(sampleBuffer)
    }
}
