//
//  ArCameraSession.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import AVFoundation
import RxSwift

protocol ArCameraSessionProtocol: class {

    func arCameraSession(session:ArCameraSession, didOutputSampleBuffer sampleBuffer: CMSampleBuffer) -> Void
}

class ArCameraSession : NSObject, AVCaptureVideoDataOutputSampleBufferDelegate {

    private let kCameraSessionQueueIdentifier = "ArCameraSessionQueue"

    private var captureSession:AVCaptureSession!
    private var videoOutput:AVCaptureVideoDataOutput!
    private var backCamera:AVCaptureDevice?

    private var cameraSessionQueue: dispatch_queue_t!

    weak var delegate:ArCameraSessionProtocol?

    private var isCameraStarting:Bool = false
    private var isCameraStopping:Bool = false

    let permissionAlertShown = Variable<Bool>(false)

    // MARK: Init

    override init() {
        super.init()

        self.captureSession = AVCaptureSession()
        self.captureSession.sessionPreset = AVCaptureSessionPreset1280x720

        self.cameraSessionQueue = dispatch_queue_create(kCameraSessionQueueIdentifier, DISPATCH_QUEUE_SERIAL)

        configureVideoOutput()
        findAvailableCameras()
    }

    // MARK: Permissions

    class func getCameraPermission() -> AVAuthorizationStatus {
        return AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
    }

    private func checkIfUserHasEnabledCameraPermissions() -> Bool {

        let authorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)

        if authorizationStatus != AVAuthorizationStatus.Authorized {

            AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (result:Bool) in
                self.permissionAlertShown.value = true

                if result {

                    dispatch_async(dispatch_get_main_queue()) {

                        self.startCameraSession()
                    }
                }
            })
        }

        return authorizationStatus == AVAuthorizationStatus.Authorized
    }

    // MARK: Setup

    private func configureVideoOutput() {

        self.videoOutput = AVCaptureVideoDataOutput()
        self.videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey : Int(kCVPixelFormatType_32BGRA)]
        self.videoOutput.alwaysDiscardsLateVideoFrames = true

        self.videoOutput.setSampleBufferDelegate(self, queue: self.cameraSessionQueue)

        if self.captureSession.canAddOutput(self.videoOutput) {

            self.captureSession.addOutput(self.videoOutput)
        }
    }

    // MARK: Camera

    private func findAvailableCameras() {

        let devices = AVCaptureDevice.devices()

        for device in devices {

            if device.hasMediaType(AVMediaTypeVideo) && device.position == AVCaptureDevicePosition.Back {

                self.backCamera = device as? AVCaptureDevice
                break
            }
        }
    }

    // MARK: Session

    func startCameraSession() {

        //back camera as default one
        guard let camera = self.backCamera where self.checkIfUserHasEnabledCameraPermissions() && self.captureSession != nil && !self.captureSession.running && !self.isCameraStarting else {

            return
        }

        self.isCameraStarting = true

        do {

            let deviceInput = try AVCaptureDeviceInput(device: camera)

            if self.captureSession.canAddInput(deviceInput) && self.captureSession.inputs.isEmpty {

                self.captureSession.addInput(deviceInput)
            }

            if let videoConnection = self.videoOutput?.connectionWithMediaType(AVMediaTypeVideo) {

                videoConnection.videoOrientation = AVCaptureVideoOrientation.Portrait
            }

            self.captureSession.startRunning()

            self.isCameraStarting = false

        } catch (_) {

            if let controller = UIApplication.sharedApplication().keyWindow?.rootViewController?.presentedViewController ?? UIApplication.sharedApplication().keyWindow?.rootViewController {

                UIAlertUtils.showAlertWithTitle(Localizations.alert.cant_start_camera, fromController: controller, showCompletion: nil)
            }

            self.isCameraStarting = false
        }
    }

    func stopCameraSession() {

        if self.captureSession.running && !self.isCameraStopping {

            self.isCameraStopping = true

            self.captureSession.stopRunning()

            if let inputs = self.captureSession.inputs as? [AVCaptureInput] {
                for input in inputs {

                    self.captureSession.removeInput(input)
                }
            }

            self.isCameraStopping = false
        }
    }

    func getCameraFieldOfView() -> CGFloat {

        guard let camera = self.backCamera else {

            return 0
        }

        return CGFloat(camera.activeFormat.videoFieldOfView)
    }

    // MARK: AVCaptureVideoDataOutputSampleBufferDelegate

    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!) {

        self.delegate?.arCameraSession(self, didOutputSampleBuffer: sampleBuffer)
    }
    
}
