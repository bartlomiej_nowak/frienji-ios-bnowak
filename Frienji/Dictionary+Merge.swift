//
//  Dictionary+Merge.swift
//  Frienji
//
//  Created by Piotr Łyczba on 25/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

func + <K,V>(left: [K : V], right: [K : V]) -> [K : V] {
    var result = [K:V]()

    for (key,value) in left {
        result[key] = value
    }

    for (key,value) in right {
        result[key] = value
    }
    return result
}
