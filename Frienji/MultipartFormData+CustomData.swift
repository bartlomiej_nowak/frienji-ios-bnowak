//
//  MultipartFormData+CustomData.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Alamofire

private let kCompressionQuality: CGFloat = 0.95
private let kMaxImageWidthForPortrait: CGFloat = 720
private let kMaxImageWidthForLandscape: CGFloat = 1280

extension MultipartFormData {

    func appendLocation(latitude: Double?, longitude: Double?) {
        let latitudeData = latitude.map(String.init).flatMap { $0.dataWithUTF8Encoding() }
        let longitudeData = longitude.map(String.init).flatMap { $0.dataWithUTF8Encoding() }
        if let latitudeData = latitudeData, longitudeData = longitudeData {
            appendBodyPart(data: latitudeData, name: "location[latitude]")
            appendBodyPart(data: longitudeData, name: "location[longitude]")
        }
    }

    func appendImage(image: UIImage?, key: String) {
        
        guard let photo = image else {
            return
        }
        
        //scale photo
        let kPhotoMaxWidth:CGFloat = (photo.size.width < photo.size.height ? kMaxImageWidthForPortrait : kMaxImageWidthForLandscape) / UIScreen.mainScreen().scale
        let scaledPhoto = photo.resizeWithWidth(kPhotoMaxWidth)
        
        if let compressedPhoto = UIImageJPEGRepresentation(scaledPhoto, kCompressionQuality) {
            appendBodyPart(data: compressedPhoto, name: key, fileName: "coverImage", mimeType: "image/*")
        }
    }

    func appendText(text: String?, key: String) {
        if let contentData = text?.dataWithUTF8Encoding() {
            appendBodyPart(data: contentData, name: key)
        }
    }

    func appendAvatar(avatar: Avatar?) {
        let nameData = avatar?.name.dataWithUTF8Encoding()
        let skinData = avatar?.skinTone.dataWithUTF8Encoding()
        let typeData = avatar.map { "\($0.type.rawValue)" }.flatMap { $0.dataWithUTF8Encoding() }
        if let nameData = nameData, typeData = typeData, skinData = skinData {
            appendBodyPart(data: nameData, name: "avatar_attributes[avatar_name]")
            appendBodyPart(data: skinData, name: "avatar_attributes[skin_tone]")
            appendBodyPart(data: typeData, name: "avatar_attributes[avatar_type]")
        }
    }

}
