//
//  AttachmentHandler.swift
//  Frienji
//
//  Created by Piotr Łyczba on 19/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

class AttachmentHandler<TargetViewController: UIViewController where TargetViewController: protocol<HasAttachment, HasDisposeBag>> {

    let image = BehaviorSubject<UIImage?>(value: nil)
    let location = BehaviorSubject<CLLocation?>(value: nil)

    private let target: TargetViewController
    private let locationManager = LocationManager.sharedInstance
    private let alert: Observable<UIAlertController>
    private let disposeBag: DisposeBag

    init(targetViewController target: TargetViewController, cameraEnabled: Bool = true, galleryEnabled: Bool = true, locationEnabled: Bool = true) {
        self.target = target
        alert = target.attachmentRequested
            .flatMapLatest { [weak target] _ in
                UIAlertController.rx_createWithParent(target, animated: true)
            }
            .shareReplay(1)
        disposeBag = target.disposeBag

        prepareImage(cameraEnabled, galleryEnabled: galleryEnabled)
        if locationEnabled {
            prepareLocation()
        }
        prepareCancel()
    }

    private func prepareImage(cameraEnabled: Bool, galleryEnabled: Bool) {
        let cameraImage = cameraEnabled ? prepareCameraImage() : Observable.empty()
        let galleryImage = galleryEnabled ? prepareGalleryImage() : Observable.empty()
        Observable.of(cameraImage, galleryImage).merge()
            .map { info in
                info[UIImagePickerControllerOriginalImage] as? UIImage
            }
            .map {
                $0?.resizeWithWidth(UIScreen.mainScreen().bounds.size.width)
            }
            .bindTo(image)
            .addDisposableTo(disposeBag)
    }

    private func prepareCameraImage() -> Observable<[String: AnyObject]> {
        return alert
            .flatMap { $0.rx_action(Localizations.alert.attachment.camera) }
            .flatMapLatest { [weak target] _ in
                UIImagePickerController.rx_createWithParent(target, animated: true) { picker in
                    picker.sourceType = .Camera
                    picker.allowsEditing = false
                }
                .flatMap { $0.rx_didFinishPickingMediaWithInfo }
                .take(1)
            }
    }

    private func prepareGalleryImage() -> Observable<[String: AnyObject]> {
        return alert
            .flatMap { $0.rx_action(Localizations.alert.attachment.gallery) }
            .flatMapLatest { [weak target] _ in
                UIImagePickerController.rx_createWithParent(target, animated: true) { picker in
                    picker.sourceType = .PhotoLibrary
                    picker.allowsEditing = false
                }
                .flatMap { $0.rx_didFinishPickingMediaWithInfo }
                .take(1)
            }
    }

    private func prepareLocation() {
        alert
            .flatMap { $0.rx_action(Localizations.alert.attachment.location) }
            .withLatestFrom(locationManager.currentLocation.asObservable())
            .bindTo(location)
            .addDisposableTo(disposeBag)
    }

    private func prepareCancel() {
        alert
            .flatMap { alert in
                alert.rx_action(Localizations.alert.attachment.cancel, style: .Cancel)
                    .map { _ in alert }
            }
            .subscribeNext {
                $0.dismissViewControllerAnimated(true, completion: nil)
            }
            .addDisposableTo(disposeBag)
    }

}
