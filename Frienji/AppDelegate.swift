//
//  AppDelegate.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import RxSwift

let kDefaultAnimationDuration = 0.33

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var enteringForeground = false

    let notificationTapped = ReplaySubject<AnyObject>.create(bufferSize: 1)
    private let disposeBag = DisposeBag()
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        enableExternalServices()

        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        window?.makeKeyAndVisible()

        let navCont = FrienjiNavigationController()

        self.setupNavigationController(navCont)

        self.window?.rootViewController = navCont

        if let userInfo = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] {
            notificationTapped.onNext(userInfo)
        }
        
        if let shortcutItem = launchOptions?[UIApplicationLaunchOptionsShortcutItemKey] as? UIApplicationShortcutItem {
            AppShortcutUtils.handleActionForAppShortcut(shortcutItem, application: application)
        }
        
        if Settings.sharedInstance.askedForPushPermissions {
            PermissionsHelper.registerForPushNotifications()
        }
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        
        Settings.sharedInstance.userVerified ? AppShortcutUtils.activateAppShortcutItems(application: application) : AppShortcutUtils.deactivateAppShortcutItems(application: application)
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        enteringForeground = true
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        if self.enteringForeground && Settings.sharedInstance.askedForPushPermissions {
            PermissionsHelper.registerForPushNotifications()
        }
        
        enteringForeground = false
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    // MARK: Open URL

    func application(app: UIApplication, openURL url: NSURL, options: [String: AnyObject]) -> Bool {
        return false
    }
    
    // MARK: App shortcut
    
    func application(application: UIApplication, performActionForShortcutItem shortcutItem: UIApplicationShortcutItem, completionHandler: (Bool) -> Void) {
        completionHandler(AppShortcutUtils.handleActionForAppShortcut(shortcutItem, application: application))
    }

    // MARK: Push notifications
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {

        let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>")

        let deviceTokenString: String = (deviceToken.description as NSString)
            .stringByTrimmingCharactersInSet(characterSet)
            .stringByReplacingOccurrencesOfString(" ", withString: "") as String

        print("device token: \(deviceTokenString), not trimmed: \(deviceToken)")

        if Settings.sharedInstance.sessionToken != nil
            && (!Settings.sharedInstance.registeredForPushNotifications || Settings.sharedInstance.pushNotificationsToken != deviceTokenString) {
            Settings.sharedInstance.pushNotificationsToken = deviceTokenString
         
            FrienjiApi.sharedInstance.updatePushToken().bindNext({ 
                Settings.sharedInstance.registeredForPushNotifications = true
            }).addDisposableTo(self.disposeBag)
            
        } else {
            Settings.sharedInstance.pushNotificationsToken = deviceTokenString
        }
    }

    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject: AnyObject]) {
        self.handlePushNotification(userInfo)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        self.handlePushNotification(userInfo)
        
        completionHandler(UIBackgroundFetchResult.NoData)
    }
    
    private func handlePushNotification(userInfo: [NSObject : AnyObject]) {
        let launchingFromBackground = UIApplication.sharedApplication().applicationState != .Active
        if self.enteringForeground && launchingFromBackground {
            self.notificationTapped.onNext(userInfo)
        }
    }

    // MARK: External services

    private func enableExternalServices() {
        // Fabric
        if ConfigurationsHelper.sharedInstance.isRunningInRelease() {
            Fabric.with([Crashlytics.self])
        }
    }

    private func setupNavigationController(navCont: UINavigationController) {
        if self.checkIfUserIsVerified() {
            self.setupNavigationControllerForLoggedInUser(navCont)
        } else {
            self.setupNavigationControllerForNotLoggedInUser(navCont)
        }
    }

    private func checkIfUserIsVerified() -> Bool {
        return Settings.sharedInstance.userVerified
    }

    private func setupNavigationControllerForLoggedInUser(navCont: UINavigationController) {
        let arController = ArViewController.loadFromStoryboard()

        navCont.viewControllers = [arController]
        navCont.delegate = TransitionDelegate.sharedInstance
    }

    private func setupNavigationControllerForNotLoggedInUser(navCont: UINavigationController) {
        let viewCont = IntroductionViewController.loadFromStoryboard()

        navCont.viewControllers = [viewCont]
        navCont.navigationBarHidden = true
    }

}
