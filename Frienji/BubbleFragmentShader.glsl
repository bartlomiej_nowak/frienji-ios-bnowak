precision mediump float;

//camera texture and texture coordinates
uniform sampler2D Texture;
varying vec2 CameraTextureCoord;

//image inside bubble or animation frame
uniform sampler2D LogoTexture;

//bool and time for "touch and hold" animation
uniform bool glowAnimation;
uniform float glowAnimationTime;

//bool if we should draw flat image
uniform bool flatImage;

uniform bool logoWithoutBubble;

//constants
#define FULL_RADIUS 0.5
#define RADIUS 0.4
#define LOGO_RADIUS 0.27
#define CENTER vec2(0.5, 0.5)

#define REFRACTIVE_INDEX 0.71

#define AMBIENT_COLOUR vec3(0.5, 0.5, 0.5)

#define SPECULAR_COLOUR vec3(0.5, 0.5, 0.5)
#define SPECULAR_INTENSITY 6.0

#define DELTA 0.00001

//hold animation constants, glow effect
#define GLOW_COLOR vec3(137.0/255.0, 212.0/255.0, 252.0/255.0)
#define GLOW_RADIUS 1.2
#define GLOW_INTENSITY 0.75

//functions
void drawBubble();
void drawLogoWithoutBubble();
void drawFlatImage();

void drawGlowEffect();
vec3 burn(vec2 p, float size);
float snoise(vec3 uv, float res);

void main() {
    //check what should we draw
    if (flatImage) {
        drawFlatImage();
    } else {
        //draw normal bubble
        if (logoWithoutBubble) {
            drawLogoWithoutBubble();
        } else {
            drawBubble();
        }
        
        if (glowAnimation) {
            drawGlowEffect();
        }
    }
}

// Bubble with glass

void drawBubble() {
    // check for presence in circle
    highp float distanceFromCenter = distance(CENTER, CameraTextureCoord.xy);
    highp float checkForPresenceWithinCircle = 1.0 - smoothstep(RADIUS - DELTA, RADIUS + DELTA, distanceFromCenter);
    
    // helper values
    highp float distanceFromCenterNormalized = distanceFromCenter / RADIUS;
    highp float checkForPresenceWithinLogo = 1.0 - smoothstep(LOGO_RADIUS - DELTA, LOGO_RADIUS + DELTA, distanceFromCenter);
    highp vec2 coordinatesMinusCenter = CameraTextureCoord.xy - CENTER;
    
    // glass without aberation
    highp float normalizedDepth = RADIUS * sqrt(1.0 - distanceFromCenterNormalized * distanceFromCenterNormalized);
    highp vec3 sphereNormal = normalize(vec3(coordinatesMinusCenter, normalizedDepth));
    highp vec3 refractedVector = refract(vec3(0.0, 0.0, -1.0), sphereNormal, REFRACTIVE_INDEX);
    
    highp vec2 finalSphereCoordinate = (refractedVector.xy + 1.0) * 0.5;
    highp vec3 finalSphereColor = texture2D(Texture, finalSphereCoordinate).rgb;
    
    // logo
    highp float imageDistanceFromCenter = distanceFromCenter / LOGO_RADIUS;
    highp float imageNormalizedDepth = LOGO_RADIUS * sqrt(1.0 - imageDistanceFromCenter * imageDistanceFromCenter);
    highp vec3 imageNormal = normalize(vec3(coordinatesMinusCenter, imageNormalizedDepth));
    highp vec3 imageRefractedVector = refract(vec3(0.0, 0.0, -1.0), imageNormal, 0.5);
    imageRefractedVector.xy = -imageRefractedVector.xy;
    highp vec2 finalImageCoordinate = (imageRefractedVector.xy + 1.0) * 0.5;
    
    highp vec4 logoColor = texture2D(LogoTexture, 2.0 * finalImageCoordinate - 0.5);
    
    if(logoColor.a > 0.0) {
        highp float value = 1.0 - smoothstep(0.8, 1.0, imageDistanceFromCenter);
        finalSphereColor = mix(finalSphereColor, logoColor.rgb, value * 0.8);
    }
    
    // ambient light
    finalSphereColor += 0.3 * (1.0 - pow(clamp(dot(vec3(0.0, 0.0, 1.0), sphereNormal), 0.0, 1.0), 0.25)) * AMBIENT_COLOUR;
    
    // specular light
    highp float lightingIntensity = clamp(dot(normalize(vec3(-0.5, -0.5, 1.0)), sphereNormal), 0.0, 1.0);
    lightingIntensity = pow(lightingIntensity, SPECULAR_INTENSITY);
    finalSphereColor += (SPECULAR_COLOUR * lightingIntensity) * 0.3;
    
    // set final pixel color
    if (step(distanceFromCenter, LOGO_RADIUS) > 0.0) {
        gl_FragColor = vec4(finalSphereColor, checkForPresenceWithinLogo);
        
    } else {
        gl_FragColor = vec4(finalSphereColor, checkForPresenceWithinCircle);
    }
}

void drawLogoWithoutBubble() {
    //draw only logo same size as in bubble
    gl_FragColor = texture2D(LogoTexture, 2.0 * CameraTextureCoord.xy - 0.5);
}

// Flat image

void drawFlatImage() {
    //just draw the image
    gl_FragColor = texture2D(LogoTexture, CameraTextureCoord.xy);
}

// Glow effect
// based on: https://www.shadertoy.com/view/XdV3DW

void drawGlowEffect() {
    //check if we are in place where glow effect shoud be
    float distanceFromCenter = distance(CENTER, CameraTextureCoord.xy);
    float minRadius = FULL_RADIUS - FULL_RADIUS * 0.2;
    
    if (step(distanceFromCenter, minRadius) <= 0.0) {
        //draw glow outside
        vec2 p = CameraTextureCoord.xy - 0.5;
        
        vec3 burnColor = burn(p, GLOW_RADIUS);
        vec3 glowColor = burnColor * GLOW_COLOR * GLOW_INTENSITY;
        
        float checkForPresenceWithinCircle = 1.0 - smoothstep(minRadius, FULL_RADIUS, distanceFromCenter);
        
        gl_FragColor = vec4(glowColor, 1.0 * checkForPresenceWithinCircle);
    }
}

vec3 burn(vec2 p, float size) {
    float color = size*4.-3.*length(2.5*p);
    vec3 coord = vec3(atan(p.x,p.y)/6.2832+.5, length(p)*.4, .5);
    for(int i = 1; i <= 3; i++) {
        float power = exp2(float(i));
        color += 0.2*(1.5 / power) * snoise(coord + vec3(0.,-glowAnimationTime*.05, -glowAnimationTime*.01), power*16.);
    }
    
    color *= GLOW_INTENSITY;
    
    return vec3(color);
}

float snoise(vec3 uv, float res) {
    const vec3 s = vec3(1e0, 1e2, 1e3);
    
    uv *= res;
    vec3 uv0 = floor(mod(uv, res))*s;
    vec3 uv1 = floor(mod(uv+1., res))*s;
    
    vec3 f = fract(uv);
    f = f*f*(3.0-2.0*f);
    
    vec4 v = vec4(uv0.x+uv0.y+uv0.z, uv1.x+uv0.y+uv0.z,
                  uv0.x+uv1.y+uv0.z, uv1.x+uv1.y+uv0.z);
    vec4 r = fract(sin(v*1e-1)*1e3);
    
    float r0 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);
    r = fract(sin((v + uv1.z - uv0.z)*1e-1)*1e3);
    float r1 = mix(mix(r.x, r.y, f.x), mix(r.z, r.w, f.x), f.y);
    
    return mix(r0, r1, f.z)*2.-1.;
}
