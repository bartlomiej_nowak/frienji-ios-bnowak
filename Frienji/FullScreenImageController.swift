//
//  FullScreenImageController.swift
//  Frienji
//
//  Created by Adam Szeremeta on 30.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class FullScreenImageController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "FullScreen"
    
    //outlets
    @IBOutlet weak var imageView: UIImageView!
    
    //properties
    var image:UIImage?
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addPanGesture()
        
        self.imageView.image = image
    }
    
    // MARK: Pan gesture
    
    private func addPanGesture() {
        
        guard let manager = self.transitioningDelegate as? ImageTransitionManager else {
            return
        }

        let gesture = UIPanGestureRecognizer(target: manager, action: Selector("interactiveGestureHandler:"))
        self.imageView.addGestureRecognizer(gesture)
    }
    
}
