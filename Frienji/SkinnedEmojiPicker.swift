//
//  SkinnedEmojiPicker.swift
//  Frienji
//
//  Created by adam kolodziej on 21.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

protocol SkinnedEmojiPickerDelegate {
    
    func skinnedEmojiPicker(picker: SkinnedEmojiPicker, didSelectEmoji emoji: String, withSkinTone skinTone: String)
}

class SkinnedEmojiPicker: UIViewController, StoryboardLoad {
    
    static var storyboardId = "Introductions"
    static var storyboardControllerId = "SkinnedEmojiPicker"
    
    var emoji: String?
    var delegate: SkinnedEmojiPickerDelegate?

    override var preferredContentSize: CGSize {
        get {
            return CGSizeMake(UIScreen.mainScreen().bounds.size.width, SkinnedEmojiPicker.sizeForSkinnedEmojiCell().height)
        }
        set {
            super.preferredContentSize = newValue
        }
    }
    
    // MARK: Cell size
    
    class func sizeForSkinnedEmojiCell() -> CGSize {
        let padding: CGFloat = 8
        
        let width = floor((UIScreen.mainScreen().bounds.size.width - 2 * padding) / CGFloat(String.emojiSkinToneModifiers.count + 1))
        return CGSizeMake(width, width)
    }

}

extension SkinnedEmojiPicker: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return String.emojiSkinToneModifiers.count + 1
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(SkinnedEmojiCollectionViewCell.identifier, forIndexPath: indexPath)
        
        guard let skinnedEmojiCell = cell as? SkinnedEmojiCollectionViewCell, let emoji = emoji else {
            return cell
        }
        
        skinnedEmojiCell.emojiLabel.text = emoji + skinTone(atIndex: indexPath.row)
        
        return skinnedEmojiCell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        collectionView.deselectItemAtIndexPath(indexPath, animated: true)
        
        self.dismissViewControllerAnimated(true) { [unowned self] in
            
            guard let emoji = self.emoji else {
                assert(false, "No emoji provided to the picker")
                return
            }
            
            self.delegate?.skinnedEmojiPicker(self, didSelectEmoji: emoji, withSkinTone: self.skinTone(atIndex: indexPath.row))
        }
    }
    
    private func skinTone(atIndex index: Int) -> String {
        return index == 0 ? "" : String.emojiSkinToneModifiers[index - 1]
    }
}

extension SkinnedEmojiPicker: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return SkinnedEmojiPicker.sizeForSkinnedEmojiCell()
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
}

