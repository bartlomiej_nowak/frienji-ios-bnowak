//
//  PushNotificationPermissionViewController.swift
//  Frienji
//
//  Created by bolek on 01.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class PushNotificationPermissionViewController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "PushNotificationPermissionViewController"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(!PermissionsHelper.isRegisterdForPushNotifications()) {
            PermissionsHelper.registerForPushNotifications()
        }
    }

    //MARK: - IBActions
    
    @IBAction func nextBtnClicked(sender:AnyObject) {
        if(!PermissionsHelper.hasGrantedLocationPermission()) {
            self.performSegueWithIdentifier("locationPermissionSegue", sender: nil)
        } else if(Settings.sharedInstance.userVerified) {
            self.performSegueWithIdentifier("arSegue", sender: nil)
        } else {
            self.performSegueWithIdentifier("signInOrUpSegue", sender: nil)
        }
    }
}
