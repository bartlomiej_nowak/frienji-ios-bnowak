//
//  FrienjiNavigationController.swift
//  Traces
//
//  Created by Adam Szeremeta on 21.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class FrienjiNavigationController : UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.interactivePopGestureRecognizer?.delegate = self
    }
}

extension FrienjiNavigationController: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        //enabled only for deeper navigation, two screens from AR
        return self.viewControllers.count > 2
    }
}
