//
//  MutableExplorationViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 11/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import Argo

let kLocationChangeTimeInterval = 2.0

class ExplorationViewModel {

    // MARK: - Output variables

    var frienjisAround = Variable<[Frienji]>([])
    var darkBackgroundVisible = Variable<Bool>(false)

    // MARK: - Inputs

    let frienjisDistance = Variable<CGFloat>(ArSliderView.kMinSliderDistanceValue)
    
    let catched = PublishSubject<Frienji>()
    let rejected = PublishSubject<Frienji>()
    let blocked = PublishSubject<Frienji>()

    let previewTapped = PublishSubject<(Frienji, CGRect)>()
    let myWallTapped = PublishSubject<Void>()
    let activitiesTapped = PublishSubject<Void>()
    let shouldReloadFrienji = PublishSubject<Void>()

    let disposeBag = DisposeBag()

    private let navigationHandler: NavigationHandler
    private let settings: Settings
    private let api: FrienjiApi
    private let locationManager: LocationManager
    private let notificationHandler: NotificationHandler
    private let transitionManager = BubblePopupAnimator()

    // MARK: - Dependencies

    init(
        navigationHandler: NavigationHandler,
        settings: Settings = Settings.sharedInstance,
        api: FrienjiApi = FrienjiApi.sharedInstance,
        locationManager: LocationManager = LocationManager.sharedInstance,
        notificationHandler: NotificationHandler = NotificationHandler()
        ) {
        self.navigationHandler = navigationHandler
        self.settings = settings
        self.api = api
        self.locationManager = locationManager
        self.notificationHandler = notificationHandler

        prepareLoadFrienjisAround()
        prepareUpdateLocation()
        prepareUpdateFrienjisAround()
        prepareSwitchTranluscientBackground()
        prepareNavigation()
    }

    private func prepareLoadFrienjisAround() {
        shouldReloadFrienji.asObservable().withLatestFrom(locationManager.currentLocation.asObservable())
            .flatMap { [unowned self] location -> Observable<[Frienji]> in
                guard let location = location else {
                    return Observable.empty()
                }
            
                let ditance = self.frienjisDistance.value
                return self.api.getFrienjisAround(location.coordinate.latitude, longitude: location.coordinate.longitude, distance: ditance)
            }
            .map { $0.filter { $0.relation != .Blocked} }
            .bindTo(frienjisAround)
            .addDisposableTo(disposeBag)
        
        locationManager.currentLocation.asObservable()
            .throttle(kLocationChangeTimeInterval, scheduler: MainScheduler.instance)
            .flatMap { [unowned self] location -> Observable<[Frienji]> in
                guard let location = location else {
                    return Observable.empty()
                }

                let ditance = self.frienjisDistance.value
                return self.api.getFrienjisAround(location.coordinate.latitude, longitude: location.coordinate.longitude, distance: ditance)
            }
            .map { $0.filter { $0.relation != .Blocked} }
            .bindTo(frienjisAround)
            .addDisposableTo(disposeBag)
    }

    private func prepareUpdateLocation() {
        locationManager.currentLocation.asObservable()
            .throttle(kLocationChangeTimeInterval, scheduler: MainScheduler.instance)
            .flatMap { [unowned self] location -> Observable<Frienji> in
                guard let location = location else {
                    return Observable.empty()
                }

                return self.api.updateLocation(location.coordinate.latitude, longitude: location.coordinate.longitude)
            }
            .subscribe()
            .addDisposableTo(disposeBag)
    }

    private func prepareUpdateFrienjisAround() {
        let catched = self.catched
            .flatMap(api.saveFrienji)
            .withLatestFrom(self.catched)
        let rejected = self.rejected
            .flatMap(api.rejectFrienji)
            .withLatestFrom(self.rejected)

        Observable.of(catched, rejected, blocked).merge().bindNext({ [unowned self] (frienji:Frienji) in
            if let index = self.frienjisAround.value.indexOf(frienji) {
                self.frienjisAround.value[index] = frienji
            }
        }).addDisposableTo(disposeBag)
        
        //navigate to user wall
        self.catched.bindTo(navigationHandler.segue(R.segue.explorationViewController.showFrienji.identifier, destinationType: WallPostsViewController.self)) { input, user in
            input.loaded.onNext(user)
        }.addDisposableTo(disposeBag)
    }

    private func prepareSwitchTranluscientBackground() {
        previewTapped
            .map { _ in true }
            .bindTo(darkBackgroundVisible)
            .addDisposableTo(disposeBag)
        Observable.of(self.catched, self.rejected).merge()
            .map { _ in false }
            .bindTo(darkBackgroundVisible)
            .addDisposableTo(disposeBag)
    }

    private func prepareNavigation() {
        //show popup
        previewTapped.bindNext { [unowned self] (frienji, rect ) in
            self.transitionManager.startingFrame = rect
            let controller = PreviewViewController.loadFromStoryboard()
            controller.input.loaded.onNext(frienji)
            controller.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
            controller.transitioningDelegate = self.transitionManager
            
            self.navigationHandler.sourceViewController?.navigationController?.presentViewController(controller, animated: true, completion: nil)
        }.addDisposableTo(disposeBag)
        myWallTapped
            .flatMap { [unowned self] in
                self.settings.rx_userFrienji
            }
            .bindTo(navigationHandler.segue(R.segue.explorationViewController.showMyWall.identifier, destinationType: WallViewController.self)) { input, user in
                input.loaded.onNext(user)
            }
            .addDisposableTo(disposeBag)
        let notificationTapped = notificationHandler.notificationTapped.map { _ in }
        Observable.of(activitiesTapped, notificationTapped).merge()
            .bindTo(navigationHandler.segue(R.segue.explorationViewController.showActivities.identifier, destinationType: ActivitiesViewController.self))
            .addDisposableTo(disposeBag)
    }

}
