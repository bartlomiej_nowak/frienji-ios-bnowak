//
//  UIAlertController+RxCreate.swift
//  Frienji
//
//  Created by Piotr Łyczba on 19/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

extension UIAlertController {

    static func rx_createWithParent(parent: UIViewController?, animated: Bool = true, configureAlertController: (UIAlertController) throws -> () = { x in }) -> Observable<UIAlertController> {

        return Observable.create { [weak parent] observer in
            let alert = UIAlertController()
            do {
                try configureAlertController(alert)
            } catch let error {
                observer.onError(error)

                return NopDisposable.instance
            }

            guard let parent = parent else {
                observer.onCompleted()

                return NopDisposable.instance
            }

            observer.onNext(alert)

            return Observable.just().subscribeNext { _ in parent.presentViewController(alert, animated: animated, completion: nil) }
        }

    }

    func rx_action(title: String?, style: UIAlertActionStyle = .Default) -> Observable<UIAlertAction> {
        let actionSubject = PublishSubject<UIAlertAction>()
        addAction(UIAlertAction(title: title, style: style) { action in actionSubject.onNext(action) })

        return actionSubject.asObservable()
    }

}
