//
//  UIViewController+StoryboardLoad.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardLoad: class {
    
    /** The storyboard idendifier */
    static var storyboardId: String {get}
    
    /** The controller identifier in the storyboard */
    static var storyboardControllerId: String {get}
    
}

extension StoryboardLoad where Self: UIViewController {
    
    static var storyboardControllerId: String {
        
        return String(Self)
    }
    
    static func loadFromStoryboard() -> Self {
        
        let bundle = NSBundle(forClass: Self.self)
        let storyboard = UIStoryboard(name: Self.storyboardId, bundle: bundle)
        let vc = storyboard.instantiateViewControllerWithIdentifier(Self.storyboardControllerId) as! Self
        
        return vc
    }

    var isModal: Bool {
        // If we are a child view controller, we need to check our parent's presentation
        // rather than our own.  So walk up the chain until we don't see any parentViewControllers
        var potentiallyPresentedViewController : UIViewController = self
        while (potentiallyPresentedViewController.parentViewController != nil) {
            potentiallyPresentedViewController = potentiallyPresentedViewController.parentViewController!
        }

        if self.presentingViewController?.presentedViewController == potentiallyPresentedViewController {
            return true
        }

        if let navigationController = potentiallyPresentedViewController.navigationController {
            if navigationController.presentingViewController?.presentedViewController == navigationController {
                return true
            }
        }

        return potentiallyPresentedViewController.tabBarController?.presentingViewController is UITabBarController
    }
    
}
