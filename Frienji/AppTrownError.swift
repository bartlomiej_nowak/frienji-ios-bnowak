//
//  ErrorsThrown.swift
//  Frienji
//
//  Created by bolek on 24.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

enum AppTrownError:ErrorType
{
    case UnableToStartCameraSession
}