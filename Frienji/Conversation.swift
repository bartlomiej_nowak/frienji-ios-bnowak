//
//  Conversation.swift
//  Frienji
//
//  Created by Piotr Łyczba on 16/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Conversation: Equatable {

    let id: Int
    let lastMessage: Message?
    let receiver: Frienji
    let unreadCount: Int

}

// MARK: - Fake

extension Conversation: Fakeable {

    static func fake() -> Conversation {
        return Conversation(
            id: faker.number.increasingUniqueId(),
            lastMessage: Message.fake(),
            receiver: Frienji.fake(),
            unreadCount: faker.number.randomInt(min: 0, max: 50)
        )
    }

}

// MARK: - Decode

extension Conversation: Decodable {

    static func decode(json: JSON) -> Decoded<Conversation> {
        return curry(Conversation.init)
            <^> json <| "id"
            <*> json <|? "last_message"
            <*> json <| "receiver"
            <*> json <| "unread_count"
    }

}

func == (lhs: Conversation, rhs: Conversation) -> Bool {
    return lhs.id == rhs.id
}
