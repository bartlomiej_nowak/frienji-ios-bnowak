//
//  PhoneNumberViewModel.swift
//  Frienji
//
//  Created by bolek on 22.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import libPhoneNumber_iOS

enum PhoneNumberViewEntryType :Int
{
    case SignUp = 0
    case SignIn = 1
}

class PhoneNumberViewModel {
    
    static let kZeroOptionalPrefix = "(0)"
    static let kUKPhoneCountryCode = 44
    
    var entryType : PhoneNumberViewEntryType!
    var wasRegistractionSuccessfull:Bool = false
    
    var phoneNumber:String!
    var countryCode:String!
    var countryPhoneCode:Int!
    var countryName:String!
    var formatedPhoneNumber:String!
    
    let disposeBag = DisposeBag()
    
    func phoneNumberStringWithOptionalUKPrefix(userInput:String) -> String {
        let inputWithoutOptionalZero = userInput.stringByReplacingOccurrencesOfString(PhoneNumberViewModel.kZeroOptionalPrefix, withString: "")
        self.phoneNumber = inputWithoutOptionalZero

        guard self.countryPhoneCode == PhoneNumberViewModel.kUKPhoneCountryCode && !inputWithoutOptionalZero.isEmpty else {
            return inputWithoutOptionalZero
        }
        
        return "\(PhoneNumberViewModel.kZeroOptionalPrefix)\(inputWithoutOptionalZero)"
    }
    
    func isPhoneNumberValid( completion:(isValid:Bool)->Void)
    {
        PhoneNumberUtils.validatePhonuNumber(self.phoneNumber, countryCode: self.countryCode, countryPhoneCode: self.countryPhoneCode)
        { (isValid, formatedPhoneNumber) in
            if let formatedPhoneNumberUnwrapped = formatedPhoneNumber {
                self.formatedPhoneNumber = formatedPhoneNumberUnwrapped
            }
            completion(isValid: isValid)
        }
    }
    
    func updateCoutryCode(ISOCountryCode:String) -> String
    {
        self.countryCode = ISOCountryCode
        self.countryPhoneCode = NBPhoneNumberUtil.sharedInstance().getCountryCodeForRegion(ISOCountryCode).integerValue
        
        self.countryName = NSLocale.currentLocale().displayNameForKey(NSLocaleCountryCode, value: ISOCountryCode) as String!
        
        return Localizations.phoneNumber.contryCode(self.countryPhoneCode!, self.countryName!)
    }
    
    func getTitleForCoutryCodePickerRow(row:Int)->String
    {
        let countryCode = NSLocale.ISOCountryCodes()[row]
        let countryPhoneCode = NBPhoneNumberUtil.sharedInstance().getCountryCodeForRegion(countryCode)
        let countryName = NSLocale.currentLocale().displayNameForKey(NSLocaleCountryCode, value: countryCode)
        
        return "+\(countryPhoneCode) - \(countryName!)"
    }
    
    var getNumberOfComponentsForCoutryCodePicker : Int {
        get {
            return 1
        }
    }
    
    func getNumberOfRowsForCoutryCodePicker(component:Int) -> Int
    {
        return NSLocale.ISOCountryCodes().count
    }
    
    func getDefaultCoutryCodeBasedOnLocale() ->Int
    {
        var countryCode: String? = NSLocale.currentLocale().objectForKey(NSLocaleCountryCode) as? String
        if(countryCode == nil)
        {
            countryCode = NSLocale(localeIdentifier: NSLocale.preferredLanguages().first!).objectForKey(NSLocaleCountryCode) as? String
        }
        
        let range = NSLocale.ISOCountryCodes().count - 1
        for i in 0...range
        {
            if(NSLocale.ISOCountryCodes()[i] == countryCode!)
            {
                self.updateCoutryCode(countryCode!)
                return i
            }
        }
        return 0
    }
    
    func storeUserInfo()
    {
        Settings.sharedInstance.userCountryCode = self.countryCode
        Settings.sharedInstance.userCountryPhoneCode = "+\(self.countryPhoneCode)"
        Settings.sharedInstance.userCountryName = self.countryName
        Settings.sharedInstance.userPhoneNumber = self.phoneNumber.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
    
    
    func createAccountAndSendSmsCode(controller:UIViewController, completion:(success:Bool)->Void)
    {
        let activityView = ActivityViewController.getActivityInFullScreen("☎️", inView: controller.view)
        
        FrienjiApi.sharedInstance.signUp()
            .subscribe(
                onNext: { [weak self] accountCreated in
                    self?.wasRegistractionSuccessfull = accountCreated
                    self?.sendSmsCode(controller, completion: completion)
                    activityView.hide()
                },
                onError: { [weak self] error in
                    activityView.hide()
                    self?.showErrorAlert(controller, apiError: FrienjiApiError.fromError(error))
                    completion(success: false)
                }
        )
        .addDisposableTo(disposeBag)
    }
    
    func sendSmsCode(controller:UIViewController, completion:(success:Bool)->Void)
    {
        let activityView = ActivityViewController.getActivityInFullScreen("☎️", inView: controller.view)
        
        FrienjiApi.sharedInstance.sendLoginCode()
            .subscribe(
                onNext: { //[activityView, completion] in
                    activityView.hide()
                    completion(success: true)
                },
                onError: { error in
                    activityView.hide()
                    if let apiError = error as? ApiError where apiError.statusCode == 404 {
                        UIAlertUtils.showAlertWithTitle(Localizations.api_error.notFoundPhoneNumber, fromController: controller, showCompletion: nil)
                        completion(success: false)
                    } else {
                        UIAlertUtils.showAlertWithTitle(FrienjiApiError.fromError(error).message, fromController: controller, showCompletion: nil)
                        completion(success: false)
                    }
                    
                }
        )
        .addDisposableTo(disposeBag)
    }
    
    private func showErrorAlert(controller:UIViewController, apiError:FrienjiApiError) {
        //check if phone is taken
        if let codes = apiError.apiErrorCodes where codes.contains(FrienjiApiError.ApiErrorCode.PhoneNumberTaken) {
            UIAlertUtils.showAlertWithTitle(apiError.message, message: nil, positiveButton: Localizations.signUp.login_on_frenji, fromController: controller, showCompletion: nil, actionHandler: {
                //pop to login/sign up
                if let signInOrUpController = (controller.navigationController?.viewControllers.filter { $0 is SignInOrUpViewController })?.first {
                    controller.navigationController?.popToViewController(signInOrUpController, animated: true)
                } else {
                    controller.navigationController?.popToRootViewControllerAnimated(true)
                }
            })
            
        } else {
            UIAlertUtils.showAlertWithTitle(apiError.message, fromController: controller, showCompletion: nil)
        }
    }
}
