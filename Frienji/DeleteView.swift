//
//  DeleteView.swift
//  Frienji
//
//  Created by Adam Szeremeta on 16.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

protocol DeleteViewProtocol: class {
    
    func deleteViewDidTouchDelete() -> Void
}

@IBDesignable
class DeleteView: UIView, NibInstantiate {
    
    weak var delegate: DeleteViewProtocol?
    
    // MARK: Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        loadViewFromNib()
    }
    
    private func loadViewFromNib() {
        let view = self.instantiateFromNib()
        self.addSubviewFullscreen(view)
    }
    
    // MARK: Actions
    
    @IBAction func onDeleteButtonTouch(sender: AnyObject) {
        self.delegate?.deleteViewDidTouchDelete()
    }
    
}
