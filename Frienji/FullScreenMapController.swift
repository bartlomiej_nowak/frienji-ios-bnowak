//
//  FullScreenMapController.swift
//  Frienji
//
//  Created by Adam Szeremeta on 02.01.2017.
//  Copyright © 2017 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit

class FullScreenMapController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "FullScreen"
    
    //outlets
    @IBOutlet weak var mapView: MKMapView!
    
    //properties
    private let coordinateSpanDelta = 0.01
    var pinLocation: CLLocationCoordinate2D?
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addMapLocationPin()
        setMapVisibleRegion()
    }

    // MARK: Actions
    
    @IBAction func onCloseButtonTouch(sender: AnyObject) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: Location pin
    
    private func addMapLocationPin() {
        
        guard let location = self.pinLocation else {
            return
        }
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location
        
        self.mapView.addAnnotation(annotation)
    }
    
    private func setMapVisibleRegion() {
        
        guard let location = self.pinLocation else {
            return
        }
        
        let span = MKCoordinateSpan(latitudeDelta: self.coordinateSpanDelta, longitudeDelta: self.coordinateSpanDelta)
        let region = self.mapView.regionThatFits(MKCoordinateRegion(center: location, span: span))
        
        self.mapView.setRegion(region, animated: true)
    }
    
}
