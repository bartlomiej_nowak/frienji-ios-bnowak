//
//  AnimatorPresent.swift
//  Frienji
//
//  Created by bolek on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AnimatorPresent: NSObject, UIViewControllerAnimatedTransitioning, CAAnimationDelegate
{
    let kDuration : Double = 0.25
    var isPresenting : Bool = false
    
    private let kAnimationTypeName = "dismiss"
    private let kSxTransform:CGFloat = 1.4
    private let kSyTransform:CGFloat = 1.4
    private let kSzTransform:CGFloat = 1.0
    
    //MARK: - UIViewControllerAnimatedTransitioning
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval
    {
        return self.kDuration
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning)
    {
        if(self.isPresenting == true)
        {
            self.presentAnimation(transitionContext)
        }
        else
        {
            self.dismissAnimation(transitionContext)
        }
    }
    
    //MARK: - Private methods
    
    private func presentAnimation(transitionContext: UIViewControllerContextTransitioning)
    {
        guard
            let container : UIView = transitionContext.containerView(),
            let viewFrom : UIView = transitionContext.viewForKey(UITransitionContextFromViewKey),
            let viewTo : UIView = transitionContext.viewForKey(UITransitionContextToViewKey),
            let viewControllerTo = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)
            else {return}
        
        container.backgroundColor = UIColor.clearColor()
        
        viewFrom.frame = container.frame
        container.addSubview(viewFrom)
        
        viewTo.frame = container.frame
        container.addSubview(viewTo)
        
        if viewControllerTo is UINavigationController {
            viewTo.alpha = 0.0
            viewTo.transform = CGAffineTransformMakeScale(self.kSxTransform, self.kSyTransform)
            
            UIView.animateWithDuration(self.kDuration, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
                viewTo.alpha = 1.0
                viewTo.transform = CGAffineTransformIdentity
                }, completion: { (finished) in
                    transitionContext.completeTransition(true)
            })

            return
        }
        
        viewControllerTo.view.frame = CGRectMake(0.0, viewControllerTo.view.frame.size.height, viewControllerTo.view.frame.size.width, viewControllerTo.view.frame.size.height)
        
        UIView.animateWithDuration(self.kDuration, animations: {
            viewControllerTo.view.frame = viewControllerTo.view.bounds
            }) { (finished) in
                transitionContext.completeTransition(true)
        }
    }
    
    private func dismissAnimation(transitionContext: UIViewControllerContextTransitioning)
    {
        guard
            let container : UIView = transitionContext.containerView(),
            let viewTo : UIView = transitionContext.viewForKey(UITransitionContextToViewKey),
            let viewFrom : UIView = transitionContext.viewForKey(UITransitionContextFromViewKey),
            let viewControllerFrom = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)
            else {return}
        
        container.backgroundColor = UIColor.clearColor()
        
        viewTo.frame = container.frame
        container.addSubview(viewTo)
        
        viewFrom.frame = container.frame
        container.addSubview(viewFrom)
        
        if viewControllerFrom is UINavigationController {
            let animationScale = CABasicAnimation(keyPath: "transform.scale")
            animationScale.fromValue = NSValue(CATransform3D: CATransform3DIdentity)
            animationScale.toValue = NSValue(CATransform3D: CATransform3DMakeScale(self.kSxTransform, self.kSyTransform, self.kSxTransform))
            
            let animationOpacity = CABasicAnimation(keyPath: "opacity")
            animationOpacity.fromValue = 1.0
            animationOpacity.fromValue = 0.0
            
            let animationGroup = CAAnimationGroup()
            animationGroup.animations = [animationScale, animationOpacity]
            animationGroup.duration = self.kDuration
            animationGroup.fillMode = kCAFillModeForwards
            animationGroup.removedOnCompletion = false
            animationGroup.delegate = self
            animationGroup.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseIn)
            animationGroup.setValue(self.kAnimationTypeName, forKey: "name")
            animationGroup.setValue(transitionContext, forKey: "transitionContext")
            animationGroup.setValue(viewFrom, forKey: "view")
            
            viewFrom.layer.addAnimation(animationGroup, forKey: nil)
            
            return
        }
        
        UIView.animateWithDuration(self.kDuration, animations: {
            viewControllerFrom.view.frame = CGRectMake(0.0, viewControllerFrom.view.frame.size.height, viewControllerFrom.view.frame.size.width, viewControllerFrom.view.frame.size.height)
        }) { (finished) in
            transitionContext.completeTransition(true)
        }
    }
    
    func animationDidStop(anim: CAAnimation, finished flag: Bool)
    {
        if let animationName = anim.valueForKey("name") as? String where animationName == self.kAnimationTypeName {
            if let view = anim.valueForKey("view") as? UIView {
                view.layer.removeAllAnimations()
                if let transitionContext = anim.valueForKey("transitionContext") {
                    transitionContext.completeTransition(true)
                }
            }
        }
    }
}
