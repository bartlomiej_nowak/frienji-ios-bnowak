//
//  AnyClass+CellReuseIdentifier.swift
//  Frienji
//
//  Created by Adam Szeremeta on 21.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

protocol CellReuseIdentifier {
    static var reuseIdentifier: String {get}
    
}

extension CellReuseIdentifier {
    
    static var reuseIdentifier: String {
        return String(Self)
    }
    
}
