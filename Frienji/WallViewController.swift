//
//  WallViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import CoreLocation
import MapKit

struct WallInput: Input {
    let loaded = ReplaySubject<Frienji>.create(bufferSize: 1)
}

class WallViewController: BaseMessageViewController, HasInput {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var navigationTitleView: NavigationTitleView!

    // MARK: - Observables

    var input = WallInput()
    let viewAppeared = PublishSubject<Void>()
    let textSent = PublishSubject<String>()
    let postCommented = PublishSubject<Post>()
    let postLiked = PublishSubject<Post>()
    let postDisliked = PublishSubject<Post>()
    let postReported = PublishSubject<Post>()
    var navigationHandler: NavigationHandler!
    
    let attachmentBehavior = BehaviorSubject<UIImage?>(value: nil)
    let locationBehavior = BehaviorSubject<CLLocation?>(value: nil)
    
    let attachment = Variable<UIImage?>(nil)
    let location = Variable<CLLocation?>(nil)
    
    var viewModel: Observable<WallViewModel> {
        return Observable.empty()
    }
    
    private var postsCount = 0
    
    private var transitionManager: ImageTransitionManager?

    // MARK: - View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        navigationHandler = NavigationHandler(sourceViewController: self)

        configureView()
        createBindings()
        configureNavigationTitleView()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let chatViewController = segue.destinationViewController as? ChatViewController else {
            return
        }
        
        input.loaded.asObservable().subscribeNext({ [unowned self] frienji in
            FrienjiApi.sharedInstance.createConversation(frienji).showAlertOnApiError(self).catchError { _ in
                Observable.empty()
            }.asObservable().subscribeNext({ [weak chatViewController] conversation in
                chatViewController?.setConversation(conversation)
            }).addDisposableTo(self.disposeBag)
        }).addDisposableTo(self.disposeBag)
    }

    // MARK: - Bindings

    func createBindings() {
        
        input.loaded.asObservable().take(1).bindNext { frienji in
            if frienji == Settings.sharedInstance.userFrienji {
                if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.UserWall) {
                    let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.UserWall) {
                        Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.UserWall)
                    }
                    coachmark.showWithAnimationInContainer()
                }
            } else {
                if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.FrienjiWall) {
                    let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.FrienjiWall) {
                        Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.FrienjiWall)
                    }
                    coachmark.showWithAnimationInContainer()
                }
            }
        }.addDisposableTo(disposeBag)
        
        attachment.asObservable().bindTo(attachmentBehavior).addDisposableTo(disposeBag)
        location.asObservable().bindTo(locationBehavior).addDisposableTo(disposeBag)

        let viewModel = self.viewModel.showAlertOnApiError(self)

        // Bind profile
        input.loaded.asObservable()
            .filter { $0 != Settings.sharedInstance.userFrienji } // not logged user
            .map { $0.username }
            .bindTo(rx_title)
            .addDisposableTo(disposeBag)
        
        //Bind activity indicator
        viewModel
            .bindNext({ [weak self]_ in
                self?.activityIndicator.stopAnimating()
            })
            .addDisposableTo(disposeBag)
        
        // Bind posts
        
        viewModel.map { $0.posts }.bindTo(tableView.rx_itemsWithCellFactory) { [unowned self] (tableView, row, post) in
            if let _ = post.avatarTrail {
                //avatar change
                let cell = tableView.dequeueReusableCellWithIdentifier(AvatarChangeCell.reuseIdentifier) as! AvatarChangeCell
                cell.configureWithPost(post)
                
                return cell
                
            } else {
                //normal post
                let cell = tableView.dequeueReusableCellWithIdentifier(PostCell.reuseIdentifier) as! PostCell
                self.configurePostCell(cell, withPost: post, tableView: tableView)
                
                return cell
            }
            
        }.addDisposableTo(self.disposeBag)
    }
    
    private func configurePostCell(cell:PostCell, withPost post:Post, tableView:UITableView) {
        cell.configure(withPost: post, commentsButtonVisible: !(self is WallCommentsViewController))
        
        cell.comment.rx_tap
            .subscribeNext { [unowned self] in
                self.postCommented.onNext(post)
            }
            .addDisposableTo(cell.disposeBag)
        cell.liked
            .throttle(0.3, scheduler: MainScheduler.instance)
            .subscribeNext { [unowned self] in
                self.postLiked.onNext(post)
            }
            .addDisposableTo(cell.disposeBag)
        cell.reported
            .subscribeNext { [unowned self] in
                self.postReported.onNext(post)
            }
            .addDisposableTo(cell.disposeBag)
        cell.unliked
            .throttle(0.3, scheduler: MainScheduler.instance)
            .subscribeNext { [unowned self] in
                self.postDisliked.onNext(post)
            }
            .addDisposableTo(cell.disposeBag)
        cell.avatar.rx_gesture(.Tap)
            .flatMap { _ in post.author.map(Observable.just) ?? .empty() }
            .filter { $0 != Settings.sharedInstance.userFrienji } // not logged user
            .flatMap { [unowned self] author in // prevent going to wall that is already showing
                self.input.loaded
                    .filter { $0 != author }
                    .map { _ in author }
            }
        .bindTo(self.navigationHandler.pushed(WallPostsViewController.self)) { input, author in
                input.loaded.onNext(author)
        }.addDisposableTo(cell.disposeBag)
        
        // Show/hide image attachment in fullscreen
        cell.imageAttachment.rx_gesture(.Tap)
            .subscribeNext { [unowned self, unowned cell] _ in
                self.showImageFullScreen(cell, imageView: cell.imageAttachment)
            }
        .addDisposableTo(cell.disposeBag)
        
        // Show/hide location attachment fullscreen
        cell.locationAttachment.rx_gesture(.Tap)
            .subscribeNext { [unowned self, unowned cell, unowned tableView] _ in
                
                guard let indexPath = tableView.indexPathForCell(cell), let post = try? tableView.rx_modelAtIndexPath(indexPath) as Post else {
                    return
                }
                
                if let latitude = post.latitude, let longitude = post.longitude {
                    self.showLocationFullscreen(cell, imageView: cell.locationAttachment, location: CLLocationCoordinate2D(latitude: latitude, longitude: longitude))
                }
            }
        .addDisposableTo(cell.disposeBag)
    }
    
    // MARK: Image fullscreen
    
    private func showImageFullScreen(fromCell:UITableViewCell, imageView:UIImageView) {
        
        guard let image = imageView.image else {
            return
        }
        
        self.transitionManager = ImageTransitionManager()
        self.transitionManager?.startingFrame = fromCell.convertRect(imageView.frame, toView: self.view)
        self.transitionManager?.presentingController = self
        
        let controller = FullScreenImageController.loadFromStoryboard()
        controller.image = image
        controller.modalPresentationStyle = .Custom
        controller.transitioningDelegate = self.transitionManager
        
        self.transitionManager?.modalController = controller
        
        self.presentViewController(controller, animated: true, completion: nil)
    }
    
    private func showLocationFullscreen(cell:UITableViewCell, imageView:UIImageView, location:CLLocationCoordinate2D) {
        self.transitionManager = ImageTransitionManager()
        self.transitionManager?.startingFrame = cell.convertRect(imageView.frame, toView: self.view)
        self.transitionManager?.presentingController = self
        
        let controller = FullScreenMapController.loadFromStoryboard()
        controller.pinLocation = location
        controller.modalPresentationStyle = .Custom
        controller.transitioningDelegate = self.transitionManager
        
        self.transitionManager?.modalController = controller
        
        self.presentViewController(controller, animated: true, completion: nil)
    }

    // MARK: - Wall view controller

    func configureView() {
        let cellNib = UINib(nibName: PostCell.reuseIdentifier, bundle: NSBundle(forClass: PostCell.self))
        self.tableView.registerNib(cellNib, forCellReuseIdentifier: PostCell.reuseIdentifier)
        
        let avatarChangeNib = UINib(nibName: AvatarChangeCell.reuseIdentifier, bundle: NSBundle(forClass: AvatarChangeCell.self))
        self.tableView.registerNib(avatarChangeNib, forCellReuseIdentifier: AvatarChangeCell.reuseIdentifier)
        
        tableView.estimatedRowHeight = tableView.rowHeight
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        sizeTableHeaderViewToFit()
    }
    
    func configureNavigationTitleView() {
        guard let titleView = self.navigationTitleView else {
            return
        }
        self.input.loaded.asObservable().subscribeNext({ [unowned titleView] frienji in
            titleView.titleLabel.text = frienji.username
        }).addDisposableTo(self.disposeBag)
        
        Observable.combineLatest(self.input.loaded.asObservable(), LocationManager.sharedInstance.currentLocation.asObservable()) { (frienji: $0, location: $1) }.subscribeNext({ [unowned titleView] frienji, location in
            if let latitude = frienji.lastLatitude, let longitude = frienji.lastLongitude, let location = location {
                let distance = location.distanceFromLocation(CLLocation(latitude: latitude, longitude: longitude))
                let formatter = MKDistanceFormatter()
                formatter.unitStyle = .Abbreviated
                formatter.units = Settings.sharedInstance.distanceUnit == .km ? .Metric : .Imperial
                titleView.subtitleLabel.text = Localizations.wall.navigation.subtitle(formatter.stringFromDistance(distance))
            } else {
                titleView.subtitleLabel.text = Localizations.wall.navigation.defaultSubtitle
            }
        }).addDisposableTo(self.disposeBag)
    }

    func sizeTableHeaderViewToFit() {
        guard let headerView = tableView.tableHeaderView else {
            return
        }

        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        let height = headerView.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height
        headerView.frame.size.height = height
    }
    
    // MARK: - LocationPickerViewControllerDelegate
    
    override func locationPicker(picker: LocationPickerViewController, didFinishPickingLocation location: CLLocationCoordinate2D, withMessage message:String) {
        self.location.value = CLLocation(latitude: location.latitude, longitude: location.longitude)
        textSent.onNext(message)
        dismissViewControllerAnimated(true, completion: nil)
        scrollTableViewAfterMessageWasAdded()
    }
    
    // MARK: - SendPhotoViewControllerDelegate
    
    override func sendPhotoController(controller: SendPhotoViewController, didFinishPreparingPhoto photo: UIImage, withMessage message: String) {
        attachment.value = photo
        textSent.onNext(message)
        dismissViewControllerAnimated(true, completion: nil)
        scrollTableViewAfterMessageWasAdded()
    }
    
    //MARK: - MessageToolbarProtocol
    
    override func didTouchSendWithMesssage(message: String, forMessageToolBarView messageToolBarView: MessageToolbarView) {
        activityIndicator.startAnimating()
        textSent.onNext(message)
        scrollTableViewAfterMessageWasAdded()
    }
    
    private func scrollTableViewAfterMessageWasAdded() {
        self is WallCommentsViewController ? scrollToBottom() : scrollToTop()
    }
    
    private func scrollToTop() {
        if tableView.numberOfSections == 0 || tableView.numberOfRowsInSection(0) == 0 {
            return
        }
        tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0), atScrollPosition: .Top, animated: true)
    }
    
    private func scrollToBottom() {
        performWithDelay(kDefaultAnimationDuration) { [weak self] in
            
            guard let strongSelf = self where strongSelf.tableView.numberOfRowsInSection(0) > 0 else {
                return
            }
            
            let row = strongSelf.tableView.numberOfRowsInSection(0)
            strongSelf.tableView.scrollToRowAtIndexPath(NSIndexPath(forRow: row - 1, inSection: 0), atScrollPosition: .Bottom, animated: true)
        }
    }
    
}

extension  WallViewController: UITableViewDelegate {
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.wallSpaceCell)
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView.dequeueReusableCellWithIdentifier(R.reuseIdentifier.wallSpaceCell)?.bounds.height ?? tableView.sectionHeaderHeight
    }
    
    func tableView(tableView: UITableView, editingStyleForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCellEditingStyle {
        let post: Post
        do {
            post = try self.tableView.rx_modelAtIndexPath(indexPath)
            return (post.reported || post.avatarTrail != nil || Settings.sharedInstance.userFrienji?.userId == post.author?.userId) ? .None : .Delete
        } catch {
            return .None
        }
    }
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        let reportAction = UITableViewRowAction(style: .Normal, title: Localizations.wall.report.button.title) { [unowned self] action, index in
            self.reportPost(atIndexPath: index)
        }
        reportAction.backgroundColor = UIColor.appOrangeReportColor()
        return [reportAction]
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        let post: Post
        do {
            post = try self.tableView.rx_modelAtIndexPath(indexPath)
            return !post.reported
        } catch {
            return false
        }
    }
    
    private func reportPost(atIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? PostCell {
            cell.reported.onNext()
            let actionSheetController = UIAlertController(title: Localizations.wall.report.alert.message, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
            actionSheetController.view.tintColor = UIColor.appPurpleColor()
            let cancelAction = UIAlertAction(title: Localizations.wall.report.alert.cancel, style: UIAlertActionStyle.Cancel) { (action:UIAlertAction) -> Void in
            }
            actionSheetController.addAction(cancelAction)
            
            self.presentViewController(actionSheetController, animated: true, completion: nil)
        }
    }
}
