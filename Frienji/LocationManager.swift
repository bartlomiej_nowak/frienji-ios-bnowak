//
//  LocationManager.swift
//  Traces
//
//  Created by Adam Szeremeta on 15.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift
import RxCocoa

class LocationManager : NSObject {
    private let kMinimumDistanceChange:Double = 10 //meters
    private let kHeadingsStorageCount = 10
    private let kMaxHeadingMeasureRange:Double = 5 //degrees

    // MARK: Shared instance

    static let sharedInstance = LocationManager()

    // MARK: Properties
    internal var locationManager:CLLocationManager!
    private (set) var isUpdatingLocation:Bool = false

    let authorizationStatus = Variable<CLAuthorizationStatus?>(nil)
    let currentLocation = Variable<CLLocation?>(nil)
    let currentHeading = Variable<Double?>(nil)

    private var headings = [CLHeading]()

    // MARK: Init

    override init() {
        super.init()

        setUpLocationManager()
    }

    // MARK: Permissions

    class func getLocationPermission() -> CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }

    // MARK: Location manager

    private func setUpLocationManager() {
        self.locationManager = CLLocationManager()
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.activityType = CLActivityType.Fitness
        self.locationManager.pausesLocationUpdatesAutomatically = true
        self.locationManager.distanceFilter = self.kMinimumDistanceChange
        self.locationManager.headingFilter = 0.01
        self.locationManager.delegate = self
    }

    func requestWhenInUseAuthorization() {
        self.locationManager.requestWhenInUseAuthorization()
    }

    func startLocationUpdates() {
        self.isUpdatingLocation = true

        self.locationManager.startUpdatingLocation()
        self.locationManager.startUpdatingHeading()
    }

    func stopLocationUpdates() {
        self.isUpdatingLocation = false

        self.locationManager.stopUpdatingLocation()
        self.locationManager.stopUpdatingHeading()
    }

    // MARK: Bearing

    func getBearingBetweenTwoPoints(point1 : CLLocation, point2 : CLLocation) -> CGFloat {
        let lat1 = degreesToRadians(point1.coordinate.latitude)
        let lon1 = degreesToRadians(point1.coordinate.longitude)

        let lat2 = degreesToRadians(point2.coordinate.latitude);
        let lon2 = degreesToRadians(point2.coordinate.longitude);

        let dLon = lon2 - lon1;

        let y = sin(dLon) * cos(lat2);
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon);
        let radiansBearing = atan2(y, x);

        return (CGFloat(radiansToDegrees(radiansBearing)) + 360) % 360
    }

    private func degreesToRadians(degrees: Double) -> Double {
        return degrees * M_PI / 180.0
    }

    private func radiansToDegrees(radians: Double) -> Double {
        return radians * 180.0 / M_PI
    }

    // MARK: CLLocationManagerDelegate (Location)

    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let newLocation = locations.last else {

            return
        }

        //we want only to propagate new locations, check if location is changed
        if let currentLocation = self.currentLocation.value where (currentLocation.coordinate.latitude != newLocation.coordinate.latitude && currentLocation.coordinate.longitude != newLocation.coordinate.longitude) {

            self.currentLocation.value = newLocation

        } else if self.currentLocation.value == nil {

            self.currentLocation.value = newLocation
        }
    }

    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        self.authorizationStatus.value = status
    }

}

extension LocationManager : CLLocationManagerDelegate {

    func locationManager(manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        self.headings.append(newHeading)
        if self.headings.count > self.kHeadingsStorageCount {

            self.headings.removeFirst()
        }

        // check if we passing 360 degrees, if so discard values
        self.headings = headings.filter { abs($0.trueHeading - newHeading.trueHeading) < kMaxHeadingMeasureRange }

        let average = headings.reduce(0) {$0 + Double($1.trueHeading)} / Double(self.headings.count)
        self.currentHeading.value = average
    }

}
