//
//  ConfigurationsHelper.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

class ConfigurationsHelper {
    
    private enum Configuration : String {
        
        case Debug = "Debug"
        case Staging = "Staging"
        case Production = "Production"
    }
    
    private enum EnvironmentProperty : String {
        
        case BackendUrl = "backendUrl"
    }
    
    private let kConfigurationKey = "Configuration"
    private let kEnvironmentPlistName = "EnvironmentVariables"
    
    private var activeConfiguration:Configuration?
    private var activeEnviromentDictionary:NSDictionary!
    
    // MARK: Shared instance
    
    static let sharedInstance = ConfigurationsHelper()
    
    // MARK: Init
    
    private init() {
        
        let bundle = NSBundle(forClass: ConfigurationsHelper.self)
        
        guard let configurationName = bundle.infoDictionary![kConfigurationKey] as? String else {
            return
        }
        
        self.activeConfiguration = Configuration(rawValue: configurationName)
        
        //load our configuration plist
        if let environmentsPath = bundle.pathForResource(kEnvironmentPlistName, ofType: "plist"),
            let environmentsDict = NSDictionary(contentsOfFile: environmentsPath),
            let activeEnviromentDic = environmentsDict[self.activeConfiguration!.rawValue] as? NSDictionary {
            
            self.activeEnviromentDictionary = activeEnviromentDic
        }
    }
    
    // MARK: Runtime
    
    func isRunningInRelease() -> Bool {
        
        guard let activeConfig = self.activeConfiguration else {
            
            return false
        }
        
        return activeConfig == Configuration.Staging || activeConfig == Configuration.Production
    }
    
    func isProduction() -> Bool {
        
        guard let activeConfig = self.activeConfiguration else {
            return false
        }
        
        return activeConfig == Configuration.Production
    }
    
    // MARK: Environment properties
    
    func getBackendUrl() -> String {
        
        return self.activeEnviromentDictionary[EnvironmentProperty.BackendUrl.rawValue] as! String
    }
    
    // MARK: App
    
    func getAppVersion() -> String {
        return NSBundle(forClass: ConfigurationsHelper.self).objectForInfoDictionaryKey("CFBundleShortVersionString") as! String
    }
    
    func getAppStoreUrl() -> String {
        return "https://itunes.apple.com/us/app/apple-store/id1179786006?mt=8"
    }
}
