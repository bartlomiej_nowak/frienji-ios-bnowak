//
//  TermsAncCondViewController.swift
//  Frienji
//
//  Created by bolek on 01.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class TermsAncCondViewController: UIViewController, StoryboardLoad
{
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "TermsAncCondViewController"

    @IBOutlet var webView:UIWebView!
    
    var viewModel = TermsAndContViewModel()
    
    //MARK: - View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.initControls()
    }
    
    //MARK: - Private methods
    
    func initControls()
    {
        self.webView.loadRequest(NSURLRequest.init(URL: NSURL.init(string: self.viewModel.termsAndContUrl)!))
    
        self.navigationItem.title = Localizations.termsAndCond.title
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(title: "❌", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(closeClicked(_:)))
    }
    
    //MARK: - IBActions
    
    @IBAction func closeClicked(sender:AnyObject)
    {
        self.navigationController?.dismissViewControllerAnimated(true, completion: nil)
    }
}
