//
//  AvatarSelectionCollectionViewController.swift
//  Frienji
//
//  Created by bolek on 02.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class AvatarSelectionViewController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "Introductions"
    
    static let kAvatarsPerRow: CGFloat = 4
    
    //outlets
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var collectionView: UICollectionView!
    
    //properites
    private (set) var viewModel: AvatarSelectionViewModel!
    private var selectedAvatar: (value:String, type:AvatarType)?
    
    //MARK: - View lifecycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.viewModel = AvatarSelectionViewModel()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        self.collectionView.delegate = self
    }
    
    // MARK: Actions
    
    @IBAction func goBackClicked(sender:UIButton) {
        if self.presentingViewController?.presentedViewController == self {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // MARK: Avatar selection
    
    private func showEmojiSkinTonePicker(item: (value:String, type:AvatarType), cell: UICollectionViewCell) {
        let skinnedEmojiPicker = SkinnedEmojiPicker.loadFromStoryboard()
        skinnedEmojiPicker.emoji = item.value
        skinnedEmojiPicker.modalPresentationStyle = .Popover
        skinnedEmojiPicker.delegate = self

        let presentationController = skinnedEmojiPicker.popoverPresentationController
        presentationController?.sourceView = cell
        presentationController?.sourceRect = cell.bounds
        presentationController?.delegate = self
        presentationController?.permittedArrowDirections = .Down
        
        self.presentViewController(skinnedEmojiPicker, animated: true, completion: nil)
    }
    
    // MARK: Navigation
    
    private func navigateAfterSelection(item: (value:String, type:AvatarType), skinTone: String? = nil) {
        if isModal {
            let navigation = NavigationHandler(sourceViewController: self)
            navigation.dismissModal(SettingsViewController.self) { input in
                input.avatarSelected?.onNext(Avatar(name: item.type == .Emoji ? item.value.emojiEscaped : item.value, skinTone: skinTone ?? "", type: item.type))
            }
        } else {
            self.performSegueWithIdentifier(R.segue.avatarSelectionViewController.signUp.identifier, sender: nil)
        }
    }

}

extension AvatarSelectionViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return self.viewModel.getNumberOfSections()
    }
    
    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionHeader {
            let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: AvatarSelectionCollectionViewHeader.reuseIdentifier, forIndexPath: indexPath) as! AvatarSelectionCollectionViewHeader
            header.titleLabel.text = self.viewModel.getTitleForSection(indexPath.section)
            
            return header
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.getNumberOfItemsForSection(section)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(AvatarSelectionCollectionViewCell.reuseIdentifier, forIndexPath: indexPath) as! AvatarSelectionCollectionViewCell
        let item = self.viewModel.getItemForIndexPath(indexPath)
        
        cell.configureCellFor(item.value, entryType: item.type)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let item = self.viewModel.getItemForIndexPath(indexPath)
        
        guard let cell = collectionView.cellForItemAtIndexPath(indexPath) where item.type == .Emoji && item.value.canHaveSkinToneModifier else {
            self.viewModel.storeAvatarInfo(item.value, skinTone: nil, avatarType: item.type)
            self.navigateAfterSelection(item)
            return
        }
        
        self.selectedAvatar = item
        self.showEmojiSkinTonePicker(item, cell: cell)
    }
}

extension AvatarSelectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width = floor(self.view.frame.size.width / AvatarSelectionViewController.kAvatarsPerRow)
        return CGSizeMake(width, width)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 0
    }
}

extension AvatarSelectionViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyleForPresentationController(controller: UIPresentationController) -> UIModalPresentationStyle {
        return .None
    }
}

extension AvatarSelectionViewController: SkinnedEmojiPickerDelegate {
    
    func skinnedEmojiPicker(picker: SkinnedEmojiPicker, didSelectEmoji emoji: String, withSkinTone skinTone: String) {
        self.viewModel.storeAvatarInfo(emoji, skinTone: skinTone, avatarType: .Emoji)
        
        if let avatar = self.selectedAvatar {
            self.navigateAfterSelection(avatar, skinTone: skinTone)
        }
    }
}
