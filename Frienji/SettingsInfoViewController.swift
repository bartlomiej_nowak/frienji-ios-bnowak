//
//  SettingsInfoViewController.swift
//  Frienji
//
//  Created by Adam Szeremeta on 14.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

class SettingsInfoViewController: UIViewController, HasDisposeBag {
    
    //outlets
    @IBOutlet weak var versionLabel: UILabel!
    
    @IBOutlet weak var rateButton: UIButton!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    
    //properties
    private var viewModel: SettingsInfoViewModel!
    var disposeBag = DisposeBag()
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let navigationHandler = NavigationHandler(sourceViewController: self)
        self.viewModel = SettingsInfoViewModel(navigationHandler: navigationHandler)
        
        setVersionInLabel()
        
        setUpBindings()
    }
    
    // MARK: Version
    
    func setVersionInLabel() {
        self.versionLabel.text = "\(Localizations.setting.who_made_this)\(ConfigurationsHelper.sharedInstance.getAppVersion())"
    }
    
    // MARK: Bindings
    
    private func setUpBindings() {
        self.rateButton.rx_tap.bindNext { [unowned self] in
            self.viewModel.showAppRatings()
        }.addDisposableTo(self.disposeBag)
        
        self.contactButton.rx_tap.bindNext { [unowned self] in
            self.viewModel.navigateToContactUs()
        }.addDisposableTo(self.disposeBag)
        
        self.termsButton.rx_tap.bindNext { [unowned self] in
            self.viewModel.showTermsAndConditions()
        }.addDisposableTo(self.disposeBag)
        
        self.logoutButton.rx_tap.bindNext { [unowned self] in
            self.showLogoutAlert()
        }.addDisposableTo(self.disposeBag)
    }
    
    // MARK: Logout alert
    
    private func showLogoutAlert() {
        UIAlertUtils.showAlertWithTitle(Localizations.settings.logout_alert_title, message: nil, positiveButton: Localizations.alert.yes_button, negativeButton: Localizations.alert.no_button, fromController: self, showCompletion: nil, actionHandler: { (possitiveButtonTouched:Bool) in
            
            if possitiveButtonTouched {
                self.viewModel.logout()
            }
        })
    }
}
