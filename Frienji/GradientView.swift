//
//  GradientView.swift
//  Frienji
//
//  Created by Adam Szeremeta on 06.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class GradientView: UIView {
    
    // MARK: Properties
    
    @IBInspectable
    var vertical: Bool = false {
        didSet{
            refreshView()
        }
    }
    
    @IBInspectable
    var startColor: UIColor = UIColor.appOrangePlaceholderColor() {
        didSet{
            refreshView()
        }
    }
    
    @IBInspectable
    var endColor: UIColor = UIColor.appMessageLightGreenColor() {
        didSet{
            refreshView()
        }
    }
    
    @IBInspectable
    var startLocation: CGFloat = 1.0 {
        didSet{
            refreshView()
        }
    }
    
    @IBInspectable
    var endLocation: CGFloat = 0.0 {
        didSet{
            refreshView()
        }
    }
    
    // MARK: Refresh
    
    private func refreshView() {
        self.layoutIfNeeded()
    }
    
    // MARK: Draw
    
    override func drawRect(rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        let locations:[CGFloat] = [self.startLocation, self.endLocation]
        let colors = [self.startColor.CGColor, self.endColor.CGColor]
        let colorspace = CGColorSpaceCreateDeviceRGB()
        
        if let gradient = CGGradientCreateWithColors(colorspace, colors, locations) {
            if vertical {
                CGContextDrawLinearGradient(context, gradient, CGPoint(x: 0, y: 0), CGPoint(x: 0, y: rect.size.height), CGGradientDrawingOptions())
            } else {
                CGContextDrawLinearGradient(context, gradient, CGPoint(x: 0, y: rect.size.height), CGPoint(x: rect.size.width, y: 0), CGGradientDrawingOptions())
            }
        }
    }
    
}
