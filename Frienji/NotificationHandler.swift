//
//  NotificationsHandler.swift
//  Frienji
//
//  Created by Piotr Łyczba on 25/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import RxAppState
import RxSwift
import Argo

class NotificationHandler {

    static let kNotificationDateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
    static let dateFormatter: NSDateFormatter = {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = NotificationHandler.kNotificationDateFormat
        return dateFormatter
    }()

    let application = UIApplication.sharedApplication()

    func notificationForActivity(activity: ActivityType) -> Observable<AnyObject> {
        return application.rx_delegate
            .observe(#selector(UIApplicationDelegate.application(_:didReceiveRemoteNotification:fetchCompletionHandler:)))
            .map { args in args[1] }
            .filter {
                let decodedActivity: Decoded<ActivityType> = JSON($0) <| "key"
                switch decodedActivity {
                case let .Success(value):
                    return value == activity
                case let .Failure(error):
                    throw error
                }
            }
    }

    func messageByAuthor(author: Frienji) -> Observable<Message> {
        return notificationForActivity(.IncomingMessage)
            .filter {
                let decodedAuthor: Decoded<Frienji> = JSON($0) <| ["data", "frienji"]
                switch decodedAuthor {
                case let .Success(value):
                    return value == author
                case let .Failure(error):
                    throw error
                }
            }
            .decode { $0 <| "data" }
    }

    func deletedMessageByAuthor(author: Frienji) -> Observable<Message> {
        return notificationForActivity(.DeletedMessage)
            .filter {
                let decodedAuthor: Decoded<Frienji> = JSON($0) <| ["data", "frienji"]
                switch decodedAuthor {
                case let .Success(value):
                    return value == author
                case let .Failure(error):
                    throw error
                }
            }
            .decode { $0 <| "data" }
    }
    
    var notificationTapped: Observable<AnyObject> {
        return ((application.delegate as? AppDelegate)?.notificationTapped ?? Observable<AnyObject>.empty())
            .filter {
                let decodedActivity: Decoded<ActivityType> = JSON($0) <| "key"
                switch decodedActivity {
                case .Success:
                    return true
                case let .Failure(error):
                    throw error
                }
            }
    }

}
