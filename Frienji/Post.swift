//
//  Post.swift
//  Frienji
//
//  Created by Piotr Łyczba on 29/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Post {

    let id: Int
    let message: String
    let createdAt: NSDate
    let isAuthor: Bool
    let numberOfComments: Int?
    let numberOfLikes: Int
    let liked: Bool
    let author: Frienji?
    let attachmentUrl: String?
    let latitude: Double?
    let longitude: Double?
    let wallOwnerId: Int
    let parentPostId: Int
    let avatarTrail: AvatarTrail?
    let reported: Bool
}

// MARK: - Fake

extension Post: Fakeable {

    static func fake() -> Post {
        return Post(
            id: faker.number.increasingUniqueId(),
            message: faker.lorem.sentences(amount: 10),
            createdAt: NSDate.randomWithinDaysBeforeToday(356),
            isAuthor: faker.number.randomBool(),
            numberOfComments: faker.number.randomInt(min: 0, max: 5),
            numberOfLikes: faker.number.randomInt(min: 0, max: 5),
            liked: faker.number.randomBool(),
            author: Frienji.fake(),
            attachmentUrl: faker.internet.image(),
            latitude: faker.address.latitude(),
            longitude: faker.address.longitude(),
            wallOwnerId: faker.number.randomInt(min: 0, max: 5),
            parentPostId: faker.number.randomInt(min: 0, max: 5),
            avatarTrail: nil,
            reported: faker.number.randomBool()
        )
    }

}

extension Post: Decodable {

    static func decode(json: JSON) -> Decoded<Post> {
        //Divided into two parts because "Expression was too complex to be solved in reasonable time"
        let d = curry(Post.init)
            <^> json <| "id"
            <*> json <| "message" <|> pure("")
            <*> (json <| "created_at" >>- toNSDate)
            <*> json <| "is_author"
            <*> json <|? "number_of_comments"
            <*> json <| "number_of_likes"
            <*> json <| "liked"
        return d
            <*> json <|? "author"
            <*> json <|? "attachment_url"
            <*> json <|? ["location", "latitude"]
            <*> json <|? ["location", "longitude"]
            <*> json <| "wall_owner_id"
            <*> json <| "parent_id"
            <*> json <|? "avatar_trail"
            <*> json <| "reported"
    }

}

func == (lhs: Post, rhs: Post) -> Bool {
    return lhs.id == rhs.id
}
