//
//  ProfileBaseView.swift
//  Frienji
//
//  Created by adam kolodziej on 15.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

@IBDesignable class ProfileBaseView: UIView, NibInstantiate {

    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var profileDescription: UILabel!
    @IBOutlet weak var name: UILabel?
    
    let profileLoaded = PublishSubject<Frienji>()
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        #if !TARGET_INTERFACE_BUILDER
            // MARK: - Load command
            let loadProfile = profileLoaded
                .map(ProfileCommand.LoadProfile)
            
            // MARK: - View model
            let viewModel = loadProfile
                .scan(ProfileViewModel()) { [weak self] viewModel, command in
                    let viewModel = viewModel.executeCommand(command)
                    self?.layoutIfNeeded()
                    
                    return viewModel
                }
                .startWith(ProfileViewModel())
                .shareReplay(1)
            
            // MARK: - Bind view model
            [
                viewModel.map { $0.avatar?.avatarImage } --> avatarImage.rx_image,
                viewModel.map { $0.description } --> profileDescription.rx_text,
                viewModel.map { NSURL(string: $0.coverUrl) } --> coverImage.sd_setImageWithURL,
                ].forEach { $0.addDisposableTo(disposeBag) }
            if let name = name {
                viewModel.map { $0.name }.bindTo(name.rx_text).addDisposableTo(disposeBag)
            }
        #endif
    }

}
