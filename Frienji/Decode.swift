//
//  Decode.swift
//  Frienji
//
//  Created by Piotr Łyczba on 25/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import Argo

extension Decodable {

    static var toNSDate: String -> Decoded<NSDate> {
        return {
            .fromOptional(
                FrienjiApi.jsonDateFormatter.dateFromString($0)
                    ?? NotificationHandler.dateFormatter.dateFromString($0)
            )
        }
    }

}

extension ObservableConvertibleType where Self.E == AnyObject {

    func decode<Element: Decodable where Element.DecodedType == Element>() -> Observable<Element> {
        return decode(Element.decode)
    }

    func decode<Element>(decodeHandler: (JSON) -> Decoded<Element>) -> Observable<Element> {
        return decode { decodeHandler(Argo.JSON($0)) }
    }

    func decode<Element>(decodeHandler: (AnyObject) -> Decoded<Element>) -> Observable<Element> {
        return asObservable().map { data in
            switch decodeHandler(data) {
            case let .Failure(error):
                throw error
            case let .Success(result):
                return result
            }
        }
    }
    
}
