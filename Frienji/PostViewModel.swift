//
//  WallCellModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 29/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

enum PostCommand {
    case Load(post: Post)
}

struct PostViewModel {

    let authorName: String
    let createdAt: String
    let details: String
    let avatar: UIImage?
    let likeText: String
    let imageUrl: NSURL?
    let location: CLLocation?
    let likeCount: Int
    let commentCount: Int
    let report: Bool

    init(authorName: String = "", createdAt: String = "", details: String = "", avatar: UIImage? = nil, likeText: String = Localizations.wall.like, imageUrl: NSURL? = nil, location: CLLocation? = nil, likeCount: Int = 0, commentCount: Int = 0, report: Bool = false) {
        self.authorName = authorName
        self.createdAt = createdAt
        self.details = details
        self.avatar = avatar
        self.likeText = likeText
        self.imageUrl = imageUrl
        self.location = location
        self.likeCount = likeCount
        self.commentCount = commentCount
        self.report = report
    }

    func executeCommand(command: PostCommand) -> PostViewModel {
        switch command {
        case let .Load(post):
            return PostViewModel(
                authorName: post.author?.username ?? "",
                createdAt: post.createdAt.timeAgo ?? "",
                details: post.message ?? "",
                avatar: post.author?.avatar.avatarImage,
                likeText: likeText(post.liked),
                imageUrl: post.attachmentUrl.flatMap(NSURL.init),
                location: locationFromLatitude(post.latitude, longitude: post.longitude),
                likeCount: post.numberOfLikes,
                commentCount: post.numberOfComments ?? 0,
                report: post.reported
            )
        }
    }

    private func likeText(liked: Bool) -> String {
        return liked ? Localizations.wall.like : Localizations.wall.unlike
    }

    private func locationFromLatitude(latitude: Double?, longitude: Double?) -> CLLocation? {
        guard let latitude = latitude, longitude = longitude else {
            return nil
        }

        return CLLocation(latitude: latitude, longitude: longitude)
    }

}

extension PostViewModel {

    static func viewModel(postLoaded post: Observable<Post>) -> Observable<PostViewModel> {
        let loadCommand = post.map(PostCommand.Load)

        return Observable.of(loadCommand).merge()
            .scan(PostViewModel()) { viewModel, command in
                viewModel.executeCommand(command)
            }
            .shareReplay(1)
    }

}
