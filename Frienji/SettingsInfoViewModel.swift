//
//  SettingsInfoViewModel.swift
//  Frienji
//
//  Created by Adam Szeremeta on 14.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift

class SettingsInfoViewModel {
    
    let kContactUsUrl: NSURL = NSURL(string: "http://www.frienji.io/contact")!
    let kTermsAndConditionUrl: NSURL = NSURL(string: "http://www.frienji.io/terms")!
    
    private let navigationHandler: NavigationHandler
    private let api = FrienjiApi.sharedInstance
    private let settings = Settings.sharedInstance

    private let disposeBag = DisposeBag()

    // MARK: Init
    
    init(navigationHandler: NavigationHandler) {
        self.navigationHandler = navigationHandler
    }
    
    // MARK: Actions
    
    func showAppRatings() {
        if let appStoreUrl = NSURL(string: ConfigurationsHelper.sharedInstance.getAppStoreUrl()) {
            self.navigationHandler.goToWebsite(appStoreUrl)
        }
    }
    
    func navigateToContactUs() {
        self.navigationHandler.goToWebsite(self.kContactUsUrl)
    }
    
    func showTermsAndConditions() {
        self.navigationHandler.goToWebsite(self.kTermsAndConditionUrl)
    }
    
    func logout() {
        self.api.logout().subscribeNext { [unowned self] in
            self.navigationHandler.replaceRoot(IntroductionViewController.self)
        }.addDisposableTo(disposeBag)
    }

}
