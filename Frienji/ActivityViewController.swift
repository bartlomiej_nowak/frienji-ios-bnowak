//
//  ActivityViewController.swift
//  Frienji
//
//  Created by bolek on 04.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class ActivityViewController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "ActivityView"

    //outlets
    @IBOutlet weak var spinerLbl: UILabel!
    
    private let kAnimationPathSide:CGFloat = 200
    
    var spinnerEmoji: String?
    
    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.spinerLbl.text = self.spinnerEmoji
        setupSpinner()
    }
    
    // MARK: Spinner
    
    private func setupSpinner() {
        let textAnimation = CAKeyframeAnimation(keyPath: "position")
        textAnimation.duration = 1.5
        textAnimation.path = UIBezierPath(ovalInRect: CGRectMake((self.view.frame.size.width - self.kAnimationPathSide) / 2, (self.view.frame.size.height - self.kAnimationPathSide) / 2, self.kAnimationPathSide, self.kAnimationPathSide)).CGPath
        textAnimation.rotationMode = kCAAnimationRotateAutoReverse
        textAnimation.calculationMode = kCAAnimationCubicPaced
        textAnimation.removedOnCompletion = false
        textAnimation.repeatCount = FLT_MAX
        
        self.spinerLbl.layer.addAnimation(textAnimation, forKey: "position")
    }
    
    // MARK: Public functions
    
    func hide() {
        self.view.removeFromSuperview()
        self.spinerLbl.layer.removeAllAnimations()
    }
    
    //MARK: - Class functions
    
    class func getActivityInFullScreen(emoji:String, inView view:UIView) -> ActivityViewController {
        let controller = ActivityViewController.loadFromStoryboard()
        controller.spinnerEmoji = emoji
        
        view.addSubviewFullscreen(controller.view)
        
        return controller
    }
    
}
