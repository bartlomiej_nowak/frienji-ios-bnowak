//
//  ConversationData.swift
//  Frienji
//
//  Created by Adam Szeremeta on 01.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct ConversationData {
    
    let messages: [Message]
    let avatarChanges: [AvatarChange]
}

// MARK: - Decode

extension ConversationData: Decodable {
    
    static func decode(json: JSON) -> Decoded<ConversationData> {
        return curry(ConversationData.init)
            <^> json <|| "data"
            <*> json <|| "avatar_trails_data"
    }
    
}
