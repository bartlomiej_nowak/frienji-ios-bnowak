//
//  DateUtils.swift
//  Traces
//
//  Created by Adam Szeremeta on 27.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

class DateUtils {
    
    // MARK: Date
    
    private static let dateFormatter = NSDateFormatter()
    
    class func parseDateFromApiResponse(dateString:String) -> NSDate? {
        dateFormatter.dateFormat = FrienjiApi.ApiDateFormat
        
        return dateFormatter.dateFromString(dateString)
    }
    
}
