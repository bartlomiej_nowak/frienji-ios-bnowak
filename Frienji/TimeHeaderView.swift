//
//  TimeHeaderView.swift
//  Frienji
//
//  Created by Adam Szeremeta on 21.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class TimeHeaderView: UITableViewHeaderFooterView, NibLoad, CellReuseIdentifier {
    
    static let kEstimatediewHeight: CGFloat = 24

    //outlets
    @IBOutlet private weak var dateLabel: UILabel!
    
    //properties
    private let dateFormatter = NSDateFormatter()
    
    // MARK: Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = UIColor.clearColor()
        
        self.dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
        self.dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
    }
    
    // MARK: Data
    
    func configureWithDate(date:NSDate?) {
        
        guard let date = date else {
            self.dateLabel.text = ""
            return
        }
        
        self.dateLabel.text = self.dateFormatter.stringFromDate(date)
    }
    
}
