//
//  InboxViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 16/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

struct InboxViewModel {

    let conversations: [Conversation]

    init(conversations: [Conversation] = []) {
        self.conversations = conversations
    }

    func executeCommand(command: InboxCommand) -> InboxViewModel {
        switch command {
        case let .Load(conversations):
            return InboxViewModel(conversations: conversations)
        case let .Add(conversation):
            guard !conversations.contains(conversation) else {
                return self
            }

            return InboxViewModel(conversations: conversations.arrayByAppending(conversation))
        }
    }

}

enum InboxCommand {
    case Load(conversations: [Conversation])
    case Add(conversation: Conversation)
}
