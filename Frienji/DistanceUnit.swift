//
//  DistanceUnit.swift
//  Frienji
//
//  Created by Adam Szeremeta on 28.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

enum DistanceUnit : Int {
    
    case km = 0
    case miles = 1
    
    static let labels = [Localizations.settings.unit_km, Localizations.settings.unit_miles]
}
