//
//  CoachMarkView.swift
//  Frienji
//
//  Created by Adam Szeremeta on 08.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class CoachMarkView: UIView, NibLoad {
    
    //outlets
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var actionButton: UIButton!

    //properties
    var dismissCallback:(() -> Void)?

    // MARK: Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureViewAppearance()
    }
    
    // MARK: Drawing
    
    override func drawRect(rect: CGRect) {
        //draw background overlay
        let backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.7)
        backgroundColor.setFill()
        UIRectFill(rect)
    }
    
    // MARK: Appearance
    
    private func configureViewAppearance() {
        self.titleLabel.textColor = UIColor.appPurpleColor()
        self.titleLabel.font = UIFont.latoBoldWithSize(18.0)
        
        self.descriptionLabel.textColor = UIColor.appPurpleColor()
        self.descriptionLabel.font = UIFont.latoRegulatWithSize(16.0)
        
        self.actionButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.actionButton.titleLabel?.font = UIFont.latoBoldWithSize(18.0)
        self.actionButton.backgroundColor = UIColor.appOrangePlaceholderColor()
    }
    
    // MARK: Touch
    
    override func pointInside(point: CGPoint, withEvent event: UIEvent?) -> Bool {
        //dismiss our view and pass touch to other views
        self.removeFromViewWithAnimation()
        
        return false
    }
    
    // MARK: Data
    
    func configureWithTitle(title:String, description:String, actionTitle:String?) {
        self.titleLabel.text = title
        self.descriptionLabel.text = description
        
        self.actionButton.setTitle(actionTitle, forState: UIControlState.Normal)
        self.actionButton.hidden = actionTitle == nil
    }
    
    // MARK: Animations
    
    func showWithAnimationInContainer(coachMarkContainer:UIView? =  UIApplication.sharedApplication().keyWindow) -> Bool {
        if let container = coachMarkContainer {
            self.alpha = 0
            
            container.addSubviewFullscreen(self)
            container.layoutIfNeeded()
            
            UIView.animateWithDuration(kDefaultAnimationDuration) {
                self.alpha = 1
            }
            
            return true
        }
        
        return false
    }
    
    func removeFromViewWithAnimation() {
        UIView.animateWithDuration(kDefaultAnimationDuration, animations: {
            self.alpha = 0
        }) { (result:Bool) in
            self.removeFromSuperview()
            
            self.dismissCallback?()
        }
    }

}
