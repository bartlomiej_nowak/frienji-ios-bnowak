//
//  BubbleObject.swift
//  Traces
//
//  Created by Adam Szeremeta on 14.07.2016.
//  Copyright Â© 2016 Ripple Inc. All rights reserved.
//

import Foundation
import OpenGLES
import AVFoundation
import RxSwift
import RxCocoa
import SDWebImage

class BubbleObject: Hashable, Equatable {

    static let kMaxVerticalOffset: CGFloat = 2.5
    static let kMinVerticalOffset: CGFloat = -1.3
    
    static let kCoordinateSpaceSize:CGFloat = 2 //from -1 to 1
    static let kLowPassFilterHorizontalValue:CGFloat = 0.05
    static let kLowPassFilterVerticalValue:CGFloat = 0.2
    static let kSizeAnimationDelta:Double = 0.03
    static let kPositionAnimationDelta:Double = 0.01

    private (set) var bubbleSize: CGSize = CGSizeZero
    private (set) var bubblePosition: CGPoint = CGPointZero
    private (set) var bubbleBearing: CGFloat?
    private (set) var bubbleSizePixels: CGFloat = 0
    private (set) var bubbleVerticalOffset: CGFloat = 0
    private (set) var bubbleOffsetMultiplier: CGFloat = 1.0

    //bubble explosion
    static let kBubbleExplosionImages = BubbleObject.preloadBubbleExplosionFrames()
    private var isExplosionAnimationActive = false
    private var explosionAnimationFrameIndex = 0
    private var explosionAnimationEndCallback: (() -> Void)?
    
    var glowAnimationUniform:GLuint?
    var glowAnimationTimeUniform:GLuint?
    var flatImageUniform:GLuint?
    var logoWithoutBubbleUniform:GLuint?
    
    private var holdAnimationStartDate = NSDate()
    private var isHoldAnimationActive = false
    
    private var logoTextureData:[[GLubyte]?]?
    private var currentLogoTextureDataIndex = 0
    private var logoTextureDuration: Double = 0
    static var logoTextureWidth:Int32 = Int32(ArOpenGLView.kMaximumBubbleSizeMultiplier * UIScreen.mainScreen().bounds.width * UIScreen.mainScreen().scale)
    static var logoTextureHeight:Int32 = Int32(ArOpenGLView.kMaximumBubbleSizeMultiplier * UIScreen.mainScreen().bounds.width * UIScreen.mainScreen().scale)
    static var logoTexturePointer:GLuint?

    private var xFloatingDirection:CGFloat = 1
    private var yFloatingDirection:CGFloat = 1
    private var xFloatingValue:CGFloat = 0
    private var yFloatingValue:CGFloat = 0

    private var currentSizeAnimationDelta:Double = 0
    private var sizeDisposeBag:DisposeBag? = nil

    private var currentLocationAnimationDelta:Double = 0
    private var locationAnimationDisposeBag:DisposeBag? = nil

    private var disposeBag = DisposeBag()

    // MARK: Bubble texture data

    private var bubbleVertices: (Vertex, Vertex, Vertex, Vertex) = (
        Vertex(position: (0, 0, 0), textureCoordinate: (1, 1)),
        Vertex(position: (0, 0, 0), textureCoordinate: (1, 0)),
        Vertex(position: (0, 0, 0), textureCoordinate: (0, 0)),
        Vertex(position: (0, 0, 0), textureCoordinate: (0, 1))
    )

    private var bubbleIndices: (GLubyte, GLubyte, GLubyte, GLubyte, GLubyte, GLubyte) = (
        0, 1, 2,
        2, 3, 0
    )

    // MARK: Properties

    private (set) var shouldBubbleBeDrawn = false

    private var indexBuffer: GLuint = GLuint()
    private var vertexBuffer: GLuint = GLuint()

    var frenji:Frienji!

    // MARK: Hashable

    var hashValue : Int {
        get {
            return "\(self.frenji.dbID)".hashValue
        }
    }

    // MARK: Life cycle

    init(frenji:Frienji) {
        self.frenji = frenji

        loadFrenjiAvatarAsTexture()
        setupVBOs()

        //random for start floating direction
        self.xFloatingDirection = Int(arc4random_uniform(UInt32(2))) == 0 ? -1 : 1
        self.yFloatingDirection = Int(arc4random_uniform(UInt32(2))) == 0 ? -1 : 1

        Observable<Int>.interval(BubbleObject.kSizeAnimationDelta, scheduler: MainScheduler.instance).bindNext({ [weak self] (delta:Int) in
            self?.addRandomFactorToBubblePosition()
        }).addDisposableTo(self.disposeBag)
    }

    // MARK: Setup

    private func setupVBOs() {
        // Setup VertexColor Buffer Objects

        glGenBuffers(1, &self.vertexBuffer)
        glGenBuffers(1, &self.indexBuffer)
    }

    // MARK: Texture

    private func loadFrenjiAvatarAsTexture() {
        //scale image
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let image = self.frenji.avatar.avatarImage

            //check if we have here plain image or gif frames
            if let gifFrames = image.images {
                //frames
                self.logoTextureData = [[GLubyte]?]()
                for frame in gifFrames {
                    let scaledImage = self.scaleFrenjiImage(frame)
                    let textureData = BubbleObject.createLogoTextureDataFromImage(scaledImage)
                    self.logoTextureData?.append(textureData)
                }
                
                self.logoTextureDuration = image.duration
                self.animateFrenjiTexture()
                
            } else {
                //one image
                let scaledImage = self.scaleFrenjiImage(image)
                self.logoTextureData = [BubbleObject.createLogoTextureDataFromImage(scaledImage)]
            }
        }
    }

    private func scaleFrenjiImage(image:UIImage) -> UIImage {
        let maxWidth = ArOpenGLView.kMaximumBubbleSizeMultiplier * UIScreen.mainScreen().bounds.width
        return image.scaleToWidth(maxWidth)
    }

    private class func createLogoTextureDataFromImage(logoImage:UIImage) -> [GLubyte]?  {
        
        guard let logoCGImage = logoImage.CGImage else {
            return nil
        }
        
        let logoWidth = Int(BubbleObject.logoTextureWidth)
        let logoHeight = Int(BubbleObject.logoTextureHeight)
        
        var spriteData: [GLubyte] = Array(count: Int(logoWidth * logoHeight * 4), repeatedValue: 0)
        let bitmapInfo = CGImageAlphaInfo.PremultipliedLast.rawValue
        
        if let colorSpace = CGImageGetColorSpace(logoCGImage), let spriteContext = CGBitmapContextCreate(&spriteData, logoWidth, logoHeight, 8, logoWidth * 4, colorSpace, bitmapInfo) {
            CGContextDrawImage(spriteContext, CGRectMake(0, 0, CGFloat(logoWidth) , CGFloat(logoHeight)), logoCGImage)
        }
        
        return spriteData
    }

    class func activateTextureUnit(textureLogoUniform:GLuint) {
        glActiveTexture(UInt32(GL_TEXTURE1))

        var pointer = GLuint()
        glGenTextures(1, &pointer)
        glBindTexture(UInt32(GL_TEXTURE_2D), pointer)
        BubbleObject.logoTexturePointer = pointer

        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MAG_FILTER), GL_NEAREST)
        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_MIN_FILTER), GL_NEAREST)

        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_S), Int32(GL_CLAMP_TO_EDGE))
        glTexParameteri(UInt32(GL_TEXTURE_2D), UInt32(GL_TEXTURE_WRAP_T), Int32(GL_CLAMP_TO_EDGE))

        createLogoTexture(textureLogoUniform)
    }

    class func createLogoTexture(textureLogoUniform:GLuint) {
        //allocate texture data
        glTexImage2D(UInt32(GL_TEXTURE_2D), 0, GL_RGBA, BubbleObject.logoTextureWidth, BubbleObject.logoTextureHeight, 0, UInt32(GL_RGBA), UInt32(GL_UNSIGNED_BYTE), nil)
        glUniform1i(Int32(textureLogoUniform), 1)
    }

    private func replaceLogoTextureContent() {
        //check if we have logo texture
        guard let textureData = self.logoTextureData where self.currentLogoTextureDataIndex < textureData.count else {
            return
        }
        
        if let frameData = textureData[self.currentLogoTextureDataIndex] {
            glTexSubImage2D(UInt32(GL_TEXTURE_2D), 0, 0, 0, BubbleObject.logoTextureWidth, BubbleObject.logoTextureHeight, UInt32(GL_RGBA), UInt32(GL_UNSIGNED_BYTE), frameData)
        }
    }

    class func destroyLogoTexture() {
        if var pointer = BubbleObject.logoTexturePointer {
            glDeleteTextures(1, &pointer)
        }
    }

    // MARK: Animations
    
    private func animateFrenjiTexture() {
        
        guard let textureData = self.logoTextureData else {
            return
        }
        
        let interval = self.logoTextureDuration / Double(textureData.count)
        
        Observable<Int>.interval(interval, scheduler: MainScheduler.instance).bindNext({ [weak self] (delta:Int) in
            if let strongSelf = self {
                strongSelf.currentLogoTextureDataIndex = strongSelf.currentLogoTextureDataIndex + 1 < textureData.count - 1 ? strongSelf.currentLogoTextureDataIndex + 1 : 0
            }
        }).addDisposableTo(self.disposeBag)
    }

    private func animateBubbleSize(newSize:CGFloat, viewportWidth:CGFloat, viewportHeight:CGFloat) {
        //calculate size
        let newWidth = newSize * BubbleObject.kCoordinateSpaceSize / viewportWidth
        let newHeight = newSize * BubbleObject.kCoordinateSpaceSize / viewportHeight

        //do smooth transition between old and new size
        self.currentSizeAnimationDelta = 0
        self.sizeDisposeBag = DisposeBag()

        Observable<Int>.interval(BubbleObject.kSizeAnimationDelta, scheduler: MainScheduler.instance).bindNext({ [weak self] (delta:Int) in

            self?.currentSizeAnimationDelta += BubbleObject.kSizeAnimationDelta

            if let strongSelf = self where strongSelf.currentSizeAnimationDelta < 1 {

                let finalWidth = strongSelf.bubbleSize.width + CGFloat(BubbleObject.kSizeAnimationDelta) * (newWidth - strongSelf.bubbleSize.width)
                let finalHeight = strongSelf.bubbleSize.height + CGFloat(BubbleObject.kSizeAnimationDelta) * (newHeight - strongSelf.bubbleSize.height)

                self?.bubbleSize = CGSizeMake(finalWidth, finalHeight)
                self?.updateBubbleVertices()

            } else {

                self?.sizeDisposeBag = nil
            }

            }).addDisposableTo(self.sizeDisposeBag!)
    }

    // MARK: Data

    func getBoundingBoxForScreenPixelCoordinates(screenSize:CGSize, viewportSize:CGSize) -> CGRect {
        //bubble position
        let position = CGPointMake(self.bubblePosition.x + self.xFloatingValue, self.bubblePosition.y + self.yFloatingValue + self.bubbleVerticalOffset) // UIKit origin at top, OpenGL at bottom
        let pixelSize = self.bubbleSizePixels
        
        let ratioWidth = screenSize.width / viewportSize.width
        let ratioHeight = screenSize.height / viewportSize.height
        
        let center = CGPointMake(position.x + self.bubbleSize.width/2, position.y + self.bubbleSize.height/2)
        let centerX = (viewportSize.width * (1 + center.x) / BubbleObject.kCoordinateSpaceSize) * ratioWidth
        let centerY = (viewportSize.height * (1 - center.y) / BubbleObject.kCoordinateSpaceSize) * ratioHeight
        
        return CGRectMake(centerX - pixelSize/2, centerY - pixelSize/2, pixelSize, pixelSize)
    }

    func calculateBubbleSizeForDesiredSize(desiredSize:CGFloat, viewportWidth:CGFloat, viewportHeight:CGFloat, animated:Bool) {
        precondition(viewportWidth > 0)
        precondition(viewportHeight > 0)

        if animated {

            self.animateBubbleSize(desiredSize, viewportWidth: viewportWidth, viewportHeight: viewportHeight)

        } else {

            let newWidth = desiredSize * BubbleObject.kCoordinateSpaceSize / viewportWidth
            let newHeight = desiredSize * BubbleObject.kCoordinateSpaceSize / viewportHeight

            self.bubbleSize = CGSizeMake(newWidth, newHeight)
            self.updateBubbleVertices()
        }
        
        self.bubbleSizePixels = desiredSize
    }
    
    func setBubbleVerticalOffsetForSize(bubbleSize:CGFloat, maxSize:CGFloat, minSize:CGFloat) {
        let offset = (bubbleSize - minSize) * BubbleObject.kMaxVerticalOffset / (maxSize - minSize)        
        self.bubbleVerticalOffset = offset
    }
    
    func calculateBubbleOffsetMultiplier(bubbleSize:CGFloat, maxSize:CGFloat, minSize:CGFloat) {
        self.bubbleOffsetMultiplier = (bubbleSize - minSize) / (maxSize - minSize)
    }

    func calculateBubblePositionForYMotion(inclination:CGFloat) {
        let kDefaultBubbleYOrigin = BubbleObject.kMinVerticalOffset
        let newYPosition = kDefaultBubbleYOrigin - 2 * inclination

        //low pass filter
        let yPosition = BubbleObject.kLowPassFilterVerticalValue * self.bubblePosition.y + (1.0 - BubbleObject.kLowPassFilterVerticalValue) * newYPosition
        self.bubblePosition = CGPointMake(self.bubblePosition.x, yPosition)

        updateBubbleVertices()
    }

    func calculateBubblePositionForBearing(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat, animated:Bool) {
        precondition(minFieldOfViewAngle > 0)
        precondition(maxFieldOfViewAngle > 0)

        if animated {
            //do smooth transition between old and new location
            self.currentLocationAnimationDelta = 0
            self.locationAnimationDisposeBag = DisposeBag()

            //check animation direction
            let current = self.bubbleBearing ?? 0
            let rightDistance = current <= bearing ? bearing - current : 360 - current + bearing
            let leftDistance = current < bearing ? current + 360 - bearing : current - bearing
            let distance = rightDistance <= leftDistance ? rightDistance : -leftDistance

            Observable<Int>.interval(BubbleObject.kSizeAnimationDelta, scheduler: MainScheduler.instance).bindNext({ [weak self] (delta:Int) in
                self?.currentLocationAnimationDelta += BubbleObject.kPositionAnimationDelta

                if let strongSelf = self, let currentBearing = self?.bubbleBearing where strongSelf.currentLocationAnimationDelta <= 1 {
                    let finalBearing = currentBearing + CGFloat(BubbleObject.kPositionAnimationDelta) * distance
                    strongSelf.bubbleBearing = finalBearing > 0 ? finalBearing % 360 : 360 + finalBearing

                } else {
                    self?.locationAnimationDisposeBag = nil
                    self?.bubbleBearing = bearing
                }

                }).addDisposableTo(self.locationAnimationDisposeBag!)

        } else {

            if self.bubbleBearing == nil {
                self.bubbleBearing = bearing
            }

            self.normalizeAndCalculateBubblePositionForAngle(self.bubbleBearing, minFieldOfViewAngle: minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle)
        }
    }

    private func normalizeAndCalculateBubblePositionForAngle(bearing:CGFloat?, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) {
        var minAngle = minFieldOfViewAngle
        var maxAngle = maxFieldOfViewAngle
        var bubbleBearing = bearing ?? 0

        if minFieldOfViewAngle > maxFieldOfViewAngle && bearing > maxFieldOfViewAngle {
            //for example min: 340, bearing 350, max: 40
            maxAngle = maxFieldOfViewAngle + 360

        } else if minFieldOfViewAngle > maxFieldOfViewAngle && bearing < maxFieldOfViewAngle {
            //for example min: 340, bearing 15, max: 40
            maxAngle = maxFieldOfViewAngle + 360
            bubbleBearing = bubbleBearing + 360

        } else if bearing > 360 - (maxFieldOfViewAngle - minFieldOfViewAngle) && !isFieldOfViewOver180Degrees(minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle) {
            //for example min: 10, bearing 350, max: 60
            let distance = abs(360 - bubbleBearing)

            minAngle = minFieldOfViewAngle + distance
            maxAngle = maxFieldOfViewAngle + distance
            bubbleBearing = distance

        } else if bearing < (maxFieldOfViewAngle - minFieldOfViewAngle) && isFieldOfViewOver180Degrees(minFieldOfViewAngle, maxFieldOfViewAngle: maxFieldOfViewAngle)  {
            //for example min: 320, bearing 10, max: 350
            let distance = bubbleBearing

            minAngle = minFieldOfViewAngle - distance
            maxAngle = maxFieldOfViewAngle - distance
            bubbleBearing = 360 - distance
        }

        //check if in range
        if self.isBearingInRangeOfFieldOfView(bubbleBearing, minFieldOfViewAngle: minAngle, maxFieldOfViewAngle: maxAngle) {
            self.shouldBubbleBeDrawn = true
            updateBubbleHorizontalPosition(bubbleBearing, minFieldOfViewAngle: minAngle, maxFieldOfViewAngle: maxAngle)

        } else {
            self.shouldBubbleBeDrawn = false
        }
    }

    private func isFieldOfViewOver180Degrees(minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) -> Bool {
        return minFieldOfViewAngle > 180 && maxFieldOfViewAngle > 180
    }

    private func isBearingInRangeOfFieldOfView(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) -> Bool {
        let fieldOfViewMargin = (maxFieldOfViewAngle - minFieldOfViewAngle) / 5

        var isBearingInRange = minFieldOfViewAngle < maxFieldOfViewAngle
        isBearingInRange = isBearingInRange && (bearing < maxFieldOfViewAngle + fieldOfViewMargin && bearing > minFieldOfViewAngle - fieldOfViewMargin)

        return isBearingInRange
    }

    private func updateBubbleHorizontalPosition(bearing:CGFloat, minFieldOfViewAngle:CGFloat, maxFieldOfViewAngle:CGFloat) {
        let angleRange = maxFieldOfViewAngle - minFieldOfViewAngle
        let bubbleBearing = angleRange - (maxFieldOfViewAngle - bearing)

        let position = -1 + (bubbleBearing * BubbleObject.kCoordinateSpaceSize / angleRange) //this is center so we have to substract half of the width
        let newPosition = CGPointMake(position - self.bubbleSize.width/2, 0)

        //low pass filter
        let xPosition = BubbleObject.kLowPassFilterHorizontalValue * self.bubblePosition.x + (1.0 - BubbleObject.kLowPassFilterHorizontalValue) * newPosition.x
        self.bubblePosition = CGPointMake(xPosition, self.bubblePosition.y)

        updateBubbleVertices()
    }

    private func updateBubbleVertices() {
        let position = CGPointMake(self.bubblePosition.x + self.xFloatingValue, self.bubblePosition.y + self.yFloatingValue + self.bubbleVerticalOffset)

        self.bubbleVertices.0.position = (CFloat(position.x + self.bubbleSize.width), CFloat(position.y), 0) //right bottom
        self.bubbleVertices.1.position = (CFloat(position.x + self.bubbleSize.width), CFloat(position.y + self.bubbleSize.height), 0) //right top
        self.bubbleVertices.2.position = (CFloat(position.x), CFloat(position.y + self.bubbleSize.height), 0) //left top
        self.bubbleVertices.3.position = (CFloat(position.x), CFloat(position.y), 0) //left bottom
    }
    
    private func insetBubbleVerticesByFactor(factor:CGFloat) {
        //if factor > 0 we are increasing bubble size
        //if factor < 0 we are decreasing bubble size
        
        let offsetWidth = (self.bubbleSize.width * factor) / 2
        let offsetHeight = (self.bubbleSize.height * factor) / 2
        
        let originalPosition = CGPointMake(self.bubblePosition.x + self.xFloatingValue, self.bubblePosition.y + self.yFloatingValue + self.bubbleVerticalOffset)
        
        let position = CGPointMake(originalPosition.x - offsetWidth, originalPosition.y - offsetHeight)
        let width = self.bubbleSize.width + offsetWidth * 2
        let height = self.bubbleSize.height + offsetHeight * 2
        
        self.bubbleVertices.0.position = (CFloat(position.x + width), CFloat(position.y), 0) //right bottom
        self.bubbleVertices.1.position = (CFloat(position.x + width), CFloat(position.y + height), 0) //right top
        self.bubbleVertices.2.position = (CFloat(position.x), CFloat(position.y + height), 0) //left top
        self.bubbleVertices.3.position = (CFloat(position.x), CFloat(position.y), 0) //left bottom
    }

    private func addRandomFactorToBubblePosition() {
        //the further away bubble is the less it should move
        let kXMaxRandomFactor:CGFloat = 0.3 * self.bubbleOffsetMultiplier
        let kYMaxRandomFactor:CGFloat = 0.14 * self.bubbleOffsetMultiplier

        //switch direction
        if abs(self.xFloatingValue) >= kXMaxRandomFactor {
            self.xFloatingDirection *= -1
        }

        if abs(self.yFloatingValue) >= kYMaxRandomFactor {
            self.yFloatingDirection *= -1
        }

        //add random value
        let xRandom:CGFloat = 0.0035 * self.bubbleOffsetMultiplier
        let yRandom:CGFloat = 0.0008 * self.bubbleOffsetMultiplier

        self.xFloatingValue += xRandom * self.xFloatingDirection
        self.yFloatingValue += yRandom * self.yFloatingDirection
    }

    // MARK: Render

    func render(positionSlot:GLuint, textureCoordinateSlot:GLuint, textureUniform:GLuint) {
        if let uniform = self.flatImageUniform {
            glUniform1i(Int32(uniform), self.isExplosionAnimationActive ? 1 : 0)
        }
        
        //pass to shader if we should play glow effect animation
        if let uniform = self.glowAnimationUniform, let timeUniform = self.glowAnimationTimeUniform where !self.isExplosionAnimationActive {
            glUniform1i(Int32(uniform), self.frenji.relation == .Catched || self.isHoldAnimationActive ? 1 : 0)
            
            //set time for glow effect
            glUniform1f(Int32(timeUniform), Float(NSDate().timeIntervalSinceDate(self.holdAnimationStartDate)))
        }

        //we have to increase bubble size for explosion
        if self.isExplosionAnimationActive {
            self.insetBubbleVerticesByFactor(1.5)
            
            //change animation image
            self.changeExplosionFrame()
        } else {
            //bind logo texture
            self.replaceLogoTextureContent()
        }

        //check if we should even draw this bubble (we check this here because we want to increase animation frame in case user started this on screen)
        guard self.shouldBubbleBeDrawn else {
            return
        }
        
        //bind buffers
        glBindBuffer(UInt32(GL_ARRAY_BUFFER), self.vertexBuffer)
        glBufferData(UInt32(GL_ARRAY_BUFFER), Int(sizeofValue(self.bubbleVertices)), &self.bubbleVertices, UInt32(GL_STATIC_DRAW))
        
        glBindBuffer(UInt32(GL_ELEMENT_ARRAY_BUFFER), self.indexBuffer)
        glBufferData(UInt32(GL_ELEMENT_ARRAY_BUFFER), Int(sizeofValue(self.bubbleIndices)), &self.bubbleIndices, UInt32(GL_STATIC_DRAW))
        
        //set up pointers
        let positionSlotPointer = UnsafePointer<Int>(bitPattern: 0)
        glVertexAttribPointer(positionSlot, 3, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), positionSlotPointer)
        
        let textCoordinatePointer = UnsafePointer<Int>(bitPattern: sizeof(Float) * 3)
        glVertexAttribPointer(textureCoordinateSlot, 2, UInt32(GL_FLOAT), UInt8(GL_FALSE), Int32(sizeof(Vertex)), textCoordinatePointer)
        
        let vertextBufferOffset = UnsafePointer<Int>(bitPattern: 0)
        glDrawElements(UInt32(GL_TRIANGLES), Int32(GLfloat(sizeofValue(self.bubbleIndices)) / GLfloat(sizeofValue(self.bubbleIndices.0))), UInt32(GL_UNSIGNED_BYTE), vertextBufferOffset)
    }
    
    // MARK: Explosion animation
    
    private class func preloadBubbleExplosionFrames() -> [[GLubyte]] {
        //frames base name
        let kAnimationFramesName = "ExplosionFrames/explosion_frame_"
        let kAnimationFramesCount = 45
        
        var explosionImages = [[GLubyte]]()
        for index in kAnimationFramesCount.stride(to: 0, by: -1) {
            if let frameImage = UIImage(named: "\(kAnimationFramesName)\(index)"), let imageData = BubbleObject.createLogoTextureDataFromImage(frameImage) {
                explosionImages.append(imageData)
            }
        }
        
        return explosionImages
    }
    
    func playExplosionAnimation(completion:(() -> Void)?) {
        //set callback
        self.explosionAnimationEndCallback = completion
        
        //reset state
        self.explosionAnimationFrameIndex = 0
        self.isExplosionAnimationActive = true
    }
    
    private func changeExplosionFrame() {
        //check if we are on last frame - 2 (second to last) of animation,
        //if we are call completion callback and stop animation with delay (so UI can show over on catched trace)
        if self.explosionAnimationFrameIndex == BubbleObject.kBubbleExplosionImages.count - 2 {
            self.explosionAnimationEndCallback?()
            
            performWithDelay(2, closure: {
                self.stopExplosionAnimation()
            })
        }
        
        let frameIndex = self.explosionAnimationFrameIndex < BubbleObject.kBubbleExplosionImages.count ? self.explosionAnimationFrameIndex : BubbleObject.kBubbleExplosionImages.count - 1
        
        let textureData = BubbleObject.kBubbleExplosionImages[frameIndex]
        glTexSubImage2D(UInt32(GL_TEXTURE_2D), 0, 0, 0, BubbleObject.logoTextureWidth, BubbleObject.logoTextureHeight, UInt32(GL_RGBA), UInt32(GL_UNSIGNED_BYTE), textureData)
        
        self.explosionAnimationFrameIndex += 1
    }
    
    private func stopExplosionAnimation() {
        self.insetBubbleVerticesByFactor(0)
        self.isExplosionAnimationActive = false
    }
    
    // MARK: Hold animation (Ripple effect)
    
    func playHoldAnimation() {
        self.holdAnimationStartDate = NSDate()
        self.isHoldAnimationActive = true
    }
    
    func stopHoldAnimation() {
        self.isHoldAnimationActive = false
    }

}

// MARK: Equatable

func ==(lhs: BubbleObject, rhs: BubbleObject) -> Bool {
    return lhs.hashValue == rhs.hashValue
}
