//
//  NavigationTitleView.swift
//  Frienji
//
//  Created by adam kolodziej on 16.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

@IBDesignable class NavigationTitleView: UIView, NibInstantiate {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    
    //we want title view to be always in the center of navigation bar
    override var frame: CGRect {
        set (newValue) {
            super.frame = newValue
            
            if let superview = self.superview {
                self.center = CGPoint(x: superview.center.x, y: self.center.y)
            }
        }
        get {
            return super.frame
        }
    }
    
    // MARK: - Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    private func loadViewFromNib() {
        let view = self.instantiateFromNib()
        self.addSubviewFullscreen(view)
    }

}
