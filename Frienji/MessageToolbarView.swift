//
//  MessageToolbarView.swift
//  Frienji
//
//  Created by Adam Szeremeta on 22.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import CoreLocation
import MapKit
import MobileCoreServices

protocol MessageToolbarProtocol: class {
    
    func didTouchSendWithMesssage(message: String, forMessageToolBarView messageToolBarView: MessageToolbarView) -> Void
    func didTouchCamera(forMessageToolBarView messageToolBarView:MessageToolbarView) -> Void
    func didTouchLocation(forMessageToolBarView messageToolBarView:MessageToolbarView) -> Void
}

@IBDesignable class MessageToolbarView: UIView, NibInstantiate {
    
    //outlets
    @IBOutlet weak var inputTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet var widthButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var textViewSendButtonSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    //properties
    let inputText = Variable<String>("")
    weak var delegate: MessageToolbarProtocol?
    @IBInspectable var localizedPlaceholder: String? {
        didSet {
            if let placeholder = localizedPlaceholder {
                placeholderLabel.text = NSLocalizedString(placeholder, comment: "")
            }
        }
    }
    
    @IBInspectable var buttonTitle: String? {
        didSet {
            if let buttonTitle = buttonTitle {
                sendButton.setTitle(NSLocalizedString(buttonTitle, comment: ""), forState: .Normal)
            }
        }
    }
    
    private let defaultSendButtonToTextViewSpace: CGFloat = 12
    private let disposeBag = DisposeBag()
    
    // MARK: - Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    private func loadViewFromNib() {
        let view = self.instantiateFromNib()
        self.addSubviewFullscreen(view)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.inputTextView.layer.cornerRadius = 5
        inputTextView.delegate = self
        
        setUpObservables()
        setUpBindings()
    }
    
    // MARK: Actions
    
    @IBAction func onCameraButtonTouch(sender: AnyObject) {
        self.delegate?.didTouchCamera(forMessageToolBarView: self)
    }
    
    @IBAction func onLocationButtonTouch(sender: AnyObject) {
        self.delegate?.didTouchLocation(forMessageToolBarView: self)
    }
    
    @IBAction func onSendButtonTouch(sender: AnyObject) {
        self.delegate?.didTouchSendWithMesssage(self.inputText.value, forMessageToolBarView: self)
        
        //clear
        self.inputTextView.text = ""
        
        //end editing
        self.endEditing(true)
    }
    
    // MARK: Observables
    
    private func setUpBindings() {
        self.inputTextView.rx_text.bindTo(self.inputText).addDisposableTo(self.disposeBag)
    }
    
    private func setUpObservables() {
        self.inputText.asObservable().map { $0.isEmpty }
            .bindTo(widthButtonConstraint.rx_active)
            .addDisposableTo(self.disposeBag)
        
        self.inputText.asObservable().map { $0.isEmpty }
        .bindNext { [unowned self] empty in
            self.textViewSendButtonSpaceConstraint.constant = empty ? 0 : self.defaultSendButtonToTextViewSpace
        }.addDisposableTo(disposeBag)
    }

}

extension MessageToolbarView: UITextViewDelegate {
    func textViewDidChange(textView: UITextView) {
        placeholderLabel.hidden = !textView.text.isEmpty
    }
}
