//
//  UIFont+Lato.swift
//  Frienji
//
//  Created by bolek on 03.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

extension UIFont {
    
    class func latoRegulatWithSize(size:CGFloat) -> UIFont {
        return UIFont(name: "Lato-Regular", size:size)!
    }
    
    class func latoBoldWithSize(size:CGFloat) -> UIFont {
        return UIFont(name: "Lato-Bold", size:size)!
    }
    
    class func latoLightWithSize(size:CGFloat) -> UIFont {
        return UIFont(name: "Lato-Light", size:size)!
    }
    
    class func latoItalicWithSize(size:CGFloat) -> UIFont {
        return UIFont(name: "Lato-Italic", size:size)!
    }
}
