//
//  LocationPermissionViewController.swift
//  Frienji
//
//  Created by bolek on 01.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class LocationPermissionViewController: UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "Introductions"
    static var storyboardControllerId = "LocationPermissionViewController"
    
    //MARK: - View lefecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if(!PermissionsHelper.hasGrantedLocationPermission()) {
            LocationManager.sharedInstance
        }
    }
    
    private func showLocationAccessRequiredAlert(withAction:Bool) {
        if(withAction) {
            LocationManager.sharedInstance.requestWhenInUseAuthorization()
        } else {
            UIAlertUtils.showAlertWithTitle(Localizations.locationPermission.alertTitle,
                                            message: Localizations.locationPermission.alertMessage,
                                            fromController: self, showCompletion: nil)
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func yesBtnClicked(sender:AnyObject) {
        if(!PermissionsHelper.hasGrantedLocationPermission()) {
            self.showLocationAccessRequiredAlert(true)
            return
        }
        if(Settings.sharedInstance.userVerified) {
            self.performSegueWithIdentifier("arSegue", sender: nil)
        } else {
            self.performSegueWithIdentifier("signInOrUpSegue", sender: nil)
        }
    }
}
