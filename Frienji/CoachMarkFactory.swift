//
//  CoachMarkFactory.swift
//  Frienji
//
//  Created by Adam Szeremeta on 08.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class CoachMarkFactory {
    
    enum CoachMarkType: Int {
        
        case Ar = 0
        case Zoo
        case UserActivities
        case UserWall
        case Chat
        case FrienjiWall
        case Settings

        //coach mark texts
        static let texts: [CoachMarkType : (title:String, description:String, action:String)] = [
            
            CoachMarkType.Ar: (Localizations.coachmark.ar_coach_mark_title, Localizations.coachmark.ar_coach_mark_content, Localizations.coachmark.got_it),
            CoachMarkType.Zoo: (Localizations.coachmark.zoo_coach_mark_title, Localizations.coachmark.zoo_coach_mark_content, Localizations.coachmark.got_it),
            CoachMarkType.UserActivities: (Localizations.coachmark.user_actions_coach_mark_title, Localizations.coachmark.user_actions_coach_mark_content, Localizations.coachmark.got_it),
            CoachMarkType.UserWall: (Localizations.coachmark.user_wall_coach_mark_title, Localizations.coachmark.user_wall_coach_mark_content, Localizations.coachmark.got_it),
            CoachMarkType.Chat: (Localizations.coachmark.conversation_coach_mark_title, Localizations.coachmark.conversation_coach_mark_content, Localizations.coachmark.got_it),
            CoachMarkType.FrienjiWall: (Localizations.coachmark.frienji_wall_coach_mark_title, Localizations.coachmark.frienji_wall_coach_mark_content, Localizations.coachmark.got_it),
            CoachMarkType.Settings: (Localizations.coachmark.my_settings_wall_coach_mark_title, Localizations.coachmark.my_settings_wall_coach_mark_content, Localizations.coachmark.got_it),
        ]
    }
    
    // MARK: Create
    
    class func createForType(type:CoachMarkType, withDismissCallback dismissCallback:(() -> Void)? = nil) -> CoachMarkView {
        let coachMark = CoachMarkView.loadFromNib()
        coachMark.dismissCallback = dismissCallback
        
        if let texts = CoachMarkType.texts[type] {
            coachMark.configureWithTitle(texts.title, description: texts.description, actionTitle: texts.action)
        }
        
        return coachMark
    }
    
}
