//
//  AppShortcutUtils.swift
//  Frienji
//
//  Created by Adam Szeremeta on 19.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class AppShortcutUtils {
    
    enum ShortcutItem: String {
        
        case AugmentedReality
        case ChangeYourFrienji
        case GoToYourZoo
        
        static let allValues = [AugmentedReality, ChangeYourFrienji, GoToYourZoo]
        
        func getTitle() -> String {
            switch self {
            case .AugmentedReality:
                return Localizations.app_shortcut.augmented_reality
                
            case .ChangeYourFrienji:
                return Localizations.app_shortcut.change_your_frienji
                
            case .GoToYourZoo:
                return Localizations.app_shortcut.go_to_your_zoo
            }
        }
        
        func getIcon() -> UIApplicationShortcutIcon {
            switch self {
            case .AugmentedReality:
                return UIApplicationShortcutIcon(templateImageName: "shortcut_ar")
                
            case .ChangeYourFrienji:
                return UIApplicationShortcutIcon(templateImageName: "shortcut_frienji")
                
            case .GoToYourZoo:
                return UIApplicationShortcutIcon(templateImageName: "shortcut_zoo")
            }
        }
    }
    
    // MARK: App Shortcuts
    
    class func activateAppShortcutItems(application application:UIApplication) {
        application.shortcutItems = self.createAppShortcutItems()
    }
    
    class func deactivateAppShortcutItems(application application:UIApplication) {
        application.shortcutItems = nil
    }
    
    // MARK: Actions
    
    class func handleActionForAppShortcut(shortcutItem:UIApplicationShortcutItem, application:UIApplication) -> Bool {
        
        guard let itemType = ShortcutItem(rawValue: shortcutItem.type) else {
            return false
        }
        
        switch itemType {
        case .AugmentedReality:
            self.navigateToArScreen(application)
            
        case .ChangeYourFrienji:
            self.navigateToSettingsScreen(application)
            
        case .GoToYourZoo:
            self.navigateToZooScreen(application)
        }
        
        return true
    }
    
    // MARK: Helpers (Items)
    
    private class func createAppShortcutItems() -> [UIApplicationShortcutItem] {
        var shortcuts = [UIApplicationShortcutItem]()
        
        for item in ShortcutItem.allValues {
            let shortcut = UIApplicationShortcutItem(type: item.rawValue, localizedTitle: item.getTitle(), localizedSubtitle: nil, icon: item.getIcon(), userInfo: nil)
            shortcuts.append(shortcut)
        }
        
        return shortcuts
    }
    
    // MARK: Helpers (Navigation)
    
    private class func navigateToArScreen(application:UIApplication) {
        
        guard let navigationController = application.keyWindow?.rootViewController as? UINavigationController else {
            return
        }
        
        self.popToRootController(navigationController)
    }
    
    private class func navigateToSettingsScreen(application:UIApplication) {
        
        guard let navigationController = application.keyWindow?.rootViewController as? UINavigationController
        where !(navigationController.viewControllers.last is SettingsViewController) else {
            return
        }
        
        self.popToRootController(navigationController)
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self))
        if let wallController = mainStoryboard.instantiateViewControllerWithIdentifier(R.storyboard.main.myWallPostsViewController.identifier) as? WallPostsViewController, let myFrienji = Settings.sharedInstance.userFrienji {
            wallController.input.loaded.onNext(myFrienji)
            
            navigationController.pushViewController(wallController, animated: false)
        }
        
        let settingsController = SettingsViewController.loadFromStoryboard()
        navigationController.pushViewController(settingsController, animated: true)
    }
    
    private class func navigateToZooScreen(application:UIApplication) {
        
        guard let navigationController = application.keyWindow?.rootViewController as? UINavigationController
        where !(navigationController.viewControllers.last is ZooViewController) else {
            return
        }
        
        self.popToRootController(navigationController)
        
        let zooController = ZooViewController.loadFromStoryboard()
        navigationController.pushViewController(zooController, animated: true)
    }
    
    private class func popToRootController(navigationController:UINavigationController) {
        navigationController.dismissViewControllerAnimated(false, completion: nil)
        navigationController.popToRootViewControllerAnimated(false)
    }
    
}
