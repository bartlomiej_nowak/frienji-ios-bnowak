//
//  TracesApiError.swift
//  Traces
//
//  Created by Adam Szeremeta on 18.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import CFNetwork
import Alamofire
import RxSwift

struct ApiError: ErrorType {
    let statusCode: Int
}

class FrienjiApiError {

    static let kCustomCodeKey = "apiErrorCustomCode"
    
    enum HttpStatusCode: Int {

        case Unauthorized = 401

        var message: String {
            switch self {
            case .Unauthorized:
                return Localizations.api_error.bad_credentials
            }
        }
    }
    
    enum ApiErrorCode: Int {
        
        case BadCredentials = 10001
        case UsernameEmpty = 40001
        case UsernameTaken = 40004
        case EmptyAvatar = 50001
        case EmptyPhoneNumber = 60001
        case PhoneNumberTaken = 60004
        case PhoneNumberIncorrect = 61001
        case PhoneNumberNotMobile = 61002
        case EmptyCountryCode = 70001
        case EmptyMessage = 90001
        
        var message: String {
            switch self {
            case .BadCredentials:
                return Localizations.api_error.bad_credentials
                
            case .UsernameEmpty:
                return Localizations.api_error.username_empty
                
            case .UsernameTaken:
                return Localizations.api_error.username_taken
                
            case .EmptyAvatar:
                return Localizations.api_error.empty_avatar
                
            case .EmptyPhoneNumber:
                return Localizations.api_error.phone_number_empty
                
            case .PhoneNumberTaken:
                return Localizations.api_error.phone_number_taken
                
            case .PhoneNumberIncorrect:
                return Localizations.api_error.phone_number_incorrect
                
            case .PhoneNumberNotMobile:
                return Localizations.api_error.phone_number_not_mobile
                
            case .EmptyCountryCode:
                return Localizations.api_error.country_code_empty
                
            case .EmptyMessage:
                return Localizations.api_error.message_empty
            }
        }
    }
    
    // MARK: Properties

    private (set) var statusCode: Int?
    private (set) var errorCode: Int?
    private (set) var apiErrorCodes: [ApiErrorCode]?
    
    var message: String {
        var errorMessage: String = ""
        
        //api codes
        if let apiErrorCodes = self.apiErrorCodes {
            for code in apiErrorCodes {
                errorMessage += errorMessage.isEmpty ? code.message : "\n\(code.message)"
            }
            
           return errorMessage
        }
        
        //network code
        if let networkErrorCode = self.errorCode where networkErrorCode == NSURLErrorNotConnectedToInternet {
            return Localizations.api_error.no_internet_connection
        }
        
        //status code
        if let httpCode = self.statusCode, let httpError = HttpStatusCode(rawValue: httpCode) {
            return httpError.message
        }
        
        return Localizations.api_error.other
    }

    // MARK: Init

    init(statusCode: Int?, errorCode:Int?, apiErrorCodes: [Int]?) {
        self.statusCode = statusCode
        self.errorCode = errorCode
        
        if let codes = apiErrorCodes {
            var apiCodes = [ApiErrorCode]()
            for code in codes {
                if let apiCode = ApiErrorCode(rawValue: code) {
                    apiCodes.append(apiCode)
                }
            }
            
            self.apiErrorCodes = apiCodes
        }
    }

    class func fromError(error: ErrorType) -> FrienjiApiError {
        if let apiError = error as? ApiError {
            return FrienjiApiError(statusCode: apiError.statusCode, errorCode: apiError.statusCode, apiErrorCodes: nil)
        }
        
        let error = error as NSError
        return FrienjiApiError(statusCode: error.userInfo[Error.UserInfoKeys.StatusCode] as? Int, errorCode: error.code, apiErrorCodes: error.userInfo[FrienjiApiError.kCustomCodeKey] as? [Int])
    }

}
