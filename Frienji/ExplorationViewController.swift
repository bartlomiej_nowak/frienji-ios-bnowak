//
//  ExplorationViewController.swift
//  Frienji
//
//  Created by Piotr Łyczba on 07/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift

struct ExplorationInput: Input {
    let catched = PublishSubject<Frienji>()
    let rejected = PublishSubject<Frienji>()
}

class ExplorationViewController: ArViewController, HasInput {

    // MARK: - Properties

    let kBackButtonOffset: CGFloat = -15.0
    let kNavigationBarFontSize: CGFloat = 17.0

    let darkBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.blackColor()
        view.alpha = 0.5

        return view
    }()

    private var sliderView: ArSliderView!
    
    var input = ExplorationInput()

    var explorationViewModel: ExplorationViewModel!
    
    private var shouldShowNavigationBarWhenDisappearing = true

    // MARK: - Outlets

    @IBOutlet weak var myWallButton: UIButton!
    @IBOutlet weak var activitiesButton: UIButton!

    let disposeBag = DisposeBag()

    // MARK: - View's lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        arOpenGLView.delegate = self
        
        configureNavigationBar()
        addSliderView()

        createBindings()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        explorationViewModel.shouldReloadFrienji.onNext()
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if !Settings.sharedInstance.wasCoachMarkShownForType(CoachMarkFactory.CoachMarkType.Ar) {
            let coachmark = CoachMarkFactory.createForType(CoachMarkFactory.CoachMarkType.Ar) {
                Settings.sharedInstance.setCoachMarkAsShownForType(CoachMarkFactory.CoachMarkType.Ar)
            }
            coachmark.showWithAnimationInContainer()
        }
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        if self.shouldShowNavigationBarWhenDisappearing {
            self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
    }
    
    // MARK: ArSliderView
    
    private func addSliderView() {
        self.sliderView = ArSliderView.loadFromNib()
        
        self.sliderView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.sliderView)
        
        let heightConstraint = NSLayoutConstraint(item: self.sliderView, attribute: .Height, relatedBy: .Equal, toItem: self.view, attribute: .Height, multiplier: 0.6, constant: 0)
        let xConstraint = NSLayoutConstraint(item: self.sliderView, attribute: .Trailing, relatedBy: .Equal, toItem: self.view, attribute: .Trailing, multiplier: 1.0, constant: 0)
        let yConstraint = NSLayoutConstraint(item: self.sliderView, attribute: .CenterY, relatedBy: .Equal, toItem: self.view, attribute: .CenterY, multiplier: 1.0, constant: 0)
        
        self.view.addConstraints([heightConstraint, xConstraint, yConstraint])
    }

    // MARK: - Configure

    func configureNavigationBar() {
        navigationController?.navigationBar.backIndicatorImage = UIImage()
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(kBackButtonOffset, 0), forBarMetrics: .Default)
        UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont.boldSystemFontOfSize(kNavigationBarFontSize)], forState: .Normal)
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.appOrangePlaceholderColor(), NSFontAttributeName: UIFont.boldSystemFontOfSize(kNavigationBarFontSize)]
    }

    // MARK: - Bindings

    func createBindings() {
        explorationViewModel = ExplorationViewModel(navigationHandler: NavigationHandler(sourceViewController: self))
        input.catched.bindTo(explorationViewModel.catched).addDisposableTo(disposeBag)
        input.rejected.bindTo(explorationViewModel.rejected).addDisposableTo(disposeBag)
        explorationViewModel.frienjisAround.asDriver().driveNext(arOpenGLView.updateWithTraces).addDisposableTo(disposeBag)

        // Dark background
        explorationViewModel.darkBackgroundVisible.asDriver()
            .driveNext { [unowned self] visible in self.switchDarkBackground(visible) }
            .addDisposableTo(disposeBag)

        // Remove bubble from AR
        Observable.of(input.catched, input.rejected).merge()
            .subscribeNext(arOpenGLView.updateFrenji)
            .addDisposableTo(disposeBag)

        // Animate Zoo button
        input.catched
            .subscribeNext { [unowned self] _ in self.animateProfileIconAfterCatchingTrace() }
            .addDisposableTo(disposeBag)

        // Navigation
        myWallButton.rx_tap.bindTo(explorationViewModel.myWallTapped).addDisposableTo(disposeBag)
        activitiesButton.rx_tap.bindTo(explorationViewModel.activitiesTapped).addDisposableTo(disposeBag)
        
        //slider
        self.sliderView.sliderDistance.asObservable().bindTo(self.arOpenGLView.maximumObjectDistance).addDisposableTo(self.disposeBag)
        self.sliderView.sliderDistance.asObservable().bindTo(self.arOpenGLView.radarView.frenjisView.maxObjectDistance).addDisposableTo(self.disposeBag)
        self.sliderView.sliderDistance.asObservable().bindTo(self.explorationViewModel.frienjisDistance).addDisposableTo(self.disposeBag)
        self.sliderView.sliderDistance.asObservable().skip(1).debounce(0.33, scheduler: MainScheduler.instance).bindNext { [unowned self] (distance:CGFloat) in
            self.explorationViewModel.shouldReloadFrienji.onNext()
        }.addDisposableTo(self.disposeBag)
    }

    // MARK: - Exploration view controller

    func switchDarkBackground(visible: Bool) {
        if visible {
            darkBackground.frame = view.frame
            view.addSubview(darkBackground)
        } else {
            darkBackground.removeFromSuperview()
        }
    }
    
    // MARK: Segue
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        self.shouldShowNavigationBarWhenDisappearing = !(segue.destinationViewController is UINavigationController || segue.destinationViewController is ActivitiesViewController)
    }

}

// MARK: - AR OpenGL view protocol

extension ExplorationViewController: ArOpenGLViewProtocol {

    func arOpenGLView(arOpenGLView: ArOpenGLView, didCatchFrenji frenji: Frienji, frienjiBoundingBox boundingBox:CGRect) {
        if let ourFrenji = Settings.sharedInstance.userFrienji where ourFrenji.dbID == frenji.dbID {
            
            explorationViewModel.myWallTapped.onNext()
            
        } else {
            explorationViewModel.previewTapped.onNext((frenji, boundingBox))
        }
    }

}
