//
//  OverviewViewController.swift
//  Traces
//
//  Created by Adam Szeremeta on 21.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class OverviewViewController : UIViewController, StoryboardLoad {
    
    static var storyboardId: String = "Navigation"
    
    private let kControllerContainerPadding:CGFloat = 56
    
    //outlets
    @IBOutlet weak var overviewControllerContainer: UIView!
    @IBOutlet weak var overviewControllerBottomSpaceConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topActionButton: UIButton!
    @IBOutlet weak var bottomActionButton: UIButton!
    
    //properties
    private var viewModel:OverviewViewModel!
    
    private (set) var bottomController:UIViewController!
    private (set) var overviewController:UIViewController!
    
    private var overlayShown:Bool = false
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel = OverviewViewModel()
        
        showOverlayController()
    }
    
    // MARK: Setup
    
    func configureWithBottomController(bottomController:UIViewController, overviewController:UIViewController) {
        
        self.bottomController = bottomController
        self.overviewController = overviewController
        
        self.bottomController.willMoveToParentViewController(self)
        self.view.addSubviewFullscreen(self.bottomController.view, belowSubview: self.overviewControllerContainer)
        self.addChildViewController(self.bottomController)
        self.bottomController.didMoveToParentViewController(self)
        
        self.overviewController.willMoveToParentViewController(self)
        self.overviewControllerContainer.addSubviewFullscreen(self.overviewController.view)
        self.addChildViewController(self.overviewController)
        self.overviewController.didMoveToParentViewController(self)
    }
    
    // MARK: Actions
    
    @IBAction func onTopButtonTouch(sender: AnyObject) {
        
        showOverlayController()
    }
    
    @IBAction func onBottomButtonTouch(sender: AnyObject) {
        
        hideOverlayController()
    }
    
    // MARK: Show / hide overlay
    
    func showOverlayController() {
        
        if !self.overlayShown {
            
            UIView.animateWithDuration(0.33, animations: { 
                
                self.overviewControllerContainer.transform = CGAffineTransformIdentity
                
                self.topActionButton.transform = CGAffineTransformMakeTranslation(0, -self.kControllerContainerPadding)
                self.bottomActionButton.transform = CGAffineTransformIdentity
                
            }, completion: { (result:Bool) in
                    
                self.overlayShown = true
            })
        }
    }
    
    func hideOverlayController() {
        
        if self.overlayShown {
            
            UIView.animateWithDuration(0.33, animations: {
                
                self.overviewControllerContainer.transform = CGAffineTransformMakeTranslation(0, -(UIScreen.mainScreen().bounds.size.height - 2 * self.kControllerContainerPadding))
                
                self.topActionButton.transform = CGAffineTransformIdentity
                self.bottomActionButton.transform = CGAffineTransformMakeTranslation(0, self.kControllerContainerPadding)
                
            }, completion: { (result:Bool) in
                    
                self.overlayShown = false
            })
        }
    }
}