//
//  ExplorationNavigationHandler.swift
//  Frienji
//
//  Created by Piotr Łyczba on 12/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import RxSwift
import UIKit
import SegueKit

class NavigationHandler {

    weak var sourceViewController: UIViewController?

    init(sourceViewController: UIViewController?) {
        self.sourceViewController = sourceViewController
    }

    /*
     Navigates to specified view controller

     - parameter segueIdentifier:         Identifier used to get storyboard segue
     - parameter destinationType:         Destination view controller type
     - parameter destinationInputHandler: Completion handler used to configure destination
    */
    func navigate<DestinationType where DestinationType: HasInput, DestinationType: UIViewController>
        (segueIdentifier: String, destinationType: DestinationType.Type, destinationInputHandler: (DestinationType.InputType) -> Void) {
        sourceViewController?.performSegue(with: segueIdentifier) { segue in
            let segueDestination: DestinationType? = segue.destination()
            guard let destination = segueDestination else {
                assertionFailure("Destination view controller type mismatch.")
                return
            }
            destinationInputHandler(destination.input)
        }
    }

    /*
     Dismisses specified view controller

     - parameter destinationType:         Destination view controller type
     - parameter destinationInputHandler: Completion handler used to configure destination
     */
    func dismissModal<DestinationType: HasInput>(destinationType: DestinationType.Type, destinationInputHandler: (DestinationType.InputType) -> Void) {
        var viewController = sourceViewController
        while !(viewController?.presentingViewController is UINavigationController) {
            viewController?.dismissViewControllerAnimated(false, completion: nil)
            viewController = viewController?.presentingViewController
        }
        let destinationUnwrapped = viewController?.presentingViewController?.childViewControllers.filter { $0 is DestinationType }.last
        guard let destination = destinationUnwrapped as? DestinationType else {
            assertionFailure("Destination view controller type mismatch.")
            return
        }
        viewController?.dismissViewControllerAnimated(true) {
            destinationInputHandler(destination.input)
        }
    }

    /*
     Binds sequence of action inputs with segue

     - parameter segueIdentifier: Identifier used to get storyboard segue
     - parameter destinationType: Destination view controller type

     - returns: Disposable oject that can be used to unbind

     Example:

        let tap = Observable.just("Tap")
        tap
            .bindTo(navigationHandler.segue("segueIdentifier", destinationType: DestinationViewController.self)) { input, element in
                input.onNext(element)
            }
            .addDisposableTo(disposeBag)
    */
    func segue<DestinationType, O: ObservableType where DestinationType: HasInput, DestinationType: UIViewController>
        (segueIdentifier: String, destinationType: DestinationType.Type) -> (source: O) -> (configureDestination: (DestinationType.InputType, O.E) -> Void) -> Disposable {
        return { source in
            return { configureDestination in
                return source.subscribeNext { element in
                    self.navigate(segueIdentifier, destinationType: destinationType) { input in
                        configureDestination(input, element)
                    }
                }
            }
        }
    }

    /*
     Binds sequence of void actions with segue

     - parameter segueIdentifier: Identifier used to get storyboard segue
     - parameter destinationType: Destination view controller type

     - returns: Observer object that performs segue
    */
    func segue<DestinationType: UIViewController>(segueIdentifier: String, destinationType: DestinationType.Type) -> AnyObserver<Void> {
        return AnyObserver { _ in
            self.sourceViewController?.performSegue(with: segueIdentifier) { segue in
                let destination: DestinationType? = segue.destination()
                guard destination != nil else {
                    assertionFailure("Destination view controller type mismatch.")
                    return
                }
            }
        }
    }

    /*
     Binds sequence of action inputs with modal dismission

     - parameter destinationType: Destination view controller type

     - returns: Disposable oject that can be used to unbind

     Example:

     let tap = Observable.just("Tap")
     tap
        .bindTo(navigationHandler.modalDismissed(DestinationViewController.self)) { input, element in
            input.onNext(element)
        }
        .addDisposableTo(disposeBag)
     */
    func modalDismissed<DestinationType: HasInput, O: ObservableType>
        (destinationType: DestinationType.Type) -> (source: O) -> (configureDestination: (DestinationType.InputType, O.E) -> Void) -> Disposable {
        return { source in
            return { configureDestination in
                return source.subscribeNext { element in
                    self.dismissModal(destinationType) { input in
                        configureDestination(input, element)
                    }
                }
            }
        }
    }
    
    /*
     Binds sequence of action inputs with modal dismission

     - parameter destinationType: Destination view controller type

     - returns: Observer object that dismisses modal
     */
    func modalDismissed<DestinationType: UIViewController>(destinationType: DestinationType.Type) -> AnyObserver<Void> {
        return AnyObserver { _ in
            guard let source = self.sourceViewController, presenting = self.sourceViewController?.presentingViewController else {
                return
            }
            let destination: DestinationType? = presenting.childViewController()
            guard destination != nil else {
                assertionFailure("Destination view controller type mismatch.")
                return
            }
            source.dismissViewControllerAnimated(true, completion: nil)
        }
    }

    func presentModal<DestinationType: UIViewController where DestinationType: StoryboardLoad>
        (destinationType: DestinationType.Type) {
        let destination = DestinationType.loadFromStoryboard()
        sourceViewController?.presentViewController(destination, animated: true, completion: nil)
    }

    func presentModal<DestinationType: UIViewController where DestinationType: protocol<HasInput, StoryboardLoad>>
        (destinationType: DestinationType.Type, destinationInputHandler: (DestinationType.InputType) -> Void) {
        let destination = DestinationType.loadFromStoryboard()
        sourceViewController?.presentViewController(destination, animated: true) {
            destinationInputHandler(destination.input)
        }
    }

    func modalPresented<DestinationType: UIViewController, O: ObservableType where DestinationType: protocol<HasInput, StoryboardLoad>>
        (destinationType: DestinationType.Type) -> (source: O) -> (configureDestination: (DestinationType.InputType, O.E) -> Void) -> Disposable {
        return { source in
            return { configureDestination in
                return source.subscribeNext { element in
                    self.presentModal(destinationType) { input in
                        configureDestination(input, element)
                    }
                }
            }
        }
    }
    
    func push<DestinationType: UIViewController where DestinationType: StoryboardLoad>
        (destinationType: DestinationType.Type) {
        guard let navigation = sourceViewController?.navigationController else {
            assertionFailure("Navigation controller not available.")
            return
        }
        let destination = DestinationType.loadFromStoryboard()
        navigation.pushViewController(destination, animated: true)
    }
    
    func push<DestinationType: UIViewController where DestinationType: protocol<HasInput, StoryboardLoad>>
        (destinationType: DestinationType.Type, destinationInputHandler: (DestinationType.InputType) -> Void) {
        guard let navigation = sourceViewController?.navigationController else {
            assertionFailure("Navigation controller not available.")
            return
        }
        let destination = DestinationType.loadFromStoryboard()
        navigation.pushViewController(destination, animated: true)
        destinationInputHandler(destination.input)
    }
    
    func pushed<DestinationType: UIViewController, O: ObservableType where DestinationType: protocol<HasInput, StoryboardLoad>>
        (destinationType: DestinationType.Type) -> (source: O) -> (configureDestination: (DestinationType.InputType, O.E) -> Void) -> Disposable {
        return { source in
            return { configureDestination in
                return source.subscribeNext { element in
                    self.push(destinationType) { input in
                        configureDestination(input, element)
                    }
                }
            }
        }
    }

    func pop<DestinationType: UIViewController>(destinationType: DestinationType.Type) {
        guard let navigation = sourceViewController?.navigationController else {
            assertionFailure("Navigation controller not available.")
            return
        }
        let viewController = navigation.childViewControllers.filter { $0 is DestinationType }.last
        guard let destination = viewController as? DestinationType else {
            assertionFailure("Destination view controller type mismatch.")
            return
        }
        navigation.popToViewController(destination, animated: true)
    }

    func pop<DestinationType: UIViewController where DestinationType: HasInput>
        (destinationType: DestinationType.Type, destinationInputHandler: (DestinationType.InputType) -> Void) {
        guard let navigation = sourceViewController?.navigationController else {
            assertionFailure("Navigation controller not available.")
            return
        }
        let viewController = navigation.childViewControllers.filter { $0 is DestinationType }.last
        guard let destination = viewController as? DestinationType else {
            assertionFailure("Destination view controller type mismatch.")
            return
        }
        navigation.popToViewController(destination, animated: true)
        destinationInputHandler(destination.input)
    }

    func popped<DestinationType: UIViewController, O: ObservableType where DestinationType: HasInput>
        (destinationType: DestinationType.Type) -> (source: O) -> (configureDestination: (DestinationType.InputType, O.E) -> Void) -> Disposable {
        return { source in
            return { configureDestination in
                return source.subscribeNext { element in
                    self.pop(destinationType) { input in
                        configureDestination(input, element)
                    }
                }
            }
        }
    }

    func replaceRoot<DestinationType: UIViewController where DestinationType: StoryboardLoad>
        (destinationType: DestinationType.Type) {
        guard let navigation = sourceViewController?.navigationController else {
            assertionFailure("Navigation controller not available.")
            return
        }
        navigation.setViewControllers([DestinationType.loadFromStoryboard()], animated: true)
    }

    func goToWebsite(url: NSURL) {
        UIApplication.sharedApplication().openURL(url)
    }

}

private extension UIStoryboardSegue {

    func destination<DestinationType: UIViewController>() -> DestinationType? {
        switch destinationViewController {
        case let destination as DestinationType:
            return destination
        case let navigation as UINavigationController:
            return navigation.childViewController()
        default:
            return nil
        }
    }

}

private extension UIViewController {

    func childViewController<DestinationType: UIViewController>() -> DestinationType? {
        return childViewControllers.filter { $0 is DestinationType }.first as? DestinationType
    }

}
