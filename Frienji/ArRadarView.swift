//
//  ArRadarView.swift
//  Traces
//
//  Created by Adam Szeremeta on 12.10.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import RxSwift

class ArRadarView: UIView {

    private let kBackgroumdImage = UIImage(named: "trace_radar_bg")!

    private var cameraViewAngle:Double = 0
    private var bearing:Double = 0

    private (set) var frenjisView:ArRadarFrenjisView!

    // MARK: Init

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.clearColor()
        self.clipsToBounds = true

        self.frenjisView = ArRadarFrenjisView()
        self.frenjisView.backgroundColor = UIColor.clearColor()
        self.frenjisView.clipsToBounds = true
        self.addSubviewFullscreen(self.frenjisView)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.layer.cornerRadius = self.frame.size.height / 2
    }

    // MARK: Traces

    func updateWithFrenjis(frenjis:[Frienji]) {
        self.frenjisView.updateWithFrienjis(frenjis)
    }

    // MARK: Sensors readings

    func setCameraAngle(angle:CGFloat) {
        self.cameraViewAngle = Double(angle)
    }

    func setBearing(bearing:Double) {
        self.bearing = bearing

        let angleToRotate = -(self.bearing) * M_PI / 180.0
        self.frenjisView.transform = CGAffineTransformMakeRotation(CGFloat(angleToRotate))
    }

    func setUserLocation(location:CLLocation) {
        self.frenjisView.location = location

        self.setNeedsDisplay()
    }

    // MARK: Draw

    override func drawRect(rect: CGRect) {

        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }

        //draw background
        self.kBackgroumdImage.drawInRect(rect)

        //draw angle
        CGContextSaveGState(context)
        self.drawAngleGradient(rect, context: context)
        CGContextRestoreGState(context)
    }

    private func drawAngleGradient(rect: CGRect, context:CGContext) {
        let topPointingAngle = 270.0
        let halfCameraFieldOfView = self.cameraViewAngle/2

        let center = CGPointMake(rect.size.width / 2, rect.size.height / 2)
        let radius: CGFloat = self.bounds.size.height / 2
        let startAngle = CGFloat((topPointingAngle - halfCameraFieldOfView) * M_PI / 180.0)
        let endAngle = CGFloat((topPointingAngle + halfCameraFieldOfView) * M_PI / 180.0)

        let startPoint = CGPointMake(center.x + radius * cos(startAngle), center.y + radius * sin(startAngle))

        CGContextBeginPath(context)
        CGContextMoveToPoint(context, center.x, center.y)
        CGContextAddLineToPoint(context, startPoint.x, startPoint.y)
        CGContextAddArc(context, center.x, center.y, radius, startAngle, endAngle, startAngle > endAngle ? 1 : 0)
        CGContextClosePath(context)
        CGContextClip(context)

        //create gradient
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let componentsCount: Int = 2
        let colorComponents: [CGFloat] = [
            184.0/255, 24.0/255, 151.0/255, 0.5,
            184.0/255, 24.0/255, 151.0/255, 0.1,
            ]
        let locations: [CGFloat] = [0, 1.0]

        if let gradient = CGGradientCreateWithColorComponents(colorSpace, colorComponents, locations, componentsCount) {
            //draw gradient
            CGContextDrawLinearGradient(context, gradient, CGPointMake(center.x, center.y), CGPointMake(center.x, 0), CGGradientDrawingOptions())
        }
    }
    
}

class ArRadarFrenjisView: UIView {
    
    private let kUserImage = UIImage(named: "trace_radar_user")!
    private let kFrenjiImage = UIImage(named: "trace_radar_dot")!
    
    var location:CLLocation = CLLocation(latitude: 0, longitude: 0)
    
    private var currentFrenjis = Set<Frienji>()
    private var frienjisToAdd = Set<Frienji>()
    private var frienjisToRemove = Set<Frienji>()
    
    private var inAnimationDistanceOffset: Double = 0
    private var outAnimationDistanceOffset: Double = 0
    private var currentAnimationDelta: Double = 0
    
    let maxObjectDistance = Variable<CGFloat>(ArSliderView.kMinSliderDistanceValue)
    private let disposeBag = DisposeBag()
    private var animationDisposeBag = DisposeBag()
    
    // MARK: Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setUpObservables()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Observables
    
    private func setUpObservables() {
        self.maxObjectDistance.asObservable().skip(1).bindNext { [unowned self] _ in
            self.setNeedsDisplay()
        }.addDisposableTo(self.disposeBag)
    }
    
    // MARK: Data
    
    func updateWithFrienjis(frienjis:[Frienji]) {
        //check which frenjis are already present on radar, which should be animated in and which should be animated out
        let update = Set(frienjis)
        
        //frienjis which are present now and should stay
        let normalFrienjis = self.currentFrenjis.intersect(update)
        
        //frienjis to animate in or out
        let frienjisToAnimate = update.subtract(normalFrienjis)
        let frienjisToAnimateIn = frienjisToAnimate.subtract(self.currentFrenjis)
        let frienjisToAnimateOut = frienjisToAnimate.subtract(frienjisToAnimateIn)
        
        self.currentFrenjis = normalFrienjis
        self.frienjisToAdd = frienjisToAnimateIn
        self.frienjisToRemove = frienjisToAnimateOut
        
        //start animation if not playing
        if self.currentAnimationDelta == 0 {
            self.animateFrienjisOffset()
            
        } else {
            //start with delay as we don't want some frienjis to jump when animation is playing
            performWithDelay(0.66, closure: { [weak self] in
                self?.updateWithFrienjis(frienjis)
            })
        }
        
        self.setNeedsDisplay()
    }
    
    // MARK: Animation
    
    private func animateFrienjisOffset() {
        self.animationDisposeBag = DisposeBag()
        self.currentAnimationDelta = 0
        
        let animationDelta = 0.05
        
        Observable<Int>.interval(0.03, scheduler: MainScheduler.instance).bindNext({ [weak self] _ in
            
            guard let strongSelf = self else {
                return
            }
            
            self?.currentAnimationDelta += animationDelta
            
            if strongSelf.currentAnimationDelta < 1 {
                strongSelf.inAnimationDistanceOffset = Double(strongSelf.maxObjectDistance.value) - strongSelf.currentAnimationDelta * Double(strongSelf.maxObjectDistance.value)
                strongSelf.outAnimationDistanceOffset = strongSelf.currentAnimationDelta * Double(strongSelf.maxObjectDistance.value)
                
                self?.setNeedsDisplay()
                
            } else {
                self?.currentFrenjis.unionInPlace(strongSelf.frienjisToAdd)
                self?.frienjisToAdd.removeAll()
                self?.frienjisToRemove.removeAll()
                
                self?.inAnimationDistanceOffset = 0
                self?.outAnimationDistanceOffset = 0
                self?.currentAnimationDelta = 0
                
                self?.setNeedsDisplay()

                self?.animationDisposeBag = DisposeBag()                
            }
        }).addDisposableTo(self.animationDisposeBag)
    }
    
    // MARK: Draw
    
    override func drawRect(rect: CGRect) {
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        //draw frienjis
        for frenji in self.currentFrenjis {
            CGContextSaveGState(context)
            self.drawFrenji(frenji, location: self.location, context: context)
            CGContextRestoreGState(context)
        }
        
        //draw frienjis animated in
        for frenji in self.frienjisToAdd {
            CGContextSaveGState(context)
            self.drawFrenji(frenji, location: self.location, withDistanceOffset: self.inAnimationDistanceOffset, context: context)
            CGContextRestoreGState(context)
        }
        
        //draw frienjis animated out
        for frenji in self.frienjisToRemove {
            CGContextSaveGState(context)
            self.drawFrenji(frenji, location: self.location, withDistanceOffset: self.outAnimationDistanceOffset, context: context)
            CGContextRestoreGState(context)
        }
        
        //draw user position
        self.drawUserPosition(rect)
    }
    
    private func drawUserPosition(rect: CGRect) {
        //draw user position
        let center = CGPointMake(rect.size.width / 2, rect.size.height / 2)
        let userImageSize = rect.size.width / 5
        let userImageRect = CGRectMake(center.x - userImageSize / 2, center.y - userImageSize / 2, userImageSize, userImageSize)
        self.kUserImage.drawInRect(userImageRect)
    }
    
    private func drawFrenji(frenji:Frienji, location:CLLocation, withDistanceOffset distanceOffset: Double = 0, context:CGContext) {
        //get distance to trace and bearing
        let frenjiDistance = location.distanceFromLocation(frenji.location) + distanceOffset
        let frenjiBearing = LocationManager.sharedInstance.getBearingBetweenTwoPoints(location, point2: frenji.location)
        
        let viewRadius = self.bounds.size.height/2
        let userImageSize = self.bounds.size.width / 5
        
        //calculate position and angle to rotate
        let distanceFromCenter = (viewRadius * CGFloat(frenjiDistance)) / self.maxObjectDistance.value
        let yPosition = viewRadius - userImageSize/2 - distanceFromCenter
        let angleToRotate = frenjiBearing * CGFloat(M_PI) / 180.0
        
        CGContextTranslateCTM(context, viewRadius, viewRadius)
        CGContextRotateCTM(context, angleToRotate)
        CGContextTranslateCTM(context, -viewRadius, -viewRadius)
        
        let center = CGPointMake(viewRadius, yPosition)
        let frenjiImageSize = self.bounds.size.width / 6
        let frenjiImageRect = CGRectMake(center.x - frenjiImageSize / 2, center.y - frenjiImageSize / 2, frenjiImageSize, frenjiImageSize)
        self.kFrenjiImage.drawInRect(frenjiImageRect)
    }
}

