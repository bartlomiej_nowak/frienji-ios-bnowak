//
//  String+DataEncoding.swift
//  Frienji
//
//  Created by Piotr Łyczba on 21/10/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

extension String {

    func dataWithUTF8Encoding() -> NSData? {
        return dataUsingEncoding(NSUTF8StringEncoding)
    }

}
