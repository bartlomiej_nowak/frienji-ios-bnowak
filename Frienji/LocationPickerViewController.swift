//
//  LocationPickerViewController.swift
//  Frienji
//
//  Created by adam kolodziej on 05.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import MapKit
import RxSwift

protocol LocationPickerDelegate: class {
    func locationPicker(picker: LocationPickerViewController, didFinishPickingLocation location: CLLocationCoordinate2D, withMessage message:String)
}

@IBDesignable class LocationPickerViewController: BaseViewController, StoryboardLoad {

    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var textInputBottomConstraint: NSLayoutConstraint!
    @IBOutlet private weak var textInputView: TextInputView!
    
    static var storyboardId: String = "Main"
    static var storyboardControllerId = "LocationPickerViewController"
    
    private let location = Variable<CLLocationCoordinate2D?>(nil)
    private let disposeBag = DisposeBag()
    private let coordinateSpanDelta = 0.1
    
    weak var delegate: LocationPickerDelegate?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textInputView.delegate = self
        
        setUpObservables()
        
        addPinForUserLocation()
    }
    
    // MARK: - Actions
    
    @IBAction private func mapLongPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == .Began {
            let point = sender.locationInView(mapView)
            let coordinate = mapView.convertPoint(point, toCoordinateFromView: mapView)
            addPin(forCoordinate: coordinate)
            
            var region = mapView.region
            region.center = coordinate
            mapView.setRegion(region, animated: true)
        }
    }
    
    // MARK: - Helpers
    
    private func setUpObservables() {
        location.asObservable().bindNext { [unowned self]  location in
                self.textInputView.sendEnabled = location != nil
            }.addDisposableTo(self.disposeBag)
    }
    
    private func addPinForUserLocation() {
        guard let coordinate = LocationManager.sharedInstance.currentLocation.value?.coordinate else {
            assert(false, "Location manager didn't provide user location")
            return
        }
        
        addPin(forCoordinate: coordinate)
        
        let span = MKCoordinateSpan(latitudeDelta: coordinateSpanDelta, longitudeDelta: coordinateSpanDelta)
        let region = mapView.regionThatFits(MKCoordinateRegion(center: coordinate, span: span))
        mapView.setRegion(region, animated: true)
    }
    
    private func addPin(forCoordinate coordinate: CLLocationCoordinate2D) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        mapView.removeAnnotations(mapView.annotations)
        mapView.addAnnotation(annotation)
        
        location.value = coordinate
    }
    
    // MARK: - Keyboard Handling
    
    override func keyboardWillChangeSize(newSize size: CGSize) {
        self.textInputBottomConstraint.constant = size.height
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func keyboardWillHide() {
        self.textInputBottomConstraint.constant = 0
        UIView.animateWithDuration(kDefaultAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
}

extension LocationPickerViewController: TextInputProtocol {
    func textInput(textInput: TextInputView, sendPressedWithMessage message: String) {
        guard let location = location.value else {
            assert(false, "Button should be disabled if no location")
            return
        }
        self.delegate?.locationPicker(self, didFinishPickingLocation: location, withMessage: message)
    }
}
