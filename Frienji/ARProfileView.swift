//
//  ARProfileView.swift
//  Frienji
//
//  Created by adam kolodziej on 15.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

@IBDesignable class ARProfileView: ProfileBaseView {

    // MARK: - Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadViewFromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadViewFromNib()
    }
    
    private func loadViewFromNib() {
        let view = self.instantiateFromNib()
        self.addSubviewFullscreen(view)
    }
}
