//
//  WallProfileViewModel.swift
//  Frienji
//
//  Created by Piotr Łyczba on 28/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

struct ProfileViewModel {

    let avatar: Avatar?
    let description: String
    let coverUrl: String
    let name: String

    init(avatar: Avatar? = nil, description: String = "", coverUrl: String = "", name: String = "") {
        self.avatar = avatar
        self.description = description
        self.coverUrl = coverUrl
        self.name = name
    }

    func executeCommand(command: ProfileCommand) -> ProfileViewModel {
        switch command {
        case let .LoadProfile(frienji):
            return ProfileViewModel(avatar: frienji.avatar, description: frienji.whoAreYou ?? "", coverUrl: frienji.backgroundImageUrl ?? "", name: frienji.username)
        }
    }

}

enum ProfileCommand {
    case LoadProfile(frienji: Frienji)
}
