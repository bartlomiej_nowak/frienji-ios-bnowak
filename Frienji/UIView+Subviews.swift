//
//  UIView+Subviews.swift
//  Traces
//
//  Created by Adam Szeremeta on 21.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIView {

    // MARK: Subview fullscreen

    func addSubviewFullscreen(subview: UIView) {

        self.addSubview(subview)

        addAutolayoutForSubviewFullscreen(subview)
    }

    func addSubviewFullscreen(subview: UIView, belowSubview:UIView) {

        self.insertSubview(subview, belowSubview: belowSubview)

        addAutolayoutForSubviewFullscreen(subview)
    }

    func addSubviewFullscreen(subview: UIView, atIndex:Int) {

        self.insertSubview(subview, atIndex: atIndex)

        addAutolayoutForSubviewFullscreen(subview)
    }

    private func addAutolayoutForSubviewFullscreen(subview:UIView) {

        subview.translatesAutoresizingMaskIntoConstraints = false

        let views = ["subview": subview]

        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|-(0)-[subview]-(0)-|", options: NSLayoutFormatOptions(), metrics: nil, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(0)-[subview]-(0)-|", options: NSLayoutFormatOptions(), metrics: nil, views: views))
    }

    // MARK: Subview centered

    func addSubviewCentered(subview: UIView, withSize size:CGSize) {

        subview.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(subview)

        let widthConstraint = NSLayoutConstraint(item: subview, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: size.width)
        let heightConstraint = NSLayoutConstraint(item: subview, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1.0, constant: size.height)
        let xConstraint = NSLayoutConstraint(item: subview, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: subview, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)

        self.addConstraints([widthConstraint, heightConstraint, xConstraint, yConstraint])
    }

    // MARK: Subview bottom right

    func addSubviewBottomRight(subview:UIView, withSize size:CGSize, padding:CGFloat) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(subview)

        let metrics = ["width": size.width, "height": size.height, "padding": padding]
        let views = ["subview": subview]

        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[subview(height)]-(padding)-|", options: NSLayoutFormatOptions(), metrics: metrics, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:[subview(width)]-(padding)-|", options: NSLayoutFormatOptions(), metrics: metrics, views: views))
    }
    
    // MARK: Subview bottom left
    
    func addSubviewBottomLeft(subview:UIView, withSize size:CGSize, padding:CGFloat) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(subview)
        
        let metrics = ["width": size.width, "height": size.height, "padding": padding]
        let views = ["subview": subview]
        
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:[subview(height)]-(padding)-|", options: NSLayoutFormatOptions(), metrics: metrics, views: views))
        self.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|-(padding)-[subview(width)]", options: NSLayoutFormatOptions(), metrics: metrics, views: views))
    }
    
}
