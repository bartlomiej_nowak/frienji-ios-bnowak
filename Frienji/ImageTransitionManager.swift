//
//  ImageTransitionManager.swift
//  Frienji
//
//  Created by Adam Szeremeta on 30.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

class ImageTransitionManager: UIPercentDrivenInteractiveTransition {
    
    var interactive = false
    var startingFrame = CGRectZero
    private var presenting = true
    
    weak var presentingController: UIViewController?
    weak var modalController: UIViewController?

    deinit {
        self.cancelInteractiveTransition()
    }
    
    // MARK: Gesture recognizer
    
    func interactiveGestureHandler(recognizer:UIPanGestureRecognizer) {
        
        guard let view = recognizer.view else {
            return
        }
        
        self.interactive = true
        
        let translation = recognizer.translationInView(view)
        let progress = max(0, min(1, abs(translation.y) / view.frame.size.height / 2))
        
        switch (recognizer.state) {
            
        case .Began:
            self.modalController?.dismissViewControllerAnimated(true, completion: nil)
            
        case .Changed:
            self.updateInteractiveTransition(progress)
            
        case .Ended:
            progress > 0.5 ? self.finishInteractiveTransition() : self.cancelInteractiveTransition()
            
        default:
            break
        }
    }
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return kDefaultAnimationDuration
    }
}

extension ImageTransitionManager: UIViewControllerAnimatedTransitioning {
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        if self.presenting {
            self.animateTransitionForPresentedController(transitionContext)
            
        } else {
            self.animateTransitionForDismissedController(transitionContext)
        }
    }
    
    private func animateTransitionForPresentedController(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey),
            let toView = toViewController.view
            else {
                return
        }
        
        let containerView = transitionContext.containerView()
        
        let toViewStartFrame = self.startingFrame
        let toViewFinalFrame = transitionContext.finalFrameForViewController(toViewController)
        
        //scale animation
        let xScaleFactor = toViewStartFrame.width / toViewFinalFrame.width
        let yScaleFactor = toViewStartFrame.height / toViewFinalFrame.height
        let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)
        
        //set transform for presented view
        toView.transform = scaleTransform
        toView.center = CGPointMake(CGRectGetMidX(toViewStartFrame), toViewStartFrame.origin.y + toViewStartFrame.size.height / 2)
        toView.clipsToBounds = true
        toView.alpha = 0
        
        containerView.addSubview(toView)
        
        //animation
        UIView.animateWithDuration(self.transitionDuration(transitionContext), animations: { () -> Void in
            toView.transform = CGAffineTransformIdentity
            toView.center = CGPointMake(toViewFinalFrame.size.width / 2, toViewFinalFrame.size.height / 2)
            toView.alpha = 1
            
        }) { (result:Bool) -> Void in
            let success = !transitionContext.transitionWasCancelled()
            
            if !success {
                toView.removeFromSuperview()
            }
            
            transitionContext.completeTransition(success)
        }
    }
    
    private func animateTransitionForDismissedController(transitionContext: UIViewControllerContextTransitioning) {
        
        guard
            let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey),
            let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey),
            let fromView = fromViewController.view
            else {
                return
        }
        
        let containerView = transitionContext.containerView()
        
        var toViewStartFrame = self.startingFrame
        var toViewFinalFrame = transitionContext.finalFrameForViewController(toViewController)
        
        //change frame for dismissed controller
        let temp = toViewStartFrame
        toViewStartFrame = toViewFinalFrame
        toViewFinalFrame = temp

        let xScaleFactor = toViewFinalFrame.width / toViewStartFrame.width
        let yScaleFactor = toViewFinalFrame.height / toViewStartFrame.height
        let scaleTransform = CGAffineTransformMakeScale(xScaleFactor, yScaleFactor)
        
        fromView.clipsToBounds = true
        fromView.alpha = 1
        
        containerView.addSubview(fromView)
        
        //animation
        UIView.animateWithDuration(self.transitionDuration(transitionContext), animations: { () -> Void in
            let xCenter = toViewFinalFrame.origin.x + toViewFinalFrame.size.width / 2
            let yCenter = toViewFinalFrame.origin.y + toViewFinalFrame.size.height / 2
            
            fromView.transform = scaleTransform
            fromView.center = CGPointMake(xCenter, yCenter)
            fromView.alpha = 0
            
        }) { (result:Bool) -> Void in
            let success = !transitionContext.transitionWasCancelled()
            
            if success {
                fromView.removeFromSuperview()
            }
            
            transitionContext.completeTransition(success)
        }
    }
}

extension ImageTransitionManager: UIViewControllerTransitioningDelegate {
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = true
        
        return self
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false
        
        return self
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
    func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return nil
    }
}
