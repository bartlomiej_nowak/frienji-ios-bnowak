//
//  ActivityIndicator.swift
//  RxExample
//
//  Created by Krunoslav Zaher on 10/18/15.
//  Copyright © 2015 Krunoslav Zaher. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

private struct ActivityToken<E>: ObservableConvertibleType, Disposable {
    private let source: Observable<E>
    private let disposable: AnonymousDisposable

    init(source: Observable<E>, disposeAction: () -> ()) {
        self.source = source
        disposable = AnonymousDisposable(disposeAction)
    }

    func dispose() {
        disposable.dispose()
    }

    func asObservable() -> Observable<E> {
        return source
    }

}

/**
Enables monitoring of sequence computation.

If there is at least one sequence computation in progress, `true` will be sent.
When all activities complete `false` will be sent.
*/
public class ActivityIndicator: DriverConvertibleType {
    public typealias E = Bool

    private let lock = NSRecursiveLock()
    private let variable = Variable(0)
    private let loading: Driver<Bool>

    public init() {
        loading = variable.asObservable()
            .map { $0 > 0 }
            .distinctUntilChanged()
            .asDriver(onErrorRecover: ActivityIndicator.ifItStillErrors)
    }

    private static func ifItStillErrors(error: ErrorType) -> Driver<Bool> {
        _ = fatalError("Loader can't fail")
    }


    private func trackActivityOfObservable<O: ObservableConvertibleType>(source: O) -> Observable<O.E> {
        return Observable.using({ () -> ActivityToken<O.E> in
            self.increment()
            return ActivityToken(source: source.asObservable(), disposeAction: self.decrement)
        }) { t in
            return t.asObservable()
        }
    }

    private func increment() {
        lock.lock()
        variable.value = variable.value + 1
        lock.unlock()
    }

    private func decrement() {
        lock.lock()
        variable.value = variable.value - 1
        lock.unlock()
    }

    public func asDriver() -> Driver<E> {
        return loading
    }

}

public extension ObservableConvertibleType {

    public func trackActivity(activityIndicator: ActivityIndicator) -> Observable<E> {
        return activityIndicator.trackActivityOfObservable(self)
    }

}
