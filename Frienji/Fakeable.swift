//
//  Fakeable.swift
//  Frienji
//
//  Created by Piotr Łyczba on 29/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Fakery

private let sharedFaker = Faker()

protocol Fakeable {

    associatedtype FakedType

    static func fake() -> FakedType

}

extension Fakeable {

    static var faker: Faker {
        return sharedFaker
    }

}
