//
//  NSDate+TimeAgo.swift
//  Frienji
//
//  Created by adam kolodziej on 21.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

extension NSDate {
    var timeAgo: String {
        let calendar = NSCalendar.currentCalendar()
        let now = NSDate()
        let earliest = now.earlierDate(self)
        let latest = (earliest == now) ? self : now
        let components:NSDateComponents = calendar.components([NSCalendarUnit.Minute , NSCalendarUnit.Hour , NSCalendarUnit.Day , NSCalendarUnit.WeekOfYear , NSCalendarUnit.Month , NSCalendarUnit.Year , NSCalendarUnit.Second], fromDate: earliest, toDate: latest, options: NSCalendarOptions())
        if components.year >= 2 {
            return Localizations.timeAgo.years(components.year)
        } else if components.year >= 1 {
            return Localizations.timeAgo.year
        } else if components.month >= 2 {
            return Localizations.timeAgo.months(components.month)
        } else if components.month >= 1 {
            return Localizations.timeAgo.month
        } else if components.weekOfYear >= 2 {
            return Localizations.timeAgo.weeks(components.weekOfYear)
        } else if components.weekOfYear >= 1 {
            return Localizations.timeAgo.week
        } else if components.day >= 2 {
            return Localizations.timeAgo.days(components.day)
        } else if components.day >= 1 {
            return Localizations.timeAgo.day
        } else if components.hour >= 2 {
            return Localizations.timeAgo.hours(components.hour)
        } else if components.hour >= 1 {
            return Localizations.timeAgo.hour
        } else if components.minute >= 2 {
            return Localizations.timeAgo.minutes(components.minute)
        } else if components.minute >= 1 {
            return Localizations.timeAgo.minute
        } else if components.second >= 3 {
            return Localizations.timeAgo.seconds(components.second)
        } else {
            return Localizations.timeAgo.now
        }
    }
}
