//
//  UnblockHintViewController.swift
//  Frienji
//
//  Created by adam kolodziej on 20.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

protocol UnblockHintViewControllerDelegate: class {
    
    func unblockHintViewController(unblockHintViewController: UnblockHintViewController, tappedWithSender sender: AnyObject)
}

class UnblockHintViewController: UIViewController {

    weak var delegate: UnblockHintViewControllerDelegate?
    
    // MARK: - Actions
    @IBAction func tapAction(sender: AnyObject) {
        self.delegate?.unblockHintViewController(self, tappedWithSender: sender)
    }
    
    @IBAction func tryNowButtonPressed(sender: AnyObject) {
        
        guard let navigationController = UIApplication.sharedApplication().keyWindow?.rootViewController as? UINavigationController else {
            return
        }
        
        navigationController.dismissViewControllerAnimated(false, completion: nil)
        navigationController.popToRootViewControllerAnimated(false)
        
        let zooController = ZooViewController.loadFromStoryboard()
        navigationController.pushViewController(zooController, animated: true)
    }

}
