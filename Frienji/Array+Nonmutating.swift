//
//  Array+Nonmutating.swift
//  Frienji
//
//  Created by Piotr Łyczba on 06/09/16.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

extension Array {

    func arrayByInserting(element: Element, atIndex index: Int) -> [Element] {
        var result = self
        result.insert(element, atIndex: index)
        
        return result
    }

    func arrayByAppending(element: Element) -> [Element] {
        var result = self
        result.append(element)

        return result
    }
    
    func arrayByRemovingAtIndex(index: Int) -> [Element] {
        var result = self
        result.removeAtIndex(index)
        
        return result
    }
}
