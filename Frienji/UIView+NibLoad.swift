//
//  UIView+NibLoad.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import UIKit

protocol NibLoad {
    
    static var nibId: String {get}
    
}

extension NibLoad where Self: UIView {
    
    static var nibId: String {
        
        return String(Self)
    }
    
    static func loadFromNib() -> Self {
        
        return NSBundle.mainBundle().loadNibNamed(Self.nibId, owner: self, options: nil)!.first as! Self
    }
    
}
