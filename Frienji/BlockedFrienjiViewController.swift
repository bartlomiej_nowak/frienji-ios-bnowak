//
//  BlockedFrienjiViewController.swift
//  Frienji
//
//  Created by adam kolodziej on 19.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import RxSwift
import SDWebImage

class BlockedFrienjiViewController: ZooViewController {
    
    override var emptyLabelText: String {
        return Localizations.blockedFrienji.noone_blocked
    }
    
    override var seeBlockedFrienjiButton: UIBarButtonItem? {
        return nil
    }
    
    // MARK: - Configure view
    
    override func configureNavigationBar() {
        navigationItem.leftBarButtonItem = backButton
        navigationItem.title = editing ? Localizations.blockedFrienji.viewEditingTitle : Localizations.blockedFrienji.viewTitle
        
        navigationItem.rightBarButtonItem = nil
    }
    
    override func prepareLoad() -> Observable<ZooCommand> {
        let loadMore = collectionView?.loadMoreTrigger.filter { [unowned self] in !self.editing } ?? Observable.empty()
        return FrienjiApi.sharedInstance.getBlockedFrienjis(loadMore).showAlertOnApiError(self)
            .map(ZooCommand.LoadFrienjis)
    }
    
    override func bindEditing() {
        editingCommited
            .withLatestFrom(viewModel) { _, viewModel in
                viewModel.deletedFrienjis
            }
            .flatMap { frienjis in
                FrienjiApi.sharedInstance.unblockFrienjis(frienjis)
            }
            .subscribe()
            .addDisposableTo(disposeBag)
    }
    
    override func headerTitle() -> String {
        return self.editing ? Localizations.blockedFrienji.header.editingTitle : Localizations.blockedFrienji.header.title
    }
}
