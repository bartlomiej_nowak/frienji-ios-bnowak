//
//  Like.swift
//  Frienji
//
//  Created by adam kolodziej on 23.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct Like {
    
    let liked: Bool
    let postId: Int
    let postMessage: String?
    let parentPostId: Int?
    let wallOwnerId: Int
    let frienjiId: Int
    let createdAt: NSDate
    let updatedAt: NSDate
}

// MARK: - Fake

extension Like: Fakeable {
    
    static func fake() -> Like {
        return Like(
            liked: faker.number.randomBool(),
            postId: faker.number.increasingUniqueId(),
            postMessage: "message",
            parentPostId: faker.number.increasingUniqueId(),
            wallOwnerId: faker.number.randomInt(min: 0, max: 5),
            frienjiId: faker.number.randomInt(min: 0, max: 5),
            createdAt: (NSDate.randomWithinDaysBeforeToday(30)),
            updatedAt: (NSDate.randomWithinDaysBeforeToday(30))
        )
    }
    
}

// MARK: - Decode

extension Like: Decodable {
    
    static func decode(json: JSON) -> Decoded<Like> {
        return curry(Like.init)
            <^> json <| "liked" <|> pure(false)
            <*> json <| "post_id"
            <*> json <|? "post_message"
            <*> json <|? "post_parent_id"
            <*> json <| "wall_owner_id"
            <*> json <| "frienji_id"
            <*> (json <| "created_at" >>- toNSDate)
            <*> (json <| "updated_at" >>- toNSDate)
    }
    
}
