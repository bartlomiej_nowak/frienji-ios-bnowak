//
//  ReachabilityService.swift
//  RxExample
//
//  Created by Vodovozov Gleb on 10/22/15.
//  Copyright © 2015 Krunoslav Zaher. All rights reserved.
//

import RxSwift
import Foundation
import ReachabilitySwift

public enum ReachabilityStatus {
    case Reachable(viaWiFi: Bool)
    case Unreachable
}

extension ReachabilityStatus {
    var reachable: Bool {
        switch self {
        case .Reachable:
            return true
        case .Unreachable:
            return false
        }
    }
}

protocol ReachabilityService {
    var reachability: Observable<ReachabilityStatus> { get }
}

class DefaultReachabilityService: ReachabilityService {

    private let reachabilitySubject: BehaviorSubject<ReachabilityStatus>

    var reachability: Observable<ReachabilityStatus> {
        return reachabilitySubject.asObservable()
    }

    private let reachabilityRef: Reachability

    init() throws {
        let reachabilityRef = try Reachability.reachabilityForInternetConnection()
        let reachabilitySubject = BehaviorSubject<ReachabilityStatus>(value: .Unreachable)

        // so main thread isn't blocked when reachability via WiFi is checked
        let backgroundQueue = dispatch_queue_create("reachability.wificheck", DISPATCH_QUEUE_SERIAL)

        reachabilityRef.whenReachable = { reachability in
            dispatch_async(backgroundQueue) {
                reachabilitySubject.on(.Next(.Reachable(viaWiFi: reachabilityRef.isReachableViaWiFi())))
            }
        }

        reachabilityRef.whenUnreachable = { reachability in
            dispatch_async(backgroundQueue) {
                reachabilitySubject.on(.Next(.Unreachable))
            }
        }

        try reachabilityRef.startNotifier()
        self.reachabilityRef = reachabilityRef
        self.reachabilitySubject = reachabilitySubject
    }

    deinit {
        reachabilityRef.stopNotifier()
    }

}

extension ObservableConvertibleType {

    func retryOnBecomesReachable(reachabilityService: ReachabilityService?) -> Observable<E> {
        guard let reachabilityService = reachabilityService else {
            return self.asObservable()
        }
        return self.asObservable().retryWhen { attempts in
            return attempts.flatMap { (error: NSError) -> Observable<ReachabilityStatus> in
                guard error.domain == NSURLErrorDomain && (error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorNetworkConnectionLost) else {
                    return Observable.error(error)
                }

                return reachabilityService.reachability.filter { $0.reachable }
            }
        }
    }

}
