//
//  Vertex.swift
//  Traces
//
//  Created by Adam Szeremeta on 13.07.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation

struct Vertex {
    
    var position: (CFloat, CFloat, CFloat)
    var textureCoordinate: (CFloat, CFloat)
}