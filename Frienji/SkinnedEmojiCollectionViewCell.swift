//
//  SkinnedEmojiCollectionViewCell.swift
//  Frienji
//
//  Created by adam kolodziej on 21.12.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit

class SkinnedEmojiCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "SkinnedEmojiCollectionViewCellIdentifier"
    
    @IBOutlet weak var emojiLabel: UILabel!
}
