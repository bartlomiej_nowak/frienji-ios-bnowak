//
//  GifLoader.swift
//  Frienji
//
//  Created by Adam Szeremeta on 17.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import FLAnimatedImage

class GifLoader {
    
    class func loadAnimatedImageNamed(fileName:String) -> FLAnimatedImage? {
        let imageName: NSString = fileName as NSString

        if let resourcePath = NSBundle.mainBundle().pathForResource(imageName.stringByDeletingPathExtension, ofType: imageName.pathExtension), let imageData = NSData(contentsOfFile: resourcePath) {
            return FLAnimatedImage(animatedGIFData: imageData)
        }

        return nil
    }
}
