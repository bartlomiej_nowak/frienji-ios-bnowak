//
//  AvatarSelectionCollectionViewCell.swift
//  Frienji
//
//  Created by bolek on 02.08.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import UIKit
import FLAnimatedImage

class AvatarSelectionCollectionViewCell: UICollectionViewCell, CellReuseIdentifier {
    
    //outlets
    @IBOutlet weak var avatarContainer: UIView!
    @IBOutlet weak var avatarImage: FLAnimatedImageView!
    @IBOutlet weak var avatarLabel: UILabel!
    
    //properties
    private (set) var avatarName: String!

    // MARK: - View lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.contentView.layoutIfNeeded()
        self.avatarContainer.layer.cornerRadius = self.avatarContainer.frame.size.width / 2
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.avatarLabel.text = nil
        self.avatarImage.image = nil
        self.avatarImage.animatedImage = nil
    }
    
    // MARK: - Public methods
    
    func configureCellFor(avatarName:String, entryType: AvatarType) {
        self.avatarName = avatarName
        
        switch entryType {
            
        case AvatarType.Emoji:
            self.avatarLabel.text = self.avatarName
            
        case AvatarType.Image:
            self.avatarImage.contentMode = UIViewContentMode.ScaleAspectFit
            self.avatarImage.image = UIImage(imageLiteral: self.avatarName)
           
        case AvatarType.AnimatedImage:
            self.avatarImage.contentMode = UIViewContentMode.ScaleAspectFill
            self.avatarImage.animatedImage = GifLoader.loadAnimatedImageNamed(avatarName)
        }
    }
}
