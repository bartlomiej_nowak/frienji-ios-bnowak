//
//  AvatarTrail.swift
//  Frienji
//
//  Created by adam kolodziej on 25.11.2016.
//  Copyright © 2016 Ripple Inc. All rights reserved.
//

import Foundation
import Argo
import Curry

struct AvatarTrail {
    let oldAvatar: Avatar
    let newAvatar: Avatar
}

// MARK: - Fake

extension AvatarTrail: Fakeable {
    
    static func fake() -> AvatarTrail {
        return AvatarTrail(
            oldAvatar: Avatar.fake(),
            newAvatar: Avatar.fake()
        )
    }
    
}

// MARK: Decode

extension AvatarTrail: Decodable {
    
    static func decode(json: JSON) -> Decoded<AvatarTrail> {
        let d = curry(AvatarTrail.init)
        return d
            <^> json <| "old_avatar"
            <*> json <| "new_avatar"
    }
}
